package com.rs.tms.dao;

import java.util.List;

import com.rs.tms.model.FlightRequestTModel;

public interface FlightDetailsDAO {
	public String flightDetails(List<FlightRequestTModel> flightDetailsList);
}
