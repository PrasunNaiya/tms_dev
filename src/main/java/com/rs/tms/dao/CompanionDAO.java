package com.rs.tms.dao;

import java.util.List;

import com.rs.tms.model.ComapnionDetails;

public interface CompanionDAO {
	
	public String companionDetails(List<ComapnionDetails> comapnionDetailsList);

}
