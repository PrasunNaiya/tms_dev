package com.rs.tms.dao;

import java.util.List;

import com.rs.tms.model.CarRequestTModel;
import com.rs.tms.model.ComapnionDetails;
import com.rs.tms.model.EmployeeDetails;
import com.rs.tms.model.FlightRequestTModel;
import com.rs.tms.model.HotelRequestTModel;
import com.rs.tms.model.RequestInformationModel;

public interface TmsViewTravelRequestDAO {
	
	public List<RequestInformationModel> fetchRequests(String empId);
	public EmployeeDetails fetchEmployeeRequest(int travelReqId);
	public List<FlightRequestTModel> fetchFlightDetails(int travelReqId);
	public List<HotelRequestTModel> fetchHotelDetails(int travelReqId);
	public List<CarRequestTModel> fetchCarDetails(int travelReqId);
	public List<ComapnionDetails> fetchComapnionDetails(int travelReqId);
	
	

}
