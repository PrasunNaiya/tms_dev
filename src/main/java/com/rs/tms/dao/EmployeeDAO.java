package com.rs.tms.dao;

import com.rs.tms.model.TMSModel;

public interface EmployeeDAO {

	public TMSModel fetchEmployeeDetails(String empId);
	
}
