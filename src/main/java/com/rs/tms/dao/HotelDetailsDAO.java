package com.rs.tms.dao;

import java.util.List;

import org.springframework.stereotype.Component;

import com.rs.tms.model.HotelRequestTModel;
@Component
public interface HotelDetailsDAO {
	
	public String hotelDetails(List<HotelRequestTModel> HotelDetailsList); 

}
