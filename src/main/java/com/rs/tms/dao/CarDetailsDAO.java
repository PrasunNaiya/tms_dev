package com.rs.tms.dao;

import java.util.List;

import com.rs.tms.model.CarRequestTModel;


public interface CarDetailsDAO {
	
	public String carDetails(List<CarRequestTModel> carDetailsList); 

}
