package com.rs.tms.dao;

import com.rs.tms.domain.Customer;

public interface CustomerDAO {

	public Customer fetchCustomer(int projectId);
}
