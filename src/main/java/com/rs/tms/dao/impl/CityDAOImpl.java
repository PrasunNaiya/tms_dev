package com.rs.tms.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.apache.log4j.net.SyslogAppender;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.CityDAO;
import com.rs.tms.domain.City;

@Component
public class CityDAOImpl implements CityDAO {
	
	private JdbcTemplate tmsJdbcTemplate;
	
	@Autowired
    public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
		tmsJdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Override
	public List<City> fetchCities(int countryId) {
		System.out.println("inside fetchCities DAO...");
		System.out.println("countryId = " + countryId);
		return tmsJdbcTemplate.query("select CITY_ID, CITY_NAME from ALSDEVUSER.XXRS_TMS_CITY_M "
				+ "where COUNTRY_ID = ? and IS_ACTIVE = 1", new Object[]{countryId},
			new RowMapper<City>() {
        		public City mapRow(ResultSet rs, int rowNum) throws SQLException {
        			City city = new City();
        			city.setCityId(rs.getInt("CITY_ID"));
        			city.setCityName(rs.getString("CITY_NAME"));
        			return city;
        		}
			}
		);
	}

	@Override
	public City fetchCity(String cityName) {
		// TODO Auto-generated method stub
		return null;
	}

}
