package com.rs.tms.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.FlightDetailsDAO;
import com.rs.tms.model.FlightRequestTModel;
import com.rs.tms.model.HotelRequestTModel;
@Component
public class FlightDetailsDAOImpl implements FlightDetailsDAO{
	private JdbcTemplate tmsJdbcTemplate;
	int listSize = 0;	
	String messageForFlight = null;
	@Autowired
    public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
        this.tmsJdbcTemplate = new JdbcTemplate(dataSource);        		
    }

	@Override
	public String flightDetails(List<FlightRequestTModel> flightDetailsList) {
		// TODO Auto-generated method stub

		String sql = "INSERT into  XXRS_FLIGHT_REQUEST_T(FLIGHT_REQUEST_ID, SOURCE_COUNTRY_ID , SOURCE_CITY_ID , SOURCE_AIRPORT_ID , DESTINATION_COUNTRY_ID ,"
				+ " DESTINATION_CITY_ID, DESTINATION_AIRPORT_ID,DEPARTURE_DATE,PREFFERED_DEPARTURE_TIME,REQUIRED_ARRIVAL_TIME,PORT_OF_ENTRY,FREQUENT_FLIER_NO,"
				+ "FREQUENT_FLIER_AIRLINE,FREQUENT_FLIER_FLIGHT_NO,CLIENT_NAME,TRAVEL_PURPOSE,PROJECT_ID,PROJECT_NAME,PROSPECT_NAME,TRIP_MODE,NOTES)"+
				" Values (XXRS_TMS_FLIGHT_REQUEST_T_SEQ.Nextval,?,?,?,?,?,?,to_date(?,'MM/dd/yyyy'),?,?,?,?,?,?,?,?,?,?,?,?,?)";
		
		

		tmsJdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {
						FlightRequestTModel flightRequestTModel = flightDetailsList.get(i);
						ps.setInt(1,flightRequestTModel.getSrCountryId());
						ps.setInt(2,flightRequestTModel.getSrCityId());
						ps.setInt(3,flightRequestTModel.getSrAirportId());
						ps.setInt(4,flightRequestTModel.getDsCountryId());
						ps.setInt(5,flightRequestTModel.getDsCityId());
						ps.setInt(6,flightRequestTModel.getDsAirportId());
						ps.setString(7,flightRequestTModel.getDeptDate());
						ps.setString(8,flightRequestTModel.getDeptTime());
						ps.setString(9,flightRequestTModel.getArrivalTime());
						ps.setInt(10,flightRequestTModel.getPortOfEntry());System.out.println(flightRequestTModel.getPortOfEntry());
						ps.setString(11,flightRequestTModel.getFrquentFlierNo());
						ps.setString(12,flightRequestTModel.getFrequentFlierAirline());System.out.println(flightRequestTModel.getFrequentFlierAirline());
						ps.setString(13,flightRequestTModel.getFrequentFlierFlightNo());System.out.println(flightRequestTModel.getFrequentFlierFlightNo());
						ps.setString(14,flightRequestTModel.getClientName()); System.out.println(flightRequestTModel.getClientName());
						ps.setString(15,flightRequestTModel.getTravelPurpose());
						ps.setString(16,flightRequestTModel.getProjectId());System.out.println("project id = "+flightRequestTModel.getProjectId());
						ps.setString(17,flightRequestTModel.getProjectName());System.out.println("project name = "+flightRequestTModel.getProjectName());
						ps.setString(18,flightRequestTModel.getProspectName());
						ps.setString(19,flightRequestTModel.getTripMode());
						ps.setString(20,flightRequestTModel.getNotes());
						
						
					}

					@Override
					public int getBatchSize() {
						System.out.println("listSize = "+listSize);
						listSize = flightDetailsList.size();
						return listSize;
					}
				  });	
		if(listSize>0){
			messageForFlight ="success";
			
		}
		else
		{
			messageForFlight = "unsuccess";
		}
		
		return messageForFlight;
	
	}

}
