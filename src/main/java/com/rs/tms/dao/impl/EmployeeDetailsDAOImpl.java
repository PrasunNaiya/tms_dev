package com.rs.tms.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.Date;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.EmployeeDetailsDAO;
import com.rs.tms.model.EmployeeDetails;
@Component
public class EmployeeDetailsDAOImpl implements EmployeeDetailsDAO{

private JdbcTemplate tmsJdbcTemplate;

	@Autowired
    public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
        this.tmsJdbcTemplate = new JdbcTemplate(dataSource);        		
    }
	
	@Override
	public String employeeDetails(EmployeeDetails employeeDetails) {
		// TODO Auto-generated method stub
		String contactNo = employeeDetails.getContactNo();
		String designation = employeeDetails.getDesignation();
		String dob = employeeDetails.getDob();
		String empId = employeeDetails.getEmpId();
		String name = employeeDetails.getEmpName();
		String gender = employeeDetails.getGender();
		String mealPref = employeeDetails.getMealPreference();
		String passportExpDate = employeeDetails.getPassportExpiryDate();
		String passportNo = employeeDetails.getPassportNo();
		String travelType = employeeDetails.getTravelType();
		String visaExpDate = employeeDetails.getVisaExpiryDate();
		String visaNo = employeeDetails.getVisaNo();
		String visaType = employeeDetails.getVisaType();
		String nameOnPassport = employeeDetails.getNameOnPassport();
		String addrOnPassport = employeeDetails.getAddrOnPassport();
		String passportIssuePlace = employeeDetails.getPassportIssuePlace();
		String passportIssueDate = employeeDetails.getPassportIssueDate();
		String encrCheck = employeeDetails.getEncrCheck();
		String visaIssueDate = employeeDetails.getVisaIssueDate();
		String pitition = employeeDetails.getPitition();
		String validity = employeeDetails.getValidity();
		String nomineeName =employeeDetails.getNomineeName();
		String emrContactPersonNo = employeeDetails.getEmrContactPersonNo();
		String emrContactPersonName = employeeDetails.getEmrContactPersonName();
		String emrContactPersonAddr = employeeDetails.getEmrContactPersonAddr();
				
		/*String sql = "INSERT into  XXRS_TRAVEL_REQUEST_T(TRAVEL_REQUEST_ID,EMP_ID , CONTACT_NO , TRAVEL_TYPE ,"
				+ " PASSPORT_NO, PASSPORT_EXPIRY_DATE ,VISA_TYPE,VISA_NO,VISA_EXPIRY_DATE)"+
				" Values (XXRS_TMS_TRAVEL_REQUEST_T_SEQ.Nextval,?,?,?,?,to_date(?,'MM/dd/yyyy'),?,?,to_date(?,'MM/dd/yyyy'))";
		tmsJdbcTemplate.update(sql, new Object[] {empId,contactNo,travelType,passportNo,passportExpDate,visaType,visaNo,visaExpDate});*/
		
		String sql = "INSERT into  XXRS_TRAVEL_REQUEST_T(TRAVEL_REQUEST_ID,EMP_ID , CONTACT_NO , TRAVEL_TYPE ,"
				+ " PASSPORT_NO, PASSPORT_NAME,PASSPORT_ADDRESS,PASSPORT_ISSUE_PLACE,PASSPORT_ISSUE_DATE, PASSPORT_EXPIRY_DATE ,ECNR_CHECK ,"
				+ "VISA_TYPE,VISA_NO,VISA_ISSUE_DATE,VISA_EXPIRY_DATE,PITITION_NO,VALIDITY,NOMINEE_NAME,E_CONTACT_NAME,E_CONTACT_ADDRESS,E_CONTACT_NO)"+
				" Values (XXRS_TMS_TRAVEL_REQUEST_T_SEQ.Nextval,?,?,?,?,?,?,?,to_date(?,'MM/dd/yyyy'),to_date(?,'MM/dd/yyyy'),?,?,?,to_date(?,'MM/dd/yyyy'),to_date(?,'MM/dd/yyyy'),?,?,?,?,?,?)";
		
		tmsJdbcTemplate.update(sql, new Object[] {empId,contactNo,travelType,passportNo,nameOnPassport,addrOnPassport,passportIssuePlace,passportIssueDate,
				passportExpDate,encrCheck,visaType,visaNo,visaIssueDate,visaExpDate,pitition,validity,nomineeName,emrContactPersonName,emrContactPersonAddr,emrContactPersonNo});
		
		

		/*tmsJdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {
						//EmployeeDetails employeeDetailsObj = employeeDetails.
						ps.setString(1, employeeDetails.getEmpId());
						ps.setInt(2, employeeDetails.getContactNo());
						ps.setString(3, employeeDetails.getTravelType());
						ps.setString(4, employeeDetails.getPassportNo());
						ps.setString(5, employeeDetails.getPassportExpiryDate());
						ps.setString(6, employeeDetails.getVisaType());
						ps.setString(7,employeeDetails.getVisaNo() );
						ps.setString(8, employeeDetails.getVisaExpiryDate());
					}

					@Override
					public int getBatchSize() {
						
						return 0;
					}
				  });	*/
		
		return "success";
	}

}
