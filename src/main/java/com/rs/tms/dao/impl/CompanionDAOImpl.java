package com.rs.tms.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.CompanionDAO;
import com.rs.tms.model.ComapnionDetails;
import com.rs.tms.model.FlightRequestTModel;
@Component
public class CompanionDAOImpl implements CompanionDAO{

	private JdbcTemplate tmsJdbcTemplate;
	int listSize = 0;	
	String messageForCompanion = null;
	@Autowired
	public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
		this.tmsJdbcTemplate = new JdbcTemplate(dataSource);        		
	}

	@Override
	public String companionDetails(List<ComapnionDetails> comapnionDetailsList) {
		// TODO Auto-generated method stub


		String sql = "INSERT into  XXRS_COMPANION_REQUEST_T(COMPANION_REQUEST_ID,COMPANION_NAME ,RELATION ,DATE_OF_BIRTH,PASSPORT_NO,"
				+ "PASSPORT_ADDRESS,PASSPORT_ISSUE_PLACE,PASSPORT_ISSUE_DATE,PASSPORT_EXPIRY_DATE,ECNR_CHECK,"
				+ "VISA_TYPE,VISA_NO,VISA_ISSUE_DATE,VISA_EXPIRY_DATE,VALIDITY,TRAVEL_DATE)"
				+" Values (XXRS_TMS_COMPAN_REQUEST_T_SEQ.Nextval,?,?,to_date(?,'MM/dd/yyyy'),?,?,?,to_date(?,'MM/dd/yyyy'),to_date(?,'MM/dd/yyyy'),?,?,?,to_date(?,'MM/dd/yyyy'),to_date(?,'MM/dd/yyyy'),?,to_date(?,'MM/dd/yyyy'))";



		tmsJdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				ComapnionDetails comapnionDetails = comapnionDetailsList.get(i);

				ps.setString(1,comapnionDetails.getCompanionName());
				ps.setString(2,comapnionDetails.getRelation());
				ps.setString(3,comapnionDetails.getCdateOfBirth());
				ps.setString(4,comapnionDetails.getCpassportNo());
				ps.setString(5,comapnionDetails.getCaddressOnPassport());
				ps.setString(6,comapnionDetails.getCpassportIssuePlace());
				ps.setString(7,comapnionDetails.getCpassportIssueDate());
				ps.setString(8,comapnionDetails.getCpassportExpiryDate());
				ps.setString(9,comapnionDetails.getcECNRCheck());
				ps.setString(10,comapnionDetails.getCvisaType());
				ps.setString(11,comapnionDetails.getCvisaNo());
				ps.setString(12,comapnionDetails.getCvisaIssueDate());
				ps.setString(13,comapnionDetails.getCvisaExpiryDate());
				ps.setString(14,comapnionDetails.getCvalidity());
				ps.setString(15,comapnionDetails.getCdateOfTravel());

			}

			@Override
			public int getBatchSize() {
				listSize = comapnionDetailsList.size();
				return listSize;
			}
		});	

		if(listSize>0){
			messageForCompanion = "success";
		}
		else
		{
			messageForCompanion = "unsuccess";
		}

		return messageForCompanion;

	}

}


