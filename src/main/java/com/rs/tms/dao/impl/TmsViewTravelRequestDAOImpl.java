package com.rs.tms.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.TmsViewTravelRequestDAO;
import com.rs.tms.model.CarRequestTModel;
import com.rs.tms.model.ComapnionDetails;
import com.rs.tms.model.EmployeeDetails;
import com.rs.tms.model.FlightRequestTModel;
import com.rs.tms.model.HotelRequestTModel;
import com.rs.tms.model.RequestInformationModel;

@Component
public class TmsViewTravelRequestDAOImpl implements TmsViewTravelRequestDAO{

private JdbcTemplate tmsJdbcTemplate;
	
	@Autowired
    public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
		tmsJdbcTemplate = new JdbcTemplate(dataSource);
    }


	@Override
	public List<RequestInformationModel> fetchRequests(String empId) {
		// TODO Auto-generated method stub
		System.out.println("inside fetch request dao");
		System.out.println("empId = " + empId);
		
		
		return  tmsJdbcTemplate.query("Select TRAVEL_REQUEST_ID,EMP_ID,CREATION_TIMESTAMP from XXRS_TRAVEL_REQUEST_T where EMP_ID=?",
			new Object[]{empId},new RowMapper<RequestInformationModel>() {
			public RequestInformationModel mapRow(ResultSet rs, int rowNum) throws SQLException {
				RequestInformationModel requestInformationModel = new RequestInformationModel();
				requestInformationModel.setTravelReqId(rs.getInt("TRAVEL_REQUEST_ID"));
				requestInformationModel.setEmpId(rs.getString("EMP_ID"));
				requestInformationModel.setCreationTimeStamp(rs.getString("CREATION_TIMESTAMP"));
    			return requestInformationModel;
    		}
			
		});
	
	}

	
	@Override
	public EmployeeDetails fetchEmployeeRequest(int travelReqId) {
		// TODO Auto-generated method stub
		

		System.out.println("inside fetch employee transactional DAO...");
		System.out.println("travelReqId = " + travelReqId);
		
		return  tmsJdbcTemplate.queryForObject("Select * from XXRS_TRAVEL_REQUEST_T where TRAVEL_REQUEST_ID=?",new Object[]{travelReqId},new RowMapper<EmployeeDetails>() {
    		public EmployeeDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
    			EmployeeDetails employeeDetails = new EmployeeDetails();
    			employeeDetails.setTravelRequestId(rs.getInt("TRAVEL_REQUEST_ID"));
    			employeeDetails.setRrqNo(rs.getString("RRQ_NO"));
    			employeeDetails.setEmpId(rs.getString("EMP_ID"));
    			employeeDetails.setContactNo(rs.getString("CONTACT_NO"));
    			employeeDetails.setTravelType(rs.getString("TRAVEL_TYPE"));
    			employeeDetails.setPassportNo(rs.getString("PASSPORT_NO"));
    			employeeDetails.setNameOnPassport(rs.getString("PASSPORT_NAME"));
    			employeeDetails.setAddrOnPassport(rs.getString("PASSPORT_ADDRESS"));
    			employeeDetails.setPassportIssuePlace(rs.getString("PASSPORT_ISSUE_PLACE"));
    			employeeDetails.setPassportIssueDate(rs.getString("PASSPORT_ISSUE_DATE"));
    			employeeDetails.setPassportExpiryDate(rs.getString("PASSPORT_EXPIRY_DATE"));
    			employeeDetails.setEncrCheck(rs.getString("ECNR_CHECK"));
    			employeeDetails.setVisaType(rs.getString("VISA_TYPE"));
    			employeeDetails.setVisaNo(rs.getString("VISA_NO"));
    			employeeDetails.setVisaIssueDate(rs.getString("VISA_ISSUE_DATE"));
    			employeeDetails.setVisaExpiryDate(rs.getString("VISA_EXPIRY_DATE"));
    			employeeDetails.setPitition(rs.getString("PITITION_NO"));
    			employeeDetails.setValidity(rs.getString("VALIDITY"));
    			employeeDetails.setNomineeName(rs.getString("NOMINEE_NAME"));
    			employeeDetails.setEmrContactPersonName(rs.getString("E_CONTACT_NAME"));
    			employeeDetails.setEmrContactPersonAddr(rs.getString("E_CONTACT_ADDRESS"));
    			employeeDetails.setEmrContactPersonNo(rs.getString("E_CONTACT_NO"));
    			employeeDetails.setIsApproved(rs.getInt("IS_APPROVED"));
    			employeeDetails.setIsRejected(rs.getInt("IS_REJECTED"));
    			employeeDetails.setIsCostApproved(rs.getInt("IS_COST_APPROVED"));
    			employeeDetails.setIsCostRejected(rs.getInt("IS_COST_REJECTED"));
    			employeeDetails.setIsActive(rs.getInt("IS_ACTIVE"));
    			employeeDetails.setCreatedBy(rs.getString("CREATED_BY"));
    			employeeDetails.setCreationTimeStamp(rs.getString("CREATION_TIMESTAMP"));
    			employeeDetails.setModifiedBy("MODIFIED_BY");
    			employeeDetails.setModificationTimeStamp(rs.getString("MODIFICATION_TIMESTAMP"));
    			return employeeDetails;
    		}
		});
		
		
	}

	@Override
	public List<FlightRequestTModel> fetchFlightDetails(int flightReqId) {//XXRS_FLIGHT_REQUEST_T
		// TODO Auto-generated method stub
		
		System.out.println("inside fetch flight transactional DAO...");
		System.out.println("travelReqId = " + flightReqId);
		
		return  tmsJdbcTemplate.query("select * from XXRS_FLIGHT_REQUEST_T"
				+ " where rrq_no = (select rrq_no from xxrs_travel_request_t where travel_request_id = ?)",new Object[]{flightReqId},new RowMapper<FlightRequestTModel>() {
    		public FlightRequestTModel mapRow(ResultSet rs, int rowNum) throws SQLException {
    			FlightRequestTModel flightRequestTModel = new FlightRequestTModel();
    			flightRequestTModel.setFlightRequestId(rs.getInt("FLIGHT_REQUEST_ID"));
    			flightRequestTModel.setRrqNo(rs.getString("RRQ_NO"));
    			flightRequestTModel.setSrCountryId(rs.getInt("SOURCE_COUNTRY_ID"));
    			flightRequestTModel.setSrCityId(rs.getInt("SOURCE_CITY_ID"));
    			flightRequestTModel.setSrAirportId(rs.getInt("SOURCE_AIRPORT_ID"));
    			flightRequestTModel.setDsCountryId(rs.getInt("DESTINATION_COUNTRY_ID"));
    			flightRequestTModel.setDsCityId(rs.getInt("DESTINATION_CITY_ID"));
    			flightRequestTModel.setDsAirportId(rs.getInt("DESTINATION_AIRPORT_ID"));
    			flightRequestTModel.setDeptDate(rs.getString("DEPARTURE_DATE"));
    			flightRequestTModel.setDeptTime(rs.getString("PREFFERED_DEPARTURE_TIME"));
    			flightRequestTModel.setArrivalTime(rs.getString("REQUIRED_ARRIVAL_TIME"));
    			flightRequestTModel.setPortOfEntry(rs.getInt("PORT_OF_ENTRY"));
    			flightRequestTModel.setFrquentFlierNo(rs.getString("FREQUENT_FLIER_NO"));
    			flightRequestTModel.setFrequentFlierAirline(rs.getString("FREQUENT_FLIER_AIRLINE"));
    			flightRequestTModel.setFrequentFlierFlightNo(rs.getString("FREQUENT_FLIER_FLIGHT_NO"));
    			flightRequestTModel.setClientName(rs.getString("CLIENT_NAME"));
    			flightRequestTModel.setClientAddr(rs.getString("CLIENT_ADDRESS"));
    			flightRequestTModel.setClientContactNo(rs.getInt("CLIENT_CONTACT_NO"));
    			flightRequestTModel.setTravelPurpose(rs.getString("TRAVEL_PURPOSE"));
    			flightRequestTModel.setProjectId(rs.getString("PROJECT_ID"));
    			flightRequestTModel.setProjectName(rs.getString("PROJECT_NAME"));
    			flightRequestTModel.setProjectName(rs.getString("PROSPECT_NAME"));
    			flightRequestTModel.setTripMode(rs.getString("TRIP_MODE"));
    			flightRequestTModel.setNotes(rs.getString("NOTES"));
    			flightRequestTModel.setCost(rs.getInt("COSTS"));
    			flightRequestTModel.setCurrency(rs.getString("CURRENCY"));
    			flightRequestTModel.setIsBudgeted(rs.getInt("IS_BUDGETED"));
    			flightRequestTModel.setIsBillable(rs.getInt("IS_BILLABLE"));
    			flightRequestTModel.setIsApproved(rs.getInt("IS_APPROVED"));
    			flightRequestTModel.setIsRejected(rs.getInt("IS_REJECTED"));
    			flightRequestTModel.setIsCostApproved(rs.getInt("IS_COST_APPROVED"));
    			flightRequestTModel.setIsCostRejected(rs.getInt("IS_COST_REJECTED"));
    			flightRequestTModel.setIsActive(rs.getInt("IS_ACTIVE"));
    			flightRequestTModel.setCreatedBy(rs.getString("CREATED_BY"));
    			flightRequestTModel.setCreationTimeStamp(rs.getString("CREATION_TIMESTAMP"));
    			flightRequestTModel.setModifiedBy(rs.getString("MODIFIED_BY"));
    			flightRequestTModel.setModificationTimeStamp(rs.getString("MODIFICATION_TIMESTAMP"));
    			return flightRequestTModel;
    		}
		});
		
	}

	@Override
	public List<HotelRequestTModel> fetchHotelDetails(int hotelReqId) {
		// TODO Auto-generated method stub
		
		System.out.println("inside fetch hotel transactional DAO...");
		System.out.println("hotelReqId = " + hotelReqId);
		
		return  tmsJdbcTemplate.query("select * from xxrs_tms_hotel_request_t where"
				+ " rrq_no = (select rrq_no from xxrs_travel_request_t where travel_request_id = ?)",new Object[]{hotelReqId},new RowMapper<HotelRequestTModel>() {
    		public HotelRequestTModel mapRow(ResultSet rs, int rowNum) throws SQLException {
    			HotelRequestTModel hotelRequestTModel = new HotelRequestTModel();
    			hotelRequestTModel.setHotelRequestId(rs.getInt("HOTEL_REQUEST_ID"));
    			hotelRequestTModel.setRrqNo(rs.getString("RRQ_NO"));
    			hotelRequestTModel.setCountryId(rs.getInt("COUNTRY_ID"));
    			hotelRequestTModel.setCityId(rs.getInt("CITY_ID"));
    			hotelRequestTModel.setPrefHotel(rs.getString("PREFERRED_HOTEL"));
    			hotelRequestTModel.setCheckInDate(rs.getString("CHECK_IN_DATE"));
    			hotelRequestTModel.setCheckOutDate(rs.getString("CHECK_OUT_DATE"));
    			hotelRequestTModel.setRewardMemberNo(rs.getString("REWARD_MEMBER_NO"));
    			hotelRequestTModel.setClientName(rs.getString("CLIENT_NAME"));
    			hotelRequestTModel.setTravellingLocationAddr(rs.getString("TRAVELLING_LOCATION_ADDRESS"));
    			hotelRequestTModel.setTravelPurpose(rs.getString("TRAVEL_PURPOSE"));
    			hotelRequestTModel.setProjectId(rs.getInt("PROJECT_ID"));
    			hotelRequestTModel.setProjectName(rs.getString("PROJECT_NAME"));
    			hotelRequestTModel.setProspectName(rs.getString("PROSPECT_NAME"));
    			hotelRequestTModel.setTripMode(rs.getString("TRIP_MODE"));
    			hotelRequestTModel.setNotes(rs.getString("NOTES"));
    			hotelRequestTModel.setCost(rs.getInt("COSTS"));
    			hotelRequestTModel.setCurrency(rs.getString("CURRENCY"));
    			hotelRequestTModel.setIsBudgeted(rs.getInt("IS_BUDGETED"));
    			hotelRequestTModel.setIsBillable(rs.getInt("IS_BILLABLE"));
    			hotelRequestTModel.setIsApproved(rs.getInt("IS_APPROVED"));
    			hotelRequestTModel.setIsRejected(rs.getInt("IS_REJECTED"));
    			hotelRequestTModel.setIsCostApproved(rs.getInt("IS_COST_APPROVED"));
    			hotelRequestTModel.setIsCostRejected(rs.getInt("IS_COST_REJECTED"));
    			hotelRequestTModel.setIsActive(rs.getInt("IS_ACTIVE"));
    			hotelRequestTModel.setCreatedBy(rs.getString("CREATED_BY"));
    			hotelRequestTModel.setCreationTimeStamp(rs.getString("CREATION_TIMESTAMP"));
    			hotelRequestTModel.setModifiedBy(rs.getString("MODIFIED_BY"));
    			hotelRequestTModel.setModificationTimeStamp(rs.getString("MODIFICATION_TIMESTAMP"));
    			return hotelRequestTModel;
    		}
		});
		
		
	}

	@Override
	public List<CarRequestTModel> fetchCarDetails(int carReqId) {
		// TODO Auto-generated method stub
		
		System.out.println("inside fetch car transactional DAO...");//XXRS_TMS_CAR_REQUEST_T
		System.out.println("carReqId = " + carReqId);
		
		return  tmsJdbcTemplate.query("select * from XXRS_TMS_CAR_REQUEST_T "
				+ "where rrq_no = (select rrq_no from xxrs_travel_request_t where travel_request_id  = ?)",new Object[]{carReqId},new RowMapper<CarRequestTModel>() {
    		public CarRequestTModel mapRow(ResultSet rs, int rowNum) throws SQLException {
    			CarRequestTModel carRequestTModel = new CarRequestTModel();
    			carRequestTModel.setCarRequestId(rs.getInt("CAR_REQUEST_ID"));
    			carRequestTModel.setRrqNo(rs.getString("RRQ_NO"));
    			carRequestTModel.setCountryId(rs.getInt("COUNTRY_ID"));
    			carRequestTModel.setCityId(rs.getInt("CITY_ID"));
    			carRequestTModel.setPickUpLocation(rs.getString("PICK_UP_LOCATION"));
    			carRequestTModel.setPickUpTime(rs.getString("PICK_UP_TIME"));
    			carRequestTModel.setDropLoaction(rs.getString("DROP_OFF_LOCATION"));
    			carRequestTModel.setNeedPickUp(rs.getString("NEED_RETURN_PICK_UP"));
    			carRequestTModel.setCarType(rs.getString("CAR_TYPE"));
    			carRequestTModel.setPrefCarCompany(rs.getString("PREFERRED_CAR_COMPANY"));
    			carRequestTModel.setMebershipNo(rs.getString("MEMBERSHIP_NO"));
    			carRequestTModel.setSharedCar(rs.getString("SHARED_CAR"));
    			carRequestTModel.setClientName(rs.getString("CLIENT_NAME"));
    			carRequestTModel.setTravellingLocationAddr(rs.getString("TRAVELLING_LOCATION_ADDRESS"));
    			carRequestTModel.setTravelPurpose(rs.getString("TRAVEL_PURPOSE"));
    			carRequestTModel.setProjectId(rs.getInt("PROJECT_ID"));
    			carRequestTModel.setProjectName(rs.getString("PROJECT_NAME"));
    			carRequestTModel.setProspectName(rs.getString("PROSPECT_NAME"));
    			carRequestTModel.setTripMode(rs.getString("TRIP_MODE"));
    			carRequestTModel.setNotes(rs.getString("NOTES"));
    			carRequestTModel.setCost(rs.getInt("COSTS"));
    			carRequestTModel.setCurrency(rs.getString("CURRENCY"));
    			carRequestTModel.setIsBudgeted(rs.getInt("IS_BUDGETED"));
    			carRequestTModel.setIsBillable(rs.getInt("IS_BILLABLE"));
    			carRequestTModel.setIsApproved(rs.getInt("IS_APPROVED"));
    			carRequestTModel.setIsRejected(rs.getInt("IS_REJECTED"));
    			carRequestTModel.setIsCostApproved(rs.getInt("IS_COST_APPROVED"));
    			carRequestTModel.setIsCostRejected(rs.getInt("IS_COST_REJECTED"));
    			carRequestTModel.setIsActive(rs.getInt("IS_ACTIVE"));
    			carRequestTModel.setCreatedBy(rs.getString("CREATED_BY"));
    			carRequestTModel.setCreationTimeStamp(rs.getString("CREATION_TIMESTAMP"));
    			carRequestTModel.setModifiedBy(rs.getString("MODIFIED_BY"));
    			carRequestTModel.setModificationTimeStamp(rs.getString("MODIFICATION_TIMESTAMP"));
    			return carRequestTModel;
    		}
		});
		
		
	}

	@Override
	public List<ComapnionDetails> fetchComapnionDetails(int companionReqId) {//XXRS_COMPANION_REQUEST_T
		// TODO Auto-generated method stub
		System.out.println("inside fetch companion transactional DAO...");
		System.out.println("companionReqId = " + companionReqId);
		
		return  tmsJdbcTemplate.query("select * from XXRS_COMPANION_REQUEST_T "
				+ "where rrq_no = (select rrq_no from xxrs_travel_request_t where travel_request_id  = ?)",new Object[]{companionReqId},new RowMapper<ComapnionDetails>() {
    		public ComapnionDetails mapRow(ResultSet rs, int rowNum) throws SQLException {
    			ComapnionDetails comapnionDetails = new ComapnionDetails();
    			comapnionDetails.setCompanionReqId(rs.getInt("COMPANION_REQUEST_ID"));
    			comapnionDetails.setRrqNo(rs.getString("RRQ_NO"));
    			comapnionDetails.setCompanionName(rs.getString("COMPANION_NAME"));
    			comapnionDetails.setRelation(rs.getString("RELATION"));
    			comapnionDetails.setCdateOfBirth(rs.getString("DATE_OF_BIRTH"));
    			comapnionDetails.setCpassportNo(rs.getString("PASSPORT_NO"));
    			comapnionDetails.setCaddressOnPassport(rs.getString("PASSPORT_ADDRESS"));
    			comapnionDetails.setCpassportIssuePlace(rs.getString("PASSPORT_ISSUE_PLACE"));
    			comapnionDetails.setCpassportIssueDate(rs.getString("PASSPORT_ISSUE_DATE"));
    			comapnionDetails.setCpassportExpiryDate(rs.getString("PASSPORT_EXPIRY_DATE"));
    			comapnionDetails.setcECNRCheck(rs.getString("ECNR_CHECK"));
    			comapnionDetails.setCvisaType(rs.getString("VISA_TYPE"));
    			comapnionDetails.setCvisaNo(rs.getString("VISA_NO"));
    			comapnionDetails.setCvisaIssueDate(rs.getString("VISA_ISSUE_DATE"));
    			comapnionDetails.setCvisaExpiryDate(rs.getString("VISA_EXPIRY_DATE"));
    			comapnionDetails.setCvalidity(rs.getString("VALIDITY"));
    			comapnionDetails.setCdateOfTravel(rs.getString("TRAVEL_DATE"));
    			comapnionDetails.setIsActive(rs.getInt("IS_ACTIVE"));
    			comapnionDetails.setCreatedBy(rs.getString("CREATED_BY"));
    			comapnionDetails.setCreationTimeStamp(rs.getString("CREATION_TIMESTAMP"));
    			comapnionDetails.setModifiedBy(rs.getString("MODIFIED_BY"));
    			comapnionDetails.setModificationTimeStamp(rs.getString("MODIFICATION_TIMESTAMP"));
    			return comapnionDetails;
    		}
		});
	}



}
