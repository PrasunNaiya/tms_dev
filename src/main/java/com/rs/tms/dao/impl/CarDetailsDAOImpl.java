package com.rs.tms.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.CarDetailsDAO;
import com.rs.tms.model.CarRequestTModel;
import com.rs.tms.model.FlightRequestTModel;
@Component
public class CarDetailsDAOImpl implements CarDetailsDAO{
	String messageForCar = null;
	private JdbcTemplate tmsJdbcTemplate;
	int listSize = 0;	
	String messageForFlight = null;
	@Autowired
	public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
		this.tmsJdbcTemplate = new JdbcTemplate(dataSource);        		
	}

	@Override
	public String carDetails(List<CarRequestTModel> carDetailsList) {

		String sql = "INSERT into  XXRS_TMS_CAR_REQUEST_T(CAR_REQUEST_ID, COUNTRY_ID , CITY_ID , PICK_UP_LOCATION , PICK_UP_TIME , DROP_OFF_LOCATION,"
				+ " CAR_TYPE,PREFERRED_CAR_COMPANY,MEMBERSHIP_NO,CLIENT_NAME,TRAVELLING_LOCATION_ADDRESS,TRAVEL_PURPOSE,PROJECT_ID,PROSPECT_NAME,TRIP_MODE,NOTES)"+
				" Values (XXRS_TMS_FLIGHT_REQUEST_T_SEQ.Nextval,?,?,?,to_date(?,'MM/dd/yyyy hh24:mi'),?,?,?,?,?,?,?,?,?,?,?)";



		tmsJdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

			@Override
			public void setValues(PreparedStatement ps, int i) throws SQLException {
				CarRequestTModel flightRequestTModel = carDetailsList.get(i);
				ps.setInt(1,flightRequestTModel.getCountryId());
				ps.setInt(2,flightRequestTModel.getCityId());
				ps.setString(3,flightRequestTModel.getPickUpLocation());
				ps.setString(4,flightRequestTModel.getPickUpTime());
				ps.setString(5,flightRequestTModel.getDropLoaction());
				ps.setString(6,flightRequestTModel.getCarType());
				ps.setString(7,flightRequestTModel.getPrefCarCompany());
				ps.setString(8,flightRequestTModel.getMebershipNo());
				ps.setString(9,flightRequestTModel.getClientName());
				ps.setString(10,flightRequestTModel.getTravellingLocationAddr());
				ps.setString(11,flightRequestTModel.getTravelPurpose());
				ps.setInt(12,flightRequestTModel.getProjectId());System.out.println("project id = "+flightRequestTModel.getProjectId());
				ps.setString(13,flightRequestTModel.getProspectName());System.out.println("project id = "+flightRequestTModel.getProspectName());
				ps.setString(14,flightRequestTModel.getTripMode());
				ps.setString(15,flightRequestTModel.getNotes());
				
			


			}

			@Override
			public int getBatchSize() {
				listSize = carDetailsList.size();
				return listSize;
			}
		});	
		if(listSize>0){
			messageForCar ="success";

		}
		else
		{
			messageForCar = "unsuccess";
		}

		return messageForCar;

	}
}


