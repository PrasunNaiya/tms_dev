package com.rs.tms.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.CustomerDAO;
import com.rs.tms.domain.Customer;

@Component
public class CustomerDAOImpl implements CustomerDAO {
	
private JdbcTemplate erpJdbcTemplate;
	
	@Autowired
    public void setDataSource(@Qualifier("erpDataSource")DataSource  dataSource) {
		erpJdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Override
	public Customer fetchCustomer(int projectId) {
		System.out.println("inside fetchCustomer DAO...");
		return erpJdbcTemplate.queryForObject("select c.CUSTOMER_ID, c.CUSTOMER_NAME "
			+ "from apps.ar_customers c join pa.pa_project_customers pc "
			+ "on c.CUSTOMER_ID = pc.CUSTOMER_ID "
			+ "where pc.PROJECT_ID = ?",
			new Object[]{projectId},
			new RowMapper<Customer>() {
				public Customer mapRow(ResultSet rs, int rowNum) throws SQLException {
					Customer customer = new Customer();
					customer.setCustomerId(rs.getInt("CUSTOMER_ID"));
					customer.setCustomerName(rs.getString("CUSTOMER_NAME"));
					return customer;
				}
			});
	}

}
