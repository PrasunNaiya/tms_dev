package com.rs.tms.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.AirportDAO;
import com.rs.tms.domain.Airport;

@Component
public class AirportDAOImpl implements AirportDAO {
	
	private JdbcTemplate tmsJdbcTemplate;
	
	@Autowired
    public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
		tmsJdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Override
	public List<Airport> fetchAirports(int cityId) {
		System.out.println("inside fetchAirports DAO...");
		System.out.println("cityId = " + cityId);
		return tmsJdbcTemplate.query("select AIRPORT_ID, AIRPORT_NAME from ALSDEVUSER.XXRS_TMS_AIRPORT_M "
				+ "where CITY_ID = ? and IS_ACTIVE = 1", new Object[]{cityId},
			new RowMapper<Airport>() {
        		public Airport mapRow(ResultSet rs, int rowNum) throws SQLException {
        			Airport airport = new Airport();
        			airport.setAirportId(rs.getInt("AIRPORT_ID"));
        			airport.setAirportName(rs.getString("AIRPORT_NAME"));
        			return airport;
        		}
			}
		);
	}

}
