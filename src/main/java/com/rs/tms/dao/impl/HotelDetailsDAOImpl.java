package com.rs.tms.dao.impl;

import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.BatchPreparedStatementSetter;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.HotelDetailsDAO;
import com.rs.tms.model.HotelRequestTModel;
@Component
public class HotelDetailsDAOImpl implements HotelDetailsDAO{
	
private JdbcTemplate tmsJdbcTemplate;
int listSize = 0;	
String messageForHotel = null;
	@Autowired
    public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
        this.tmsJdbcTemplate = new JdbcTemplate(dataSource);        		
    }

	@Override
	public String hotelDetails(List<HotelRequestTModel> HotelDetailsList) {
		// TODO Auto-generated method stub
		
		String sql = "INSERT into  XXRS_TMS_HOTEL_REQUEST_T(HOTEL_REQUEST_ID, COUNTRY_ID , CITY_ID , PREFERRED_HOTEL , CHECK_IN_DATE , CHECK_OUT_DATE,"
				+ " REWARD_MEMBER_NO,CLIENT_NAME,TRAVELLING_LOCATION_ADDRESS,TRAVEL_PURPOSE,PROJECT_ID,PROJECT_NAME,PROSPECT_NAME,TRIP_MODE,NOTES)"+
				" Values (XXRS_TMS_HOTEL_REQUEST_T_SEQ.Nextval,?,?,?,to_date(?,'MM/dd/yyyy hh24:mi'),to_date(?,'MM/dd/yyyy hh24:mi'),?,?,?,?,?,?,?,?,?)";
		
		

		tmsJdbcTemplate.batchUpdate(sql, new BatchPreparedStatementSetter() {

					@Override
					public void setValues(PreparedStatement ps, int i) throws SQLException {
						HotelRequestTModel hotelRequestTModel = HotelDetailsList.get(i);
						ps.setInt(1,hotelRequestTModel.getCountryId());
						ps.setInt(2,hotelRequestTModel.getCityId());
						ps.setString(3,hotelRequestTModel.getPrefHotel());
						ps.setString(4,hotelRequestTModel.getCheckInDate());
						ps.setString(5,hotelRequestTModel.getCheckOutDate());
						ps.setString(6,hotelRequestTModel.getRewardMemberNo());
						ps.setString(7,hotelRequestTModel.getClientName());
						ps.setString(8,hotelRequestTModel.getTravellingLocationAddr());
						ps.setString(9,hotelRequestTModel.getTravelPurpose());
						ps.setInt(10,hotelRequestTModel.getProjectId());System.out.println("project id = "+hotelRequestTModel.getProjectId());
						ps.setString(11,hotelRequestTModel.getProjectName());System.out.println("project name = "+hotelRequestTModel.getProjectName());
						ps.setString(12,hotelRequestTModel.getProspectName());
						ps.setString(13,hotelRequestTModel.getTripMode());
						ps.setString(14,hotelRequestTModel.getNotes());
						
						
					}

					@Override
					public int getBatchSize() {
						listSize = HotelDetailsList.size();
						return listSize;
					}
				  });	
		if(listSize>0){
			messageForHotel ="success";
			
		}
		else
		{
			messageForHotel = "unsuccess";
		}
		
		return messageForHotel;
	}

}
