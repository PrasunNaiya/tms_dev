package com.rs.tms.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.ProjectDAO;
import com.rs.tms.domain.Project;

@Component
public class ProjectDAOImpl implements ProjectDAO {
	
	private JdbcTemplate erpJdbcTemplate;
	
	@Autowired
    public void setDataSource(@Qualifier("erpDataSource")DataSource  dataSource) {
        this.erpJdbcTemplate = new JdbcTemplate(dataSource);        		
    }

	@Override
	public List<Project> fetchProjects() {
		System.out.println("inside fetchProjects DAO...");
		return erpJdbcTemplate.query("select PROJECT_ID, NAME from pa.PA_PROJECTS_ALL "
				+ "where PROJECT_STATUS_CODE = 'APPROVED' "
				+ "and CLOSED_DATE is null "
				+ "and substr(SEGMENT1,-4) not in ('1000','0000')"
				+ "and upper(PROJECT_TYPE) not like '%PROCESS%'", 
			new RowMapper<Project>() {
        		public Project mapRow(ResultSet rs, int rowNum) throws SQLException {
        			Project project = new Project();
        			project.setProjectId(rs.getInt("PROJECT_ID"));
        			project.setProjectName(rs.getString("NAME"));
        			return project;
        		}
			}
		);
	}
	
	@Override
	public List<Project> fetchProcesses() {
		System.out.println("inside fetchProcesses DAO...");
		return erpJdbcTemplate.query("select PROJECT_ID, NAME from pa.PA_PROJECTS_ALL "
				+ "where PROJECT_STATUS_CODE='APPROVED' "
				+ "and CLOSED_DATE is null "
				+ "and upper(PROJECT_TYPE) like '%PROCESS%'", 
			new RowMapper<Project>() {
        		public Project mapRow(ResultSet rs, int rowNum) throws SQLException {
        			Project project = new Project();
        			project.setProjectId(rs.getInt("PROJECT_ID"));
        			project.setProjectName(rs.getString("NAME"));
        			return project;
        		}
			}
		);
	}
	
}
