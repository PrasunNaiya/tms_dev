package com.rs.tms.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.CountryDAO;
import com.rs.tms.domain.Country;

@Component
public class CountryDAOImpl implements CountryDAO {
	
	private JdbcTemplate tmsJdbcTemplate;
	
	@Autowired
    public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
		tmsJdbcTemplate = new JdbcTemplate(dataSource);
    }

	@Override
	public List<Country> fetchCountries() {
		System.out.println("inside fetchCountries DAO...");
		return tmsJdbcTemplate.query("select COUNTRY_ID, COUNTRY_NAME from ALSDEVUSER.XXRS_TMS_COUNTRY_M where IS_ACTIVE = 1", 
			new RowMapper<Country>() {
        		public Country mapRow(ResultSet rs, int rowNum) throws SQLException {
        			Country country = new Country();
        			country.setCountryId(rs.getInt("COUNTRY_ID"));
        			country.setCountryName(rs.getString("COUNTRY_NAME"));
        			return country;
        		}
			}
		);
	}

	@Override
	public Country fetchCountry(String countryName) {
		// TODO Auto-generated method stub
		return null;
	}

}
