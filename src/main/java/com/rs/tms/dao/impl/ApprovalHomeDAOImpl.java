package com.rs.tms.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.ApprovalHomeDAO;
import com.rs.tms.model.ApprovalHomeModel;
import com.rs.tms.model.ApproveCarRequestModel;
import com.rs.tms.model.ApproveFlightRequestModel;
import com.rs.tms.model.ApproveHotelRequestModel;
import com.rs.tms.model.RequestInformationModel;
@Component
public class ApprovalHomeDAOImpl implements ApprovalHomeDAO{
	
private JdbcTemplate tmsJdbcTemplate;
	
	@Autowired
    public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
		tmsJdbcTemplate = new JdbcTemplate(dataSource);
    }


	@Override
	public List<ApprovalHomeModel> getPendingRequests(String empId) {
		// TODO Auto-generated method stub
		System.out.println("inside approve hotel request dao");
		System.out.println("empId = " + empId);
		
		
		return  tmsJdbcTemplate.query("select project_id from xxrs.xxrs_res_alloc_data_temp@rsdnt2prod where emp_id = ? and project_role in ("
				+ "'Account Manager','PROJ MANAGER','VICE PRESIDENT','Project Manager','Project Manager ERP','Delivery Head','Solution Manager',"
				+ "'Process Owner','SDM', 'ACMGR','MGR','DELIVERY MANAGER','SALES MANAGER','ASSISTANT GENERAL MANAGER','SENIOR MANAGER')",
			new Object[]{empId},new RowMapper<ApprovalHomeModel>() {
			public ApprovalHomeModel mapRow(ResultSet rs, int rowNum) throws SQLException {
				ApprovalHomeModel approvalHomeModel = new ApprovalHomeModel();
				//approveHotelRequestModel.setHotelReqId(rs.getInt("HOTEL_REQUEST_ID"));
				approvalHomeModel.setProjectId(rs.getInt("project_id"));
				//approveHotelRequestModel.setCreationTimeStamp(rs.getString("CREATION_TIMESTAMP"));
    			return approvalHomeModel;
    		}
			
		});
	}

	@Override
	public List<ApproveHotelRequestModel> approveHotelReq(String empId) {
		// TODO Auto-generated method stub
		
		
		System.out.println("inside approve hotel request dao");
		System.out.println("empId = " + empId);
		
		
		return  tmsJdbcTemplate.query("select distinct RRQ_NO from xxrs_tms_hotel_request_t where project_id in "
				+ "(	select project_id from xxrs.xxrs_res_alloc_data_temp@rsdnt2prod where emp_id = ? and project_role in ("
				+ "'Account Manager', 'PROJ MANAGER', 'VICE PRESIDENT','Project Manager','Project Manager ERP','Delivery Head','Solution Manager', "
				+ "'Process Owner','SDM','ACMGR', 'MGR','DELIVERY MANAGER','SALES MANAGER','ASSISTANT GENERAL MANAGER','SENIOR MANAGER')"
				+ ") and is_approved = 0 and is_rejected = 0",
			new Object[]{empId},new RowMapper<ApproveHotelRequestModel>() {
			public ApproveHotelRequestModel mapRow(ResultSet rs, int rowNum) throws SQLException {
				ApproveHotelRequestModel approveHotelRequestModel = new ApproveHotelRequestModel();
				//approveHotelRequestModel.setHotelReqId(rs.getInt("HOTEL_REQUEST_ID"));
				approveHotelRequestModel.setRrqNo(rs.getString("RRQ_NO"));
				//approveHotelRequestModel.setCreationTimeStamp(rs.getString("CREATION_TIMESTAMP"));
    			return approveHotelRequestModel;
    		}
			
		});
	
	}


	@Override
	public List<ApproveFlightRequestModel> approveFlightReq(String empId) {
		// TODO Auto-generated method stub
		System.out.println("inside approve flight request dao");
		System.out.println("empId = " + empId);
		
		
		return  tmsJdbcTemplate.query("select distinct RRQ_NO from XXRS_FLIGHT_REQUEST_T where project_id in "
				+ "(	select project_id from xxrs.xxrs_res_alloc_data_temp@rsdnt2prod where emp_id = ? and project_role in ("
				+ "'Account Manager', 'PROJ MANAGER', 'VICE PRESIDENT','Project Manager','Project Manager ERP','Delivery Head','Solution Manager', "
				+ "'Process Owner','SDM','ACMGR', 'MGR','DELIVERY MANAGER','SALES MANAGER','ASSISTANT GENERAL MANAGER','SENIOR MANAGER')"
				+ ") and is_approved = 0 and is_rejected = 0",
			new Object[]{empId},new RowMapper<ApproveFlightRequestModel>() {
			public ApproveFlightRequestModel mapRow(ResultSet rs, int rowNum) throws SQLException {
				ApproveFlightRequestModel approveFlightRequestModel = new ApproveFlightRequestModel();
				//approveFlightRequestModel.setFlightReqId(rs.getInt("FLIGHT_REQUEST_ID"));
				approveFlightRequestModel.setRrqNo(rs.getString("RRQ_NO"));
				//approveFlightRequestModel.setCreationTimeStamp(rs.getString("CREATION_TIMESTAMP"));
    			return approveFlightRequestModel;
    		}
			
		});
	}


	@Override
	public List<ApproveCarRequestModel> approveCarReq(String empId) {
		// TODO Auto-generated method stub
		System.out.println("inside approve car request dao");
		System.out.println("empId = " + empId);
		
		
		return  tmsJdbcTemplate.query("select distinct RRQ_NO from XXRS_TMS_CAR_REQUEST_T where project_id in "
				+ "(	select project_id from xxrs.xxrs_res_alloc_data_temp@rsdnt2prod where emp_id = ? and project_role in ("
				+ "'Account Manager', 'PROJ MANAGER', 'VICE PRESIDENT','Project Manager','Project Manager ERP','Delivery Head','Solution Manager', "
				+ "'Process Owner','SDM','ACMGR', 'MGR','DELIVERY MANAGER','SALES MANAGER','ASSISTANT GENERAL MANAGER','SENIOR MANAGER')"
				+ ") and is_approved = 0 and is_rejected = 0",
			new Object[]{empId},new RowMapper<ApproveCarRequestModel>() {
			public ApproveCarRequestModel mapRow(ResultSet rs, int rowNum) throws SQLException {
				ApproveCarRequestModel approveCarRequestModel = new ApproveCarRequestModel();
				//approveCarRequestModel.setCarReqId(rs.getInt("CAR_REQUEST_ID"));
				approveCarRequestModel.setRrqNo(rs.getString("RRQ_NO"));
				//approveCarRequestModel.setCreationTimeStamp(rs.getString("CREATION_TIMESTAMP"));
    			return approveCarRequestModel;
    		}
			
		});
	}

}
