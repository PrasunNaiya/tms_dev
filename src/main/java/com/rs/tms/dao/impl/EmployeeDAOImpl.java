package com.rs.tms.dao.impl;

import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.EmployeeDAO;
import com.rs.tms.model.EmployeeDetailsModel;
import com.rs.tms.model.TMSModel;

@Component
public class EmployeeDAOImpl implements EmployeeDAO {
	
	private JdbcTemplate tmsJdbcTemplate;
	
	@Autowired
    public void setDataSource(@Qualifier("tmsDataSource")DataSource  dataSource) {
        this.tmsJdbcTemplate = new JdbcTemplate(dataSource);        		
    }

	@Override
	public TMSModel fetchEmployeeDetails(String empId) {
		System.out.println("inside  fetchEmployeeDetails DAO..");
		System.out.println("empId = " + empId);
		return this.tmsJdbcTemplate.queryForObject(
				"SELECT people.EMPLOYEE_NUMBER, people.FULL_NAME, to_char(people.DATE_OF_BIRTH, 'MM/dd/YYYY') DATE_OF_BIRTH, "
				+ "people.SEX, substr(position.NAME,0,instr(position.NAME,'.') - 1) DESIGNATION "
				+ "FROM hr.per_all_people_f@RSDNT2PROD people,"
				+ "hr.per_all_assignments_f@RSDNT2PROD assignment, "
				+ "hr.per_all_positions@RSDNT2PROD position "
				+ "WHERE people.person_id = assignment.person_id "
				+ "AND people.business_group_id = 81 "
				+ "AND TRUNC (SYSDATE) BETWEEN NVL (TRUNC (people.effective_start_date), TRUNC (SYSDATE) - 1) "
				+ "AND NVL (TRUNC (people.effective_end_date), TRUNC (SYSDATE) + 1) "
				+ "AND people.current_employee_flag = 'Y' "
				+ "AND TRUNC (SYSDATE) BETWEEN NVL (TRUNC (assignment.effective_start_date), TRUNC (SYSDATE) - 1) "
				+ "AND NVL (TRUNC (assignment.effective_end_date), TRUNC (SYSDATE) + 1) "
				+ "AND assignment.primary_flag = 'Y'  "
				+ "AND assignment.position_id = position.position_id  "
				+ "AND people.employee_number  = ?",
	        new Object[]{empId},
	        new RowMapper<TMSModel>() {
	            public TMSModel mapRow(ResultSet rs, int rowNum) throws SQLException {
	            	System.out.println(rs.getString("EMPLOYEE_NUMBER") + "\n" + rs.getString("FULL_NAME"));
	            	EmployeeDetailsModel employee = new EmployeeDetailsModel();
	            	employee.setEmpId(rs.getString("EMPLOYEE_NUMBER"));
	            	employee.setFullName(rs.getString("FULL_NAME"));
	            	employee.setDateOfBirth(rs.getString("DATE_OF_BIRTH"));
	            	employee.setGender(rs.getString("SEX"));
	            	employee.setDesignation(rs.getString("DESIGNATION"));
	                return employee;
	            }
	        }
        );
	}

}
