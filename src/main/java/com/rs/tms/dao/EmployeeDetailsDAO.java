package com.rs.tms.dao;

import com.rs.tms.model.EmployeeDetails;

public interface EmployeeDetailsDAO {

	public String employeeDetails(EmployeeDetails employeeDetails); 
}
