package com.rs.tms.dao;

import java.util.List;

import com.rs.tms.domain.Airport;

public interface AirportDAO {

	public List<Airport> fetchAirports(int cityId);
}
