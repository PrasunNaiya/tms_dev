package com.rs.tms.dao;

import java.util.List;

import org.springframework.jdbc.core.JdbcTemplate;

import com.rs.tms.domain.Country;

public interface CountryDAO {
	
	public List<Country> fetchCountries();
	public Country fetchCountry(String countryName);
}
