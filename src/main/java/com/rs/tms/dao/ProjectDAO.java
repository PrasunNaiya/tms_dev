package com.rs.tms.dao;

import java.util.List;

import com.rs.tms.domain.Project;

public interface ProjectDAO {

	public List<Project> fetchProjects();
	public List<Project> fetchProcesses();
	
}
