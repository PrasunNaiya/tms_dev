package com.rs.tms.dao;

import java.util.List;

import com.rs.tms.domain.City;

public interface CityDAO {

	public List<City> fetchCities(int countryId);
	public City fetchCity(String cityName);
}
