package com.rs.tms.service;

import java.util.List;

import com.rs.tms.domain.Airport;
import com.rs.tms.domain.City;
import com.rs.tms.domain.Country;
import com.rs.tms.domain.Customer;
import com.rs.tms.domain.Project;
import com.rs.tms.model.CarRequestTModel;
import com.rs.tms.model.ComapnionDetails;
import com.rs.tms.model.EmployeeDetails;
import com.rs.tms.model.FlightRequestTModel;
import com.rs.tms.model.HotelRequestTModel;
import com.rs.tms.model.RequestInformationModel;
import com.rs.tms.model.TMSModel;

public interface TMSService {

	//Travel Request Form Related
	public TMSModel fetchEmployeeDetails(String empId);
	public List<Country> fetchCountries();
	public Country fetchCountry(String countryName);
	public List<City> fetchCities(int countryId);
	public City fetchCity(String cityName);
	public List<Airport> fetchAirports(int cityId);
	public List<Project> fetchProjects();
	public List<Project> fetchProcesses();
	public Customer fetchCustomer(int projectId);
	// view request service
	public List<RequestInformationModel> fetchRequests(String empId);
	public EmployeeDetails fetchEmployeeRequest(int EmpId);
	public List<FlightRequestTModel> fetchFlightDetails(int EmpId);
	public List<HotelRequestTModel> fetchHotelDetails(int EmpId);
	public List<CarRequestTModel> fetchCarDetails(int EmpId);
	public List<ComapnionDetails> fetchComapnionDetails(int EmpId);
}
