package com.rs.tms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.ApprovalHomeDAO;
import com.rs.tms.model.ApprovalHomeModel;
import com.rs.tms.model.ApproveCarRequestModel;
import com.rs.tms.model.ApproveFlightRequestModel;
import com.rs.tms.model.ApproveHotelRequestModel;
import com.rs.tms.service.ApprovalHomeService;
@Component
public class ApprovalHomeServiceImpl implements ApprovalHomeService{
    @Autowired
	ApprovalHomeDAO approvalHomeDAO;
	
	@Override
	public List<ApprovalHomeModel> getPendingRequests(String empId) {
		// TODO Auto-generated method stub
		return approvalHomeDAO.getPendingRequests(empId);
	}

	@Override
	public List<ApproveHotelRequestModel> approveHotelReq(String empId) {
		// TODO Auto-generated method stub
		return approvalHomeDAO.approveHotelReq(empId);
	}

	@Override
	public List<ApproveFlightRequestModel> approveFlightReq(String empId) {
		// TODO Auto-generated method stub
		return approvalHomeDAO.approveFlightReq(empId);
	}

	@Override
	public List<ApproveCarRequestModel> approveCarReq(String empId) {
		// TODO Auto-generated method stub
		return approvalHomeDAO.approveCarReq(empId);
	}

}
