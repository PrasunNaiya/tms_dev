package com.rs.tms.service.impl;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.rs.tms.dao.CarDetailsDAO;
import com.rs.tms.dao.EmployeeDetailsDAO;
import com.rs.tms.dao.FlightDetailsDAO;
import com.rs.tms.dao.HotelDetailsDAO;
import com.rs.tms.model.CarRequestTModel;
import com.rs.tms.model.ComapnionDetails;
import com.rs.tms.model.EmployeeDetails;
import com.rs.tms.model.FlightRequestTModel;
import com.rs.tms.model.HotelRequestTModel;
import com.rs.tms.dao.CompanionDAO;
import com.rs.tms.service.TravelRequestSubmitService;
@Component
public class TravelRequestSubmitServiceImpl implements TravelRequestSubmitService {
	@Autowired
	EmployeeDetailsDAO employeeDetailsDAO;
	@Autowired
	HotelDetailsDAO hotelDetailsDAO;
	@Autowired
	CarDetailsDAO carDetailsDAO;
	@Autowired
	FlightDetailsDAO flightDetailsDAO;
	@Autowired
	CompanionDAO companionDAO;
	
    String message = null;
    String messageForHotel = null;
    String messageForCompanion = null;
    String pattern = "yyyy/MM/dd";
    Date passportExpDate,visaExpDate;
	@Override
	public String employeeDetails(EmployeeDetails employeeDetails) throws ParseException {
		// TODO Auto-generated method stub
		 SimpleDateFormat format = new SimpleDateFormat(pattern);
		
			/* passportExpDate = format.parse(employeeDetails.getPassportExpiryDate().toString());
             visaExpDate = format.parse(employeeDetails.getVisaExpiryDate().toString());*/
           //  employeeDetails.setPassportExpiryDate(passportExpiryDate);
		 
		System.out.println(employeeDetails.getContactNo());
		System.out.println(	employeeDetails.getDesignation());
		message = employeeDetailsDAO.employeeDetails(employeeDetails);
		return message;
	}
	@Override
	public String hotelDetails(List<HotelRequestTModel> HotelDetailsList) {
		// TODO Auto-generated method stub
		messageForHotel = hotelDetailsDAO.hotelDetails(HotelDetailsList);
		return messageForHotel;
	}
	@Override
	public String carDetails(List<CarRequestTModel> carDetailsList) {
		// TODO Auto-generated method stub
		return carDetailsDAO.carDetails(carDetailsList);
	}
	@Override
	public String flightDetails(List<FlightRequestTModel> flightDetailsList) {
		// TODO Auto-generated method stub
		return flightDetailsDAO.flightDetails(flightDetailsList);
	}
	@Override
	public String companionDetails(List<ComapnionDetails> comapnionDetailsList) {
		// TODO Auto-generated method stub
		messageForCompanion = companionDAO.companionDetails(comapnionDetailsList);
		return messageForCompanion;
	}

}
