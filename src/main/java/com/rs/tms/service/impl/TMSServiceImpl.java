package com.rs.tms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.rs.tms.dao.AirportDAO;
import com.rs.tms.dao.CityDAO;
import com.rs.tms.dao.CountryDAO;
import com.rs.tms.dao.CustomerDAO;
import com.rs.tms.dao.EmployeeDAO;
import com.rs.tms.dao.ProjectDAO;
import com.rs.tms.dao.TmsViewTravelRequestDAO;
import com.rs.tms.domain.Airport;
import com.rs.tms.domain.City;
import com.rs.tms.domain.Country;
import com.rs.tms.domain.Customer;
import com.rs.tms.domain.Project;
import com.rs.tms.model.CarRequestTModel;
import com.rs.tms.model.ComapnionDetails;
import com.rs.tms.model.EmployeeDetails;
import com.rs.tms.model.EmployeeDetailsModel;
import com.rs.tms.model.FlightRequestTModel;
import com.rs.tms.model.HotelRequestTModel;
import com.rs.tms.model.RequestInformationModel;
import com.rs.tms.model.TMSModel;
import com.rs.tms.service.TMSService;

@Service
public class TMSServiceImpl implements TMSService {
	
	@Autowired
	EmployeeDAO employeeDAO;
	
	@Autowired
	CountryDAO countryDAO;
	
	@Autowired
	CityDAO cityDAO;
	
	@Autowired
	AirportDAO airportDAO;
	
	@Autowired
	ProjectDAO projectDAO;
	
	@Autowired
	CustomerDAO customerDAO;
	
	@Autowired
	TmsViewTravelRequestDAO tmsViewTravelRequestDAO;

	//Travel Request Form Related
	@Override
	public TMSModel fetchEmployeeDetails(String empId) {
		System.out.println("inside fetchEmployeeDetails service..");
		EmployeeDetailsModel employeeDetailsModel = (EmployeeDetailsModel) employeeDAO.fetchEmployeeDetails(empId);
		return employeeDetailsModel;
	}

	@Override
	public List<Country> fetchCountries() {
		System.out.println("inside fetchCountries service..");
		return countryDAO.fetchCountries();
	}

	@Override
	public Country fetchCountry(String countryName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<City> fetchCities(int countryId) {
		System.out.println("inside fetchCities service..");
		return cityDAO.fetchCities(countryId);
	}

	@Override
	public City fetchCity(String cityName) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public List<Airport> fetchAirports(int cityId) {
		System.out.println("inside fetchAirports service..");
		return airportDAO.fetchAirports(cityId);
	}

	@Override
	public List<Project> fetchProjects() {
		return projectDAO.fetchProjects();
	}

	@Override
	public List<Project> fetchProcesses() {
		return projectDAO.fetchProcesses();
	}

	@Override
	public Customer fetchCustomer(int projectId) {
		return customerDAO.fetchCustomer(projectId);
	}

	//view request Service
	
	@Override
	public List<RequestInformationModel> fetchRequests(String empId) {
		// TODO Auto-generated method stub
		return tmsViewTravelRequestDAO.fetchRequests(empId);
	}

	@Override
	public EmployeeDetails fetchEmployeeRequest(int travelReqId) {
		// TODO Auto-generated method stub
		return tmsViewTravelRequestDAO.fetchEmployeeRequest(travelReqId);
	}

	@Override
	public List<FlightRequestTModel> fetchFlightDetails(int flightReqId) {
		// TODO Auto-generated method stub
		return tmsViewTravelRequestDAO.fetchFlightDetails(flightReqId);
	}

	@Override
	public List<HotelRequestTModel> fetchHotelDetails(int hotelReqId) {
		// TODO Auto-generated method stub
		return tmsViewTravelRequestDAO.fetchHotelDetails(hotelReqId);
	}

	@Override
	public List<CarRequestTModel> fetchCarDetails(int carReqId) {
		// TODO Auto-generated method stub
		return tmsViewTravelRequestDAO.fetchCarDetails(carReqId);
	}

	@Override
	public List<ComapnionDetails> fetchComapnionDetails(int companionReqId) {
		// TODO Auto-generated method stub
		return tmsViewTravelRequestDAO.fetchComapnionDetails(companionReqId);
	}

}
