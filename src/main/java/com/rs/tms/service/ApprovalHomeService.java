package com.rs.tms.service;

import java.util.List;

import com.rs.tms.model.ApprovalHomeModel;
import com.rs.tms.model.ApproveCarRequestModel;
import com.rs.tms.model.ApproveFlightRequestModel;
import com.rs.tms.model.ApproveHotelRequestModel;

public interface ApprovalHomeService {
	
	public List<ApprovalHomeModel> getPendingRequests(String empId);
	public List<ApproveHotelRequestModel> approveHotelReq(String empId);
	public List<ApproveFlightRequestModel> approveFlightReq(String empId);
	public List<ApproveCarRequestModel> approveCarReq(String empId);
}
