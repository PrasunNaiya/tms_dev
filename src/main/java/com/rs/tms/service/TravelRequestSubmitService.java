package com.rs.tms.service;

import java.text.ParseException;
import java.util.List;

import com.rs.tms.model.CarRequestTModel;
import com.rs.tms.model.ComapnionDetails;
import com.rs.tms.model.EmployeeDetails;
import com.rs.tms.model.FlightRequestTModel;
import com.rs.tms.model.HotelRequestTModel;

public interface TravelRequestSubmitService  {
	public String employeeDetails(EmployeeDetails employeeDetails)throws ParseException ;
	public String hotelDetails(List<HotelRequestTModel> HotelDetailsList);
	public String carDetails(List<CarRequestTModel> carDetailsList);
	public String flightDetails(List<FlightRequestTModel> flightDetailsList);
	public String companionDetails(List<ComapnionDetails> comapnionDetailsList);
	
}
