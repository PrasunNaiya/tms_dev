package com.rs.tms.model;

public class ApproveFlightRequestModel {

	private String rrqNo;
	private int flightReqId,isAppropved,isRejected;
	private String empId; 
	private String creationTimeStamp;
	public String getRrqNo() {
		return rrqNo;
	}
	public void setRrqNo(String rrqNo) {
		this.rrqNo = rrqNo;
	}
	
	public int getFlightReqId() {
		return flightReqId;
	}
	public void setFlightReqId(int flightReqId) {
		this.flightReqId = flightReqId;
	}
	public int getIsAppropved() {
		return isAppropved;
	}
	public void setIsAppropved(int isAppropved) {
		this.isAppropved = isAppropved;
	}
	public int getIsRejected() {
		return isRejected;
	}
	public void setIsRejected(int isRejected) {
		this.isRejected = isRejected;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getCreationTimeStamp() {
		return creationTimeStamp;
	}
	public void setCreationTimeStamp(String creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	
	
	
}
