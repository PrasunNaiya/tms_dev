package com.rs.tms.model;

import java.util.List;

public class TravelRequestTModel {
	
	/*private int travelRequestId,contactNo;
	private String rrqNo,empId,dob,travelType,mealPreference,passportNo,passportExpiryDate,visaType,visaNo,visaExpiryDate,createdBy,modifiedBy,creationTimeStamp,modificationTimeStamp;
	private int isBudgeted,isBillable,isApproved,isRejected,isCostApproved,isCostRejected,isActive;
	private String  designation,gender,empName;*/
	
	private EmployeeDetails empDtls;
	private List<FlightRequestTModel> flightDtlsList;
	private List<HotelRequestTModel> hotelDtlsList;
	private List<CarRequestTModel> carDtlsList;
	private List<ComapnionDetails> companionDtlsList;
	
	public EmployeeDetails getEmpDtls() {
		return empDtls;
	}
	public void setEmpDtls(EmployeeDetails empDtls) {
		this.empDtls = empDtls;
	}
	public List<FlightRequestTModel> getFlightDtlsList() {
		return flightDtlsList;
	}
	public void setFlightDtlsList(List<FlightRequestTModel> flightDtlsList) {
		this.flightDtlsList = flightDtlsList; 
	}
	public List<HotelRequestTModel> getHotelDtlsList() {
		return hotelDtlsList;
	}
	public void setHotelDtlsList(List<HotelRequestTModel> hotelDtlsList) {
		this.hotelDtlsList = hotelDtlsList;
	}
	public List<CarRequestTModel> getCarDtlsList() {
		return carDtlsList;
	}
	public void setCarDtlsList(List<CarRequestTModel> carDtlsList) {
		this.carDtlsList = carDtlsList;
	}
	public List<ComapnionDetails> getCompanionDtlsList() {
		return companionDtlsList;
	}
	public void setCompanionDtlsList(List<ComapnionDetails> companionDtlsList) {
		this.companionDtlsList = companionDtlsList;
	}
	
	
	
	

}