package com.rs.tms.model;

import java.util.List;

public class L0Model {

	private String s1;
	private String s2;
	private L1Model l1;
	private L2Model l2;
	private List<L3Model> l3List;
	
	public String getS1() {
		return s1;
	}
	public String getS2() {
		return s2;
	}
	public L1Model getL1() {
		return l1;
	}
	public L2Model getL2() {
		return l2;
	}
	public List<L3Model> getL3List() {
		return l3List;
	}
	public void setS1(String s1) {
		this.s1 = s1;
	}
	public void setS2(String s2) {
		this.s2 = s2;
	}
	public void setL1(L1Model l1) {
		this.l1 = l1;
	}
	public void setL2(L2Model l2) {
		this.l2 = l2;
	}
	public void setL3List(List<L3Model> l3List) {
		this.l3List = l3List;
	}
	
	
}
