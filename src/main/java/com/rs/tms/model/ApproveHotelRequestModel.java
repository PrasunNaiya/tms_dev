package com.rs.tms.model;

public class ApproveHotelRequestModel {
	
	private String rrqNo;
	private int hotelReqId,isAppropved,isRejected;
	private String empId; 
	private String creationTimeStamp;
	public String getRrqNo() {
		return rrqNo;
	}
	public void setRrqNo(String rrqNo) {
		this.rrqNo = rrqNo;
	}
	public int getHotelReqId() {
		return hotelReqId;
	}
	public void setHotelReqId(int hotelReqId) {
		this.hotelReqId = hotelReqId;
	}
	public int getIsAppropved() {
		return isAppropved;
	}
	public void setIsAppropved(int isAppropved) {
		this.isAppropved = isAppropved;
	}
	public int getIsRejected() {
		return isRejected;
	}
	public void setIsRejected(int isRejected) {
		this.isRejected = isRejected;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getCreationTimeStamp() {
		return creationTimeStamp;
	}
	public void setCreationTimeStamp(String creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	
	
	

}
