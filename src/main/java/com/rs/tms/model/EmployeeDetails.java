package com.rs.tms.model;

import java.util.Date;

public class EmployeeDetails extends TMSModel{
	
	private int travelRequestId;
	private String rrqNo,empId,dob,travelType,mealPreference,passportNo,visaType,visaNo,createdBy,modifiedBy,creationTimeStamp,modificationTimeStamp,contactNo;
	private int isBudgeted,isBillable,isApproved,isRejected,isCostApproved,isCostRejected,isActive;
	private String  designation,gender,empName;
	private String passportExpiryDate,visaExpiryDate;
	private String nameOnPassport,addrOnPassport,passportIssuePlace,passportIssueDate,encrCheck,visaIssueDate,
	pitition,validity,nomineeName,emrContactPersonName,emrContactPersonAddr,emrContactPersonNo;
	
	public int getTravelRequestId() {
		return travelRequestId;
	}
	public void setTravelRequestId(int travelRequestId) {
		this.travelRequestId = travelRequestId;
	}
	
	public String getContactNo() {
		return contactNo;
	}
	public void setContactNo(String contactNo) {
		this.contactNo = contactNo;
	}
	public String getRrqNo() {
		return rrqNo;
	}
	public void setRrqNo(String rrqNo) {
		this.rrqNo = rrqNo;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getTravelType() {
		return travelType;
	}
	public void setTravelType(String travelType) {
		this.travelType = travelType;
	}
	public String getMealPreference() {
		return mealPreference;
	}
	public void setMealPreference(String mealPreference) {
		this.mealPreference = mealPreference;
	}
	public String getPassportNo() {
		return passportNo;
	}
	public void setPassportNo(String passportNo) {
		this.passportNo = passportNo;
	}
	
	public String getVisaType() {
		return visaType;
	}
	public void setVisaType(String visaType) {
		this.visaType = visaType;
	}
	public String getVisaNo() {
		return visaNo;
	}
	public void setVisaNo(String visaNo) {
		this.visaNo = visaNo;
	}
	
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getCreationTimeStamp() {
		return creationTimeStamp;
	}
	public void setCreationTimeStamp(String creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	public String getModificationTimeStamp() {
		return modificationTimeStamp;
	}
	public void setModificationTimeStamp(String modificationTimeStamp) {
		this.modificationTimeStamp = modificationTimeStamp;
	}
	public int getIsBudgeted() {
		return isBudgeted;
	}
	public void setIsBudgeted(int isBudgeted) {
		this.isBudgeted = isBudgeted;
	}
	public int getIsBillable() {
		return isBillable;
	}
	public void setIsBillable(int isBillable) {
		this.isBillable = isBillable;
	}
	public int getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(int isApproved) {
		this.isApproved = isApproved;
	}
	public int getIsRejected() {
		return isRejected;
	}
	public void setIsRejected(int isRejected) {
		this.isRejected = isRejected;
	}
	public int getIsCostApproved() {
		return isCostApproved;
	}
	public void setIsCostApproved(int isCostApproved) {
		this.isCostApproved = isCostApproved;
	}
	public int getIsCostRejected() {
		return isCostRejected;
	}
	public void setIsCostRejected(int isCostRejected) {
		this.isCostRejected = isCostRejected;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getEmpName() {
		return empName;
	}
	public void setEmpName(String empName) {
		this.empName = empName;
	}
	public String getPassportExpiryDate() {
		return passportExpiryDate;
	}
	public void setPassportExpiryDate(String passportExpiryDate) {
		this.passportExpiryDate = passportExpiryDate;
	}
	public String getVisaExpiryDate() {
		return visaExpiryDate;
	}
	public void setVisaExpiryDate(String visaExpiryDate) {
		this.visaExpiryDate = visaExpiryDate;
	}
	public String getNameOnPassport() {
		return nameOnPassport;
	}
	public void setNameOnPassport(String nameOnPassport) {
		this.nameOnPassport = nameOnPassport;
	}
	public String getAddrOnPassport() {
		return addrOnPassport;
	}
	public void setAddrOnPassport(String addrOnPassport) {
		this.addrOnPassport = addrOnPassport;
	}
	public String getPassportIssuePlace() {
		return passportIssuePlace;
	}
	public void setPassportIssuePlace(String passportIssuePlace) {
		this.passportIssuePlace = passportIssuePlace;
	}
	public String getPassportIssueDate() {
		return passportIssueDate;
	}
	public void setPassportIssueDate(String passportIssueDate) {
		this.passportIssueDate = passportIssueDate;
	}
	public String getEncrCheck() {
		return encrCheck;
	}
	public void setEncrCheck(String encrCheck) {
		this.encrCheck = encrCheck;
	}
	public String getVisaIssueDate() {
		return visaIssueDate;
	}
	public void setVisaIssueDate(String visaIssueDate) {
		this.visaIssueDate = visaIssueDate;
	}
	public String getPitition() {
		return pitition;
	}
	public void setPitition(String pitition) {
		this.pitition = pitition;
	}
	public String getValidity() {
		return validity;
	}
	public void setValidity(String validity) {
		this.validity = validity;
	}
	public String getNomineeName() {
		return nomineeName;
	}
	public void setNomineeName(String nomineeName) {
		this.nomineeName = nomineeName;
	}
	
	public String getEmrContactPersonName() {
		return emrContactPersonName;
	}
	public void setEmrContactPersonName(String emrContactPersonName) {
		this.emrContactPersonName = emrContactPersonName;
	}
	public String getEmrContactPersonAddr() {
		return emrContactPersonAddr;
	}
	public void setEmrContactPersonAddr(String emrContactPersonAddr) {
		this.emrContactPersonAddr = emrContactPersonAddr;
	}
	public String getEmrContactPersonNo() {
		return emrContactPersonNo;
	}
	public void setEmrContactPersonNo(String emrContactPersonNo) {
		this.emrContactPersonNo = emrContactPersonNo;
	}
	
	
	

}
