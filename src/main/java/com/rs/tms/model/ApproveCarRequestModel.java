package com.rs.tms.model;

public class ApproveCarRequestModel {
	
	private String rrqNo;
	private int carReqId,isAppropved,isRejected;
	private String empId; 
	private String creationTimeStamp;
	public String getRrqNo() {
		return rrqNo;
	}
	public void setRrqNo(String rrqNo) {
		this.rrqNo = rrqNo;
	}
	
	public int getCarReqId() {
		return carReqId;
	}
	public void setCarReqId(int carReqId) {
		this.carReqId = carReqId;
	}
	public int getIsAppropved() {
		return isAppropved;
	}
	public void setIsAppropved(int isAppropved) {
		this.isAppropved = isAppropved;
	}
	public int getIsRejected() {
		return isRejected;
	}
	public void setIsRejected(int isRejected) {
		this.isRejected = isRejected;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getCreationTimeStamp() {
		return creationTimeStamp;
	}
	public void setCreationTimeStamp(String creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	
	

}
