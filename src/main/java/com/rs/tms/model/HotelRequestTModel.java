package com.rs.tms.model;

public class HotelRequestTModel {
	
	private int hotelRequestId,countryId,cityId,projectId,processId,cost;
	private String rrqNo,prefHotel,checkInDate,checkOutDate,rewardMemberNo,clientName,travellingLocationAddr,travelPurpose,projectName,prospectName,tripMode,notes,currency;
	private int isBudgeted,isBillable,isApproved,isRejected,isCostApproved,isCostRejected,isActive;
	private String createdBy,modifiedBy,creationTimeStamp,modificationTimeStamp;
	public int getHotelRequestId() {
		return hotelRequestId;
	}
	public void setHotelRequestId(int hotelRequestId) {
		this.hotelRequestId = hotelRequestId;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	
	public int getProcessId() {
		return processId;
	}
	public void setProcessId(int processId) {
		this.processId = processId;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public String getRrqNo() {
		return rrqNo;
	}
	public void setRrqNo(String rrqNo) {
		this.rrqNo = rrqNo;
	}
	public String getPrefHotel() {
		return prefHotel;
	}
	public void setPrefHotel(String prefHotel) {
		this.prefHotel = prefHotel;
	}
	public String getCheckInDate() {
		return checkInDate;
	}
	public void setCheckInDate(String checkInDate) {
		this.checkInDate = checkInDate;
	}
	public String getCheckOutDate() {
		return checkOutDate;
	}
	public void setCheckOutDate(String checkOutDate) {
		this.checkOutDate = checkOutDate;
	}
	public String getRewardMemberNo() {
		return rewardMemberNo;
	}
	public void setRewardMemberNo(String rewardMemberNo) {
		this.rewardMemberNo = rewardMemberNo;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getTravellingLocationAddr() {
		return travellingLocationAddr;
	}
	public void setTravellingLocationAddr(String travellingLocationAddr) {
		this.travellingLocationAddr = travellingLocationAddr;
	}
	public String getTravelPurpose() {
		return travelPurpose;
	}
	public void setTravelPurpose(String travelPurpose) {
		this.travelPurpose = travelPurpose;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProspectName() {
		return prospectName;
	}
	public void setProspectName(String prospectName) {
		this.prospectName = prospectName;
	}
	public String getTripMode() {
		return tripMode;
	}
	public void setTripMode(String tripMode) {
		this.tripMode = tripMode;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public int getIsBudgeted() {
		return isBudgeted;
	}
	public void setIsBudgeted(int isBudgeted) {
		this.isBudgeted = isBudgeted;
	}
	public int getIsBillable() {
		return isBillable;
	}
	public void setIsBillable(int isBillable) {
		this.isBillable = isBillable;
	}
	public int getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(int isApproved) {
		this.isApproved = isApproved;
	}
	public int getIsRejected() {
		return isRejected;
	}
	public void setIsRejected(int isRejected) {
		this.isRejected = isRejected;
	}
	public int getIsCostApproved() {
		return isCostApproved;
	}
	public void setIsCostApproved(int isCostApproved) {
		this.isCostApproved = isCostApproved;
	}
	public int getIsCostRejected() {
		return isCostRejected;
	}
	public void setIsCostRejected(int isCostRejected) {
		this.isCostRejected = isCostRejected;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getCreationTimeStamp() {
		return creationTimeStamp;
	}
	public void setCreationTimeStamp(String creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	public String getModificationTimeStamp() {
		return modificationTimeStamp;
	}
	public void setModificationTimeStamp(String modificationTimeStamp) {
		this.modificationTimeStamp = modificationTimeStamp;
	}
	
	
}

