package com.rs.tms.model;

public class FlightRequestTModel {
  private int flightRequestId,srCountryId,srCityId,srAirportId,dsCountryId,dsCityId,dsAirportId,cost;
  private int isBudgeted,isBillable,isApproved,isRejected,isCostApproved,isCostRejected,isActive;
  private String rrqNo,deptDate,deptTime,arrivalTime,seatPreference,prefAirlines,frquentFlierNo,clientName,travellingLocationAddr,travelPurpose,projectId,processId,projectName,prospectName,tripMode,notes,currency;
  private String createdBy,modifiedBy,creationTimeStamp,modificationTimeStamp;
  private int portOfEntry;
  private String frequentFlierAirline,frequentFlierFlightNo ;
  private String clientAddr;
  private int clientContactNo;
	
public int getFlightRequestId() {
	return flightRequestId;
}
public void setFlightRequestId(int flightRequestId) {
	this.flightRequestId = flightRequestId;
}
public int getSrCountryId() {
	return srCountryId;
}
public void setSrCountryId(int srCountryId) {
	this.srCountryId = srCountryId;
}
public int getSrCityId() {
	return srCityId;
}
public void setSrCityId(int srCityId) {
	this.srCityId = srCityId;
}
public int getSrAirportId() {
	return srAirportId;
}
public void setSrAirportId(int srAirportId) {
	this.srAirportId = srAirportId;
}
public int getDsCountryId() {
	return dsCountryId;
}
public void setDsCountryId(int dsCountryId) {
	this.dsCountryId = dsCountryId;
}
public int getDsCityId() {
	return dsCityId;
}
public void setDsCityId(int dsCityId) {
	this.dsCityId = dsCityId;
}
public int getDsAirportId() {
	return dsAirportId;
}
public void setDsAirportId(int dsAirportId) {
	this.dsAirportId = dsAirportId;
}
public int getCost() {
	return cost;
}
public void setCost(int cost) {
	this.cost = cost;
}
public int getIsBudgeted() {
	return isBudgeted;
}
public void setIsBudgeted(int isBudgeted) {
	this.isBudgeted = isBudgeted;
}
public int getIsBillable() {
	return isBillable;
}
public void setIsBillable(int isBillable) {
	this.isBillable = isBillable;
}
public int getIsApproved() {
	return isApproved;
}
public void setIsApproved(int isApproved) {
	this.isApproved = isApproved;
}
public int getIsRejected() {
	return isRejected;
}
public void setIsRejected(int isRejected) {
	this.isRejected = isRejected;
}
public int getIsCostApproved() {
	return isCostApproved;
}
public void setIsCostApproved(int isCostApproved) {
	this.isCostApproved = isCostApproved;
}
public int getIsCostRejected() {
	return isCostRejected;
}
public void setIsCostRejected(int isCostRejected) {
	this.isCostRejected = isCostRejected;
}
public int getIsActive() {
	return isActive;
}
public void setIsActive(int isActive) {
	this.isActive = isActive;
}
public String getRrqNo() {
	return rrqNo;
}
public void setRrqNo(String rrqNo) {
	this.rrqNo = rrqNo;
}
public String getDeptDate() {
	return deptDate;
}
public void setDeptDate(String deptDate) {
	this.deptDate = deptDate;
}
public String getDeptTime() {
	return deptTime;
}
public void setDeptTime(String deptTime) {
	this.deptTime = deptTime;
}
public String getArrivalTime() {
	return arrivalTime;
}
public void setArrivalTime(String arrivalTime) {
	this.arrivalTime = arrivalTime;
}
public String getSeatPreference() {
	return seatPreference;
}
public void setSeatPreference(String seatPreference) {
	this.seatPreference = seatPreference;
}
public String getPrefAirlines() {
	return prefAirlines;
}
public void setPrefAirlines(String prefAirlines) {
	this.prefAirlines = prefAirlines;
}
public String getFrquentFlierNo() {
	return frquentFlierNo;
}
public void setFrquentFlierNo(String frquentFlierNo) {
	this.frquentFlierNo = frquentFlierNo;
}
public String getClientName() {
	return clientName;
}
public void setClientName(String clientName) {
	this.clientName = clientName;
}
public String getTravellingLocationAddr() {
	return travellingLocationAddr;
}
public void setTravellingLocationAddr(String travellingLocationAddr) {
	this.travellingLocationAddr = travellingLocationAddr;
}
public String getTravelPurpose() {
	return travelPurpose;
}
public void setTravelPurpose(String travelPurpose) {
	this.travelPurpose = travelPurpose;
}
public String getProjectId() {
	return projectId;
}
public void setProjectId(String projectId) {
	this.projectId = projectId;
}
public String getProjectName() {
	return projectName;
}
public void setProjectName(String projectName) {
	this.projectName = projectName;
}

public String getProcessId() {
	return processId;
}
public void setProcessId(String processId) {
	this.processId = processId;
}
public String getProspectName() {
	return prospectName;
}
public void setProspectName(String prospectName) {
	this.prospectName = prospectName;
}
public String getTripMode() {
	return tripMode;
}
public void setTripMode(String tripMode) {
	this.tripMode = tripMode;
}
public String getNotes() {
	return notes;
}
public void setNotes(String notes) {
	this.notes = notes;
}
public String getCurrency() {
	return currency;
}
public void setCurrency(String currency) {
	this.currency = currency;
}
public String getCreatedBy() {
	return createdBy;
}
public void setCreatedBy(String createdBy) {
	this.createdBy = createdBy;
}
public String getModifiedBy() {
	return modifiedBy;
}
public void setModifiedBy(String modifiedBy) {
	this.modifiedBy = modifiedBy;
}
public String getCreationTimeStamp() {
	return creationTimeStamp;
}
public void setCreationTimeStamp(String creationTimeStamp) {
	this.creationTimeStamp = creationTimeStamp;
}
public String getModificationTimeStamp() {
	return modificationTimeStamp;
}
public void setModificationTimeStamp(String modificationTimeStamp) {
	this.modificationTimeStamp = modificationTimeStamp;
}
public int getPortOfEntry() {
	return portOfEntry;
}
public void setPortOfEntry(int portOfEntry) {
	this.portOfEntry = portOfEntry;
}
public String getFrequentFlierAirline() {
	return frequentFlierAirline;
}
public void setFrequentFlierAirline(String frequentFlierAirline) {
	this.frequentFlierAirline = frequentFlierAirline;
}
public String getFrequentFlierFlightNo() {
	return frequentFlierFlightNo;
}
public void setFrequentFlierFlightNo(String frequentFlierFlightNo) {
	this.frequentFlierFlightNo = frequentFlierFlightNo;
}
public String getClientAddr() {
	return clientAddr;
}
public void setClientAddr(String clientAddr) {
	this.clientAddr = clientAddr;
}
public int getClientContactNo() {
	return clientContactNo;
}
public void setClientContactNo(int clientContactNo) {
	this.clientContactNo = clientContactNo;
}

  
  
  
}
