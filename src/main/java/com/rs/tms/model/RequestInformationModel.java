package com.rs.tms.model;

public class RequestInformationModel {
	
	private String rrqNo;
	private int travelReqId;
	private String empId; 
	private String creationTimeStamp;
	
	public String getRrqNo() {
		return rrqNo;
	}
	public void setRrqNo(String rrqNo) {
		this.rrqNo = rrqNo;
	}
	public int getTravelReqId() {
		return travelReqId;
	}
	public void setTravelReqId(int travelReqId) {
		this.travelReqId = travelReqId;
	}
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getCreationTimeStamp() {
		return creationTimeStamp;
	}
	public void setCreationTimeStamp(String creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	
	
	

}
