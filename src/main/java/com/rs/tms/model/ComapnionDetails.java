package com.rs.tms.model;

public class ComapnionDetails {
	private int companionReqId;
	private String companionName,relation,cdateOfBirth,cpassportNo ,caddressOnPassport ,cpassportIssuePlace,cpassportIssueDate,cpassportExpiryDate,cECNRCheck, 
    cvisaType,cvisaNo,cvisaIssueDate,cvisaExpiryDate,cvalidity,cdateOfTravel;
    private String createdBy,modifiedBy,creationTimeStamp,modificationTimeStamp;
	private String rrqNo;
	private int isBudgeted,isBillable,isApproved,isRejected,isCostApproved,isCostRejected,isActive;
	
	
	public int getCompanionReqId() {
		return companionReqId;
	}

	public void setCompanionReqId(int companionReqId) {
		this.companionReqId = companionReqId;
	}

	

	public String getRrqNo() {
		return rrqNo;
	}

	public void setRrqNo(String rrqNo) {
		this.rrqNo = rrqNo;
	}

	public String getCompanionName() {
		return companionName;
	}

	public void setCompanionName(String companionName) {
		this.companionName = companionName;
	}

	public String getRelation() {
		return relation;
	}

	public void setRelation(String relation) {
		this.relation = relation;
	}

	public String getCdateOfBirth() {
		return cdateOfBirth;
	}

	public void setCdateOfBirth(String cdateOfBirth) {
		this.cdateOfBirth = cdateOfBirth;
	}

	public String getCpassportNo() {
		return cpassportNo;
	}

	public void setCpassportNo(String cpassportNo) {
		this.cpassportNo = cpassportNo;
	}

	public String getCaddressOnPassport() {
		return caddressOnPassport;
	}

	public void setCaddressOnPassport(String caddressOnPassport) {
		this.caddressOnPassport = caddressOnPassport;
	}

	public String getCpassportIssuePlace() {
		return cpassportIssuePlace;
	}

	public void setCpassportIssuePlace(String cpassportIssuePlace) {
		this.cpassportIssuePlace = cpassportIssuePlace;
	}



	public String getCpassportIssueDate() {
		return cpassportIssueDate;
	}

	public void setCpassportIssueDate(String cpassportIssueDate) {
		this.cpassportIssueDate = cpassportIssueDate;
	}

	public String getCpassportExpiryDate() {
		return cpassportExpiryDate;
	}

	public void setCpassportExpiryDate(String cpassportExpiryDate) {
		this.cpassportExpiryDate = cpassportExpiryDate;
	}

	public String getcECNRCheck() {
		return cECNRCheck;
	}

	public void setcECNRCheck(String cECNRCheck) {
		this.cECNRCheck = cECNRCheck;
	}

	public String getCvisaType() {
		return cvisaType;
	}

	public void setCvisaType(String cvisaType) {
		this.cvisaType = cvisaType;
	}

	public String getCvisaNo() {
		return cvisaNo;
	}

	public void setCvisaNo(String cvisaNo) {
		this.cvisaNo = cvisaNo;
	}

	public String getCvisaIssueDate() {
		return cvisaIssueDate;
	}

	public void setCvisaIssueDate(String cvisaIssueDate) {
		this.cvisaIssueDate = cvisaIssueDate;
	}

	public String getCvisaExpiryDate() {
		return cvisaExpiryDate;
	}

	public void setCvisaExpiryDate(String cvisaExpiryDate) {
		this.cvisaExpiryDate = cvisaExpiryDate;
	}

	public String getCvalidity() {
		return cvalidity;
	}

	public void setCvalidity(String cvalidity) {
		this.cvalidity = cvalidity;
	}

	public String getCdateOfTravel() {
		return cdateOfTravel;
	}

	public void setCdateOfTravel(String cdateOfTravel) {
		this.cdateOfTravel = cdateOfTravel;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public String getCreationTimeStamp() {
		return creationTimeStamp;
	}

	public void setCreationTimeStamp(String creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}

	public String getModificationTimeStamp() {
		return modificationTimeStamp;
	}

	public void setModificationTimeStamp(String modificationTimeStamp) {
		this.modificationTimeStamp = modificationTimeStamp;
	}

	public int getIsBudgeted() {
		return isBudgeted;
	}

	public void setIsBudgeted(int isBudgeted) {
		this.isBudgeted = isBudgeted;
	}

	public int getIsBillable() {
		return isBillable;
	}

	public void setIsBillable(int isBillable) {
		this.isBillable = isBillable;
	}

	public int getIsApproved() {
		return isApproved;
	}

	public void setIsApproved(int isApproved) {
		this.isApproved = isApproved;
	}

	public int getIsRejected() {
		return isRejected;
	}

	public void setIsRejected(int isRejected) {
		this.isRejected = isRejected;
	}

	public int getIsCostApproved() {
		return isCostApproved;
	}

	public void setIsCostApproved(int isCostApproved) {
		this.isCostApproved = isCostApproved;
	}

	public int getIsCostRejected() {
		return isCostRejected;
	}

	public void setIsCostRejected(int isCostRejected) {
		this.isCostRejected = isCostRejected;
	}

	public int getIsActive() {
		return isActive;
	}

	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	
	

}
