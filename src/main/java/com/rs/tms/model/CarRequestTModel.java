package com.rs.tms.model;

public class CarRequestTModel {
	
	private int carRequestId,countryId,cityId,projectId,processId,cost;
	private String rrqNo,pickUpLocation,pickUpTime,dropLoaction,needPickUp,carType,prefCarCompany,mebershipNo,sharedCar,clientName,travellingLocationAddr,travelPurpose,projectName,prospectName,tripMode,notes,currency;
	private int isBudgeted,isBillable,isApproved,isRejected,isCostApproved,isCostRejected,isActive;
	private String createdBy,modifiedBy,creationTimeStamp,modificationTimeStamp;
	public int getCarRequestId() {
		return carRequestId;
	}
	public void setCarRequestId(int carRequestId) {
		this.carRequestId = carRequestId;
	}
	public int getCountryId() {
		return countryId;
	}
	public void setCountryId(int countryId) {
		this.countryId = countryId;
	}
	public int getCityId() {
		return cityId;
	}
	public void setCityId(int cityId) {
		this.cityId = cityId;
	}
	public int getProjectId() {
		return projectId;
	}
	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}
	
	public int getProcessId() {
		return processId;
	}
	public void setProcessId(int processId) {
		this.processId = processId;
	}
	public int getCost() {
		return cost;
	}
	public void setCost(int cost) {
		this.cost = cost;
	}
	public String getRrqNo() {
		return rrqNo;
	}
	public void setRrqNo(String rrqNo) {
		this.rrqNo = rrqNo;
	}
	public String getPickUpLocation() {
		return pickUpLocation;
	}
	public void setPickUpLocation(String pickUpLocation) {
		this.pickUpLocation = pickUpLocation;
	}
	public String getPickUpTime() {
		return pickUpTime;
	}
	public void setPickUpTime(String pickUpTime) {
		this.pickUpTime = pickUpTime;
	}
	public String getDropLoaction() {
		return dropLoaction;
	}
	public void setDropLoaction(String dropLoaction) {
		this.dropLoaction = dropLoaction;
	}
	public String getNeedPickUp() {
		return needPickUp;
	}
	public void setNeedPickUp(String needPickUp) {
		this.needPickUp = needPickUp;
	}
	public String getCarType() {
		return carType;
	}
	public void setCarType(String carType) {
		this.carType = carType;
	}
	public String getPrefCarCompany() {
		return prefCarCompany;
	}
	public void setPrefCarCompany(String prefCarCompany) {
		this.prefCarCompany = prefCarCompany;
	}
	public String getMebershipNo() {
		return mebershipNo;
	}
	public void setMebershipNo(String mebershipNo) {
		this.mebershipNo = mebershipNo;
	}
	public String getSharedCar() {
		return sharedCar;
	}
	public void setSharedCar(String sharedCar) {
		this.sharedCar = sharedCar;
	}
	public String getClientName() {
		return clientName;
	}
	public void setClientName(String clientName) {
		this.clientName = clientName;
	}
	public String getTravellingLocationAddr() {
		return travellingLocationAddr;
	}
	public void setTravellingLocationAddr(String travellingLocationAddr) {
		this.travellingLocationAddr = travellingLocationAddr;
	}
	public String getTravelPurpose() {
		return travelPurpose;
	}
	public void setTravelPurpose(String travelPurpose) {
		this.travelPurpose = travelPurpose;
	}
	public String getProjectName() {
		return projectName;
	}
	public void setProjectName(String projectName) {
		this.projectName = projectName;
	}
	public String getProspectName() {
		return prospectName;
	}
	public void setProspectName(String prospectName) {
		this.prospectName = prospectName;
	}
	public String getTripMode() {
		return tripMode;
	}
	public void setTripMode(String tripMode) {
		this.tripMode = tripMode;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public String getCurrency() {
		return currency;
	}
	public void setCurrency(String currency) {
		this.currency = currency;
	}
	public int getIsBudgeted() {
		return isBudgeted;
	}
	public void setIsBudgeted(int isBudgeted) {
		this.isBudgeted = isBudgeted;
	}
	public int getIsBillable() {
		return isBillable;
	}
	public void setIsBillable(int isBillable) {
		this.isBillable = isBillable;
	}
	public int getIsApproved() {
		return isApproved;
	}
	public void setIsApproved(int isApproved) {
		this.isApproved = isApproved;
	}
	public int getIsRejected() {
		return isRejected;
	}
	public void setIsRejected(int isRejected) {
		this.isRejected = isRejected;
	}
	public int getIsCostApproved() {
		return isCostApproved;
	}
	public void setIsCostApproved(int isCostApproved) {
		this.isCostApproved = isCostApproved;
	}
	public int getIsCostRejected() {
		return isCostRejected;
	}
	public void setIsCostRejected(int isCostRejected) {
		this.isCostRejected = isCostRejected;
	}
	public int getIsActive() {
		return isActive;
	}
	public void setIsActive(int isActive) {
		this.isActive = isActive;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public String getCreationTimeStamp() {
		return creationTimeStamp;
	}
	public void setCreationTimeStamp(String creationTimeStamp) {
		this.creationTimeStamp = creationTimeStamp;
	}
	public String getModificationTimeStamp() {
		return modificationTimeStamp;
	}
	public void setModificationTimeStamp(String modificationTimeStamp) {
		this.modificationTimeStamp = modificationTimeStamp;
	}
	
	
	
}
