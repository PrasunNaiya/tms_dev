package com.rs.tms.model;

import java.util.Date;

import org.springframework.stereotype.Component;

@Component
public class EmployeeDetailsModel extends TMSModel {
	
	private String empId;
	private String fullName;
	private String dateOfBirth;
	private String gender;
	private String designation;
	
	public String getEmpId() {
		return empId;
	}
	public void setEmpId(String empId) {
		this.empId = empId;
	}
	public String getFullName() {
		return fullName;
	}
	public void setFullName(String fullName) {
		this.fullName = fullName;
	}
	public String getDateOfBirth() {
		return dateOfBirth;
	}
	public void setDateOfBirth(String dateOfBirth) {
		this.dateOfBirth = dateOfBirth;
	}
	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	
	@Override
	public String toString() {
		return "EmployeeDetailsModel [empId=" + empId + ", fullName=" + fullName + ", dateOfBirth=" + dateOfBirth
				+ ", gender=" + gender + ", designation=" + designation + "]";
	}
	
	
	
	
	
}
