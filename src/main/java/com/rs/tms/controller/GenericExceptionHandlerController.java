package com.rs.tms.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class GenericExceptionHandlerController {
	
	@ExceptionHandler(Throwable.class)
	public ModelAndView handleException(Throwable t) {
		System.out.println("in handleException..");
		t.printStackTrace(System.err);
		ModelMap modelMap = new ModelMap();
		modelMap.put("errorMsg", t.getMessage());
		//System.out.println(t.getMessage());
		return new ModelAndView("error", modelMap);		
	}
}
