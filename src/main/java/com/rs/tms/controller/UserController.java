package com.rs.tms.controller;

import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class UserController extends GenericExceptionHandlerController {

	@RequestMapping(value = "/{userId}", method = RequestMethod.GET)
	public ModelAndView loginUser(@PathVariable("userId") String userId, 
			HttpSession session, Model model) throws Exception {
		System.out.println("Inside loginUser.. " + userId);
		model.addAttribute("userId", userId);
		model.addAttribute("firstName", "A");
		model.addAttribute("middleName", "R");
		model.addAttribute("lastName", "C");
		session.setAttribute("userId", userId);
		session.setAttribute("firstName", "A");
		session.setAttribute("middleName", "R");
		session.setAttribute("lastName", "C");
		//int i = 0/0;
		return new ModelAndView("requesterHome", "user", model);
		//return new ModelAndView("approverHome", "user", model);
	}
	
	@RequestMapping(value = "/redirect", method = RequestMethod.POST)
	public ModelAndView redirectUser(@RequestParam("targetPage") String targetPage,
			@RequestParam(value = "errorMsg", required = false) String errorMsg) throws Exception {
		System.out.println("in redirectUser");
		ModelMap modelMap = new ModelMap();
		modelMap.put("errorMsg", errorMsg);
		return new ModelAndView(targetPage, modelMap);
	}
}
