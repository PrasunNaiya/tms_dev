package com.rs.tms.controller;

import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.rs.tms.domain.Airport;
import com.rs.tms.domain.City;
import com.rs.tms.domain.Country;
import com.rs.tms.domain.Customer;
import com.rs.tms.domain.Project;
import com.rs.tms.model.ApprovalHomeModel;
import com.rs.tms.model.ApproveCarRequestModel;
import com.rs.tms.model.ApproveFlightRequestModel;
import com.rs.tms.model.ApproveHotelRequestModel;
import com.rs.tms.model.CarRequestTModel;
import com.rs.tms.model.ComapnionDetails;
import com.rs.tms.model.EmployeeDetails;
import com.rs.tms.model.FlightRequestTModel;
import com.rs.tms.model.HotelRequestTModel;
import com.rs.tms.model.L0Model;
import com.rs.tms.model.RequestInformationModel;
import com.rs.tms.model.TMSModel;
import com.rs.tms.model.TravelRequestTModel;
import com.rs.tms.service.ApprovalHomeService;
import com.rs.tms.service.TMSService;
import com.rs.tms.service.TravelRequestSubmitService;

@RestController
public class TMSController extends GenericExceptionHandlerController {
	
	@Autowired
	TMSService tmsService;
	@Autowired
	TravelRequestSubmitService travelRequestSubmitService;
	@Autowired
	ApprovalHomeService approvalHomeService;
	
	String message = null;
	String messageForHotel = null;
	String messageForCar = null;
	String messageForFlight = null;
	String messageForComapnion = null;
	List<RequestInformationModel> fetchRequestInformationList;
	List<EmployeeDetails> employeeRequestList;
	List<FlightRequestTModel> flightRequestDetails;
	List<HotelRequestTModel> hotelRequestDetails;
	List<CarRequestTModel> carRequestDetails;
	List<ComapnionDetails> comapnionDetails;
	List<ApprovalHomeModel> fetchApprovalPendingRequestList;
	
	//Travel Request Form Related
	@RequestMapping(value = "/fetchEmployeeDetails", method = RequestMethod.POST)
	public TMSModel fetchEmployeeDetails(@RequestParam("empId") String empId, HttpSession session) throws Exception {
		System.out.println("inside fetchEmployeeDetails controller..");		
		return tmsService.fetchEmployeeDetails(empId);
	}
	
	@RequestMapping(value = "/fetchCountries", method = RequestMethod.POST)
	public List<Country> fetchCountries(HttpSession session) throws Exception {
		System.out.println("inside fetchCountries controller..");		
		return tmsService.fetchCountries();
	}
	
	@RequestMapping(value = "/fetchCities", method = RequestMethod.POST)
	public List<City> fetchCities(@RequestParam("countryId") int countryId, HttpSession session) throws Exception {
		System.out.println("inside fetchCities controller..");		
		return tmsService.fetchCities(countryId);
	}
	
	@RequestMapping(value = "/fetchAirports", method = RequestMethod.POST)
	public List<Airport> fetchAirports(@RequestParam("cityId") int cityId, HttpSession session) throws Exception {
		System.out.println("inside fetchAirports controller..");		
		return tmsService.fetchAirports(cityId);
	}
	
	@RequestMapping(value = "/fetchProjects", method = RequestMethod.POST)
	public List<Project> fetchProjects(HttpSession session) throws Exception {
		System.out.println("inside fetchProjects controller..");		
		return tmsService.fetchProjects();
	}
	
	@RequestMapping(value = "/fetchProcesses", method = RequestMethod.POST)
	public List<Project> fetchProcesses(HttpSession session) throws Exception {
		System.out.println("inside fetchProcesses controller..");		
		return tmsService.fetchProcesses();
	}
	
	@RequestMapping(value = "/fetchClient", method = RequestMethod.POST)
	public Customer fetchClient(@RequestParam("projectId") int projectId, HttpSession session) throws Exception {
		System.out.println("inside fetchClient controller..");		
		return tmsService.fetchCustomer(projectId);
	}

	@RequestMapping(value = "/travelRequestSubmitController", method = RequestMethod.POST)
	public 	TravelRequestTModel travelRequestSubmitController(@RequestParam("travelRequestDetails") String travelRequestDetails) throws Exception {
		System.out.println("inside test controller");
		TravelRequestTModel travelRequestTModelObj = new ObjectMapper().readValue(travelRequestDetails, TravelRequestTModel.class);
		System.out.println(travelRequestTModelObj.getEmpDtls().getEmpName());
		System.out.println(travelRequestTModelObj.getFlightDtlsList());
		System.out.println(travelRequestTModelObj.getHotelDtlsList());
		System.out.println(travelRequestTModelObj.getCarDtlsList());
		
		message = travelRequestSubmitService.employeeDetails(travelRequestTModelObj.getEmpDtls());
		messageForHotel = travelRequestSubmitService.hotelDetails(travelRequestTModelObj.getHotelDtlsList());
		messageForCar = travelRequestSubmitService.carDetails(travelRequestTModelObj.getCarDtlsList());
		messageForFlight = travelRequestSubmitService.flightDetails(travelRequestTModelObj.getFlightDtlsList());
		messageForComapnion = travelRequestSubmitService.companionDetails(travelRequestTModelObj.getCompanionDtlsList());
		//travelRequestTModelObj.getFlightRequestTList().forEach();
		//l0ModelObj.getL3List().forEach(l3Model -> System.out.println(l3Model.getV1() + "\n" + l3Model.getV2()));
		return travelRequestTModelObj;

	}
	
	
	
	// transactinal employee details
	@RequestMapping(value = "/fetchRequestInformation", method = RequestMethod.POST)
	public List<RequestInformationModel> fetchRequestInformation(@RequestParam("empId") String empId, HttpSession session) throws Exception {
		System.out.println("inside fetch Request Information controller..");		
		fetchRequestInformationList = tmsService.fetchRequests(empId);
		for(RequestInformationModel requestInformationModel:fetchRequestInformationList){
			//System.out.println(requestInformationModel.getEmpId()+" "+requestInformationModel.getCreationTimeStamp()+" "+requestInformationModel.getTravelReqId());
		}
		
		return fetchRequestInformationList;
	}
	
	
	@RequestMapping(value = "/fetchEmployeeRequest", method = RequestMethod.POST)
	public EmployeeDetails fetchEmployeeRequest(@RequestParam("travelReqId") int travelReqId, HttpSession session) throws Exception {
		System.out.println("inside fetch employee Request controller..");		
		
		
		return tmsService.fetchEmployeeRequest(travelReqId);
	}
	
	@RequestMapping(value = "/fetchFlightDetails", method = RequestMethod.POST)
	public List<FlightRequestTModel> fetchFlightDetails(@RequestParam("travelReqId") int travelReqId, HttpSession session) throws Exception {
		System.out.println("inside fetch flight Request controller..");		
		flightRequestDetails = tmsService.fetchFlightDetails(travelReqId);
		for(FlightRequestTModel flightRequestTModel : flightRequestDetails){
			System.out.println(flightRequestTModel.getFlightRequestId()+" "+flightRequestTModel.getProjectName());
		}
		return flightRequestDetails;
	}
	
	
	@RequestMapping(value = "/fetchHotelDetails", method = RequestMethod.POST)
	public List<HotelRequestTModel> fetchHotelDetails(@RequestParam("travelReqId") int travelReqId, HttpSession session) throws Exception {
		System.out.println("inside fetch hotel Request controller..");		
		hotelRequestDetails = tmsService.fetchHotelDetails(travelReqId);
		for(HotelRequestTModel hotelRequestTModel : hotelRequestDetails){
			System.out.println(hotelRequestTModel.getCheckInDate()+" "+hotelRequestTModel.getCountryId());
		}
		return hotelRequestDetails;
	}
	
	@RequestMapping(value = "/fetchCarDetails", method = RequestMethod.POST)
	public List<CarRequestTModel> fetchCarDetails(@RequestParam("travelReqId") int travelReqId, HttpSession session) throws Exception {
		System.out.println("inside fetch car Request controller..");		
		carRequestDetails = tmsService.fetchCarDetails(travelReqId);
		return carRequestDetails;
	}
	
	@RequestMapping(value = "/fetchCompanionDetails", method = RequestMethod.POST)
	public List<ComapnionDetails> fetchCompanionDetails(@RequestParam("travelReqId") int travelReqId, HttpSession session) throws Exception {
		System.out.println("inside fetch comapnion Request controller..");		
		comapnionDetails = tmsService.fetchComapnionDetails(travelReqId);
		return comapnionDetails;
	}
	
	//approver
	
	@RequestMapping(value = "/fetchPendingRequestForApprover", method = RequestMethod.POST)
	public List<ApprovalHomeModel> fetchPendingRequestForApprover(@RequestParam("empId") String empId, HttpSession session) throws Exception {
		System.out.println("inside approver home controller..");		
		fetchApprovalPendingRequestList = approvalHomeService.getPendingRequests(empId);
		return fetchApprovalPendingRequestList;
	}
	
	@RequestMapping(value = "/approvePendingHotelRequest", method = RequestMethod.POST)
	public List<ApproveHotelRequestModel> approvePendingHotelRequest(@RequestParam("empId") String empId, HttpSession session) throws Exception {
		System.out.println("inside approvePendingHotelRequest controller..");		
		
		return approvalHomeService.approveHotelReq(empId);
	}
	
	@RequestMapping(value = "/approvePendingFlightRequest", method = RequestMethod.POST)
	public List<ApproveFlightRequestModel> approvePendingFlightRequest(@RequestParam("empId") String empId, HttpSession session) throws Exception {
		System.out.println("inside approvePendingHotelRequest controller..");		
		
		return approvalHomeService.approveFlightReq(empId);
	}
	
	@RequestMapping(value = "/approvePendingCarRequest", method = RequestMethod.POST)
	public List<ApproveCarRequestModel> approvePendingCarRequest(@RequestParam("empId") String empId, HttpSession session) throws Exception {
		System.out.println("inside approvePendingCarRequest controller..");		
		
		return approvalHomeService.approveCarReq(empId);
	}
	
	
	
	
	
	@RequestMapping(value = "/testController", method = RequestMethod.POST)
	public L0Model testController(@RequestParam("l0Model") String l0Model) throws Exception {
		System.out.println("inside test controller");
		L0Model l0ModelObj = new ObjectMapper().readValue(l0Model, L0Model.class);
		System.out.println(l0ModelObj.getS1());
		System.out.println(l0ModelObj.getS2());
		System.out.println(l0ModelObj.getL1().getT1());
		System.out.println(l0ModelObj.getL1().getT2());
		System.out.println(l0ModelObj.getL2().getU1());
		System.out.println(l0ModelObj.getL2().getU2());
		l0ModelObj.getL3List().forEach(l3Model -> System.out.println(l3Model.getV1() + "\n" + l3Model.getV2()));
		return l0ModelObj;

	}
}
