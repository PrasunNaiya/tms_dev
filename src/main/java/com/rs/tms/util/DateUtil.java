package com.rs.tms.util;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class DateUtil {
	
	public Date getCurrentDate()
	{
		Calendar calendar = new GregorianCalendar();
		return calendar.getTime();
	}
	
	public static void main(String[] args)
	{
		DateUtil obj = new DateUtil();
		System.out.println("date");
		System.out.println(obj.getCurrentDate());
	}
}
