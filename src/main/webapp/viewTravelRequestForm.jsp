<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head lang="en">

<meta charset="UTF-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, user-scalable=no">
<meta http-equiv="x-ua-compatible" content="ie=edge">

<title>TMS: Travel Request Form</title>

<link href="resources/assets/common/img/favicon.png" rel="icon"
	type="image/png">
<link href="resources/icon/travel_logo_2.ico" rel="shortcut icon">

<!-- Vendors Styles -->
<!-- v1.0.0 -->
<link rel="stylesheet" type="text/css"
	href="resources/assets/vendors/bootstrap/dist/css/bootstrap.min.css">
<link rel="stylesheet" type="text/css"
	href="resources/assets/vendors/jscrollpane/style/jquery.jscrollpane.css">
<link rel="stylesheet" type="text/css"
	href="resources/assets/vendors/ladda/dist/ladda-themeless.min.css">
<link rel="stylesheet" type="text/css"
	href="resources/assets/vendors/select2/dist/css/select2.min.css">
<link rel="stylesheet" type="text/css"
	href="resources/assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
<link rel="stylesheet" type="text/css"
	href="resources/assets/vendors/fullcalendar/dist/fullcalendar.min.css">

<!-- Clean UI Styles -->
<link rel="stylesheet" type="text/css"
	href="resources/assets/common/css/source/main.css">

<!-- Vendors Scripts -->
<!-- v1.0.0 -->
<script src="resources/assets/vendors/jquery/jquery.min.js"></script>
<script src="resources/assets/vendors/tether/dist/js/tether.min.js"></script>
<script
	src="resources/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
<script
	src="resources/assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
<script
	src="resources/assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
<script src="resources/assets/vendors/spin.js/spin.js"></script>
<script src="resources/assets/vendors/ladda/dist/ladda.min.js"></script>
<script
	src="resources/assets/vendors/select2/dist/js/select2.full.min.js"></script>
<script
	src="resources/assets/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
<script
	src="resources/assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
<script
	src="resources/assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
<script src="resources/assets/vendors/autosize/dist/autosize.min.js"></script>
<script
	src="resources/assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
<script src="resources/assets/vendors/moment/min/moment.min.js"></script>
<script
	src="resources/assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
<script
	src="resources/assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>

<script src="resources/assets/vendors/nestable/jquery.nestable.js"></script>
<script
	src="resources/assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
<script
	src="resources/assets/vendors/datatables/media/js/dataTables.bootstrap4.min.js"></script>
<script
	src="resources/assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
<script
	src="resources/assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
<script
	src="resources/assets/vendors/editable-table/mindmup-editabletable.js"></script>
<script src="resources/assets/vendors/d3/d3.min.js"></script>
<script src="resources/assets/vendors/c3/c3.min.js"></script>

<!-- v1.0.1 -->

<!-- Clean UI Scripts -->
<script src="resources/assets/common/js/common.js"></script>
<script src="resources/assets/common/js/demo.temp.js"></script>

<!-- Custom JS -->
<script src="resources/js/viewTravelRequestForm.js"></script>
<script>
	$(document).ready(function() {
		$('button').on('click', function() {
			$('.Cm').hide();
			var getid = $(this).attr('data-id');
			$('.' + getid).show();

		});
	});
</script>
<style>
.Cm {
	display: none
}
</style>
</head>
<body>
	<nav class="left-menu" left-menu>
	<div class="logo-container">
		<a href="http://www.rssoftware.com" class="logo"> <img
			src="resources/assets/common/img/logo.png"
			alt="Clean UI Admin Template" />

		</a>
	</div>
	</nav>

	<nav class="top-menu">
	<div class="menu-icon-container hidden-md-up">
		<div class="animate-menu-button left-menu-toggle">
			<div>
				<!-- -->
			</div>
		</div>
	</div>
	<div class="menu">

		<a href="index.html" class="logo hidden-md-up"> <img
			src="resources/assets/common/img/logo.png" alt="" style="width: 45%" />

		</a>
		<h3 style="color: #1fb2fe; margin-top: 6px;">Travel Management
			System</h3>
		<div class="menu-info-block"></div>
	</div>
	</nav>


	<section class="page-content">
	<div class="page-content-inner">

		<!-- Basic Form Elements -->
		<section class="panel">
		<div class="panel-heading">
			<hr>
			<h3>View Travel Request</h3>
			<hr>
		</div>
		<div class="panel-body">
			<div class="row" id="employeeDetailsDiv">
				<div class="col-lg-12">
					<div class="margin-bottom-50">
						<h4>Employee Details</h4>
						<br />
						<div class="form-group row">
							<div class="col-md-2">
								<label class="form-control-label" for="employeeId">Employee
									ID</label>
							</div>
							<div class="col-md-4" id="employeeId" name="employeeId"></div>
							<div class="col-md-2">
								<label class="form-control-label" for="employeeName">Name</label>
							</div>
							<div class="col-md-4" id="employeeName"></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2">
								<label class="form-control-label" for="dateOfBirth">Date
									of Birth</label>
							</div>
							<div class="col-md-4" id="dateOfBirth"></div>
							<div class="col-md-2">
								<label class="form-control-label" for="gender">Gender</label>
							</div>
							<div class="col-md-4" id="gender"></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2">
								<label class="form-control-label" for="designation">Designation</label>
							</div>
							<div class="col-md-4" id="designation"></div>
							<div class="col-md-2">
								<label class="form-control-label" for="contactNo">Contact
									Number</label>
							</div>
							<div class="col-md-4" id="contactNo"></div>
						</div>
						<div class="form-group row">
							<div class="col-md-2">
								<label for="travelType">Travel Type</label>
							</div>
							<div class="col-md-4" id="travelType"></div>
							<!-- <div class="col-md-2">
									<label for="mealPreference">Meal Preference</label>
								</div>
								<div class="col-md-4">
									
								</div> -->
						</div>
						<div class="form-group row internationalField">
							<div class="col-md-2">
								<label class="form-control-label" for="passportNo">Passport
									Number</label>
							</div>
							<div class="col-md-4" id="passportNo"></div>
							<div class="col-md-2">
								<label class="form-control-label" for="nameOnPassport">Name
									on Passport</label>
							</div>
							<div class="col-md-4" id="nameOnPassport"></div>
						</div>
						<div class="form-group row internationalField">
							<div class="col-md-2">
								<label class="form-control-label" for="addressOnPassport">Address
									on Passport</label>
							</div>
							<div class="col-md-4" id="addressOnPassport"></div>
							<div class="col-md-2">
								<label class="form-control-label" for="passportIssuePlace">Passport
									Issue Place</label>
							</div>
							<div class="col-md-4" id="passportIssuePlace"></div>
						</div>
						<div class="form-group row internationalField">
							<div class="col-md-2">
								<label class="form-control-label" for="passportIssueDate">Passport
									Issue Date</label>
							</div>
							<div class="col-md-4" id="passportIssueDate"></div>
							<div class="col-md-2">
								<label class="form-control-label" for="passportExpiryDate">Passport
									Expiry Date</label>
							</div>
							<div class="col-md-4" id="passportExpiryDate"></div>
						</div>
						<div class="form-group row internationalField">
							<div class="col-md-2">
								<label class="form-control-label" for="ECNRCheck">ECNR
									Check</label>
							</div>
							<div class="col-md-4" id="ECNRCheck"></div>
						</div>
						<div class="form-group row internationalField">
							<div class="col-md-2">
								<label class="form-control-label" for="visaType">Visa
									Type</label>
							</div>
							<div class="col-md-4" id="visaType"></div>
							<div class="col-md-2">
								<label class="form-control-label" for="visaNo">Visa
									Number</label>
							</div>
							<div class="col-md-4" id="visaNo"></div>
						</div>
						<div class="form-group row internationalField">
							<div class="col-md-2">
								<label class="form-control-label" for="visaIssueDate">Visa
									Issue Date</label>
							</div>
							<div class="col-md-4" id="visaIssueDate"></div>
							<div class="col-md-2">
								<label class="form-control-label" for="visaExpiryDate">Visa
									Expiry Date</label>
							</div>
							<div class="col-md-4" id="visaExpiryDate"></div>
						</div>
						<div class="form-group row internationalField">
							<div class="col-md-2">
								<label class="form-control-label" for="pitition">Petition
									#(if H1)</label>
							</div>
							<div class="col-md-4" id="pitition"></div>
							<div class="col-md-2">
								<label class="form-control-label" for="validity">Validity</label>
							</div>
							<div class="col-md-4" id="validity"></div>
						</div>
						<div class="form-group row internationalField">
							<div class="col-md-2">
								<label class="form-control-label" for="nomineeName">Nominee
									Name</label>
							</div>
							<div class="col-md-4" id="nomineeName"></div>
							<div class="col-md-2">
								<label class="form-control-label"
									for="emergencyContactPersonName">Emergency Contact
									Person Name</label>
							</div>
							<div class="col-md-4" id="emergencyContactPersonName"></div>
						</div>
						<div class="form-group row internationalField">
							<div class="col-md-2">
								<label class="form-control-label"
									for="emergencyContactPersonAddress">Emergency Contact
									Person Address</label>
							</div>
							<div class="col-md-4" id="emergencyContactPersonAddress"></div>
							<div class="col-md-2">
								<label class="form-control-label"
									for="emergencyContactPersonNumber">Emergency Contact
									Person Contact Number</label>
							</div>
							<div class="col-md-4"></div>
						</div>

					</div>
				</div>
			</div>
			<button data-id="bookedFlight">View Flight Details</button>
			<button data-id="bookedHotel">View Hotel Details</button>
			<button data-id="bookedCar">View Car Details</button>
			<button data-id="bookedCompanion">View Companion Details</button>
			<div class="Cm bookedFlight">
				<div class="row" id="flightDetailsDiv">
					<div class="col-lg-12">
						<div class="margin-bottom-50">
							<hr>
							<h4>Flight Details</h4>
							<!--  <span class="pull-right">
	                        	<i class="fa fa-plus" id="flightAddIcon" aria-hidden="false" onclick="addRow('flight')" style="cursor: pointer;"></i>&nbsp;&nbsp;&nbsp;
	                        	<i class="fa fa-minus" id="flightRemoveIcon" aria-hidden="false" onclick="removeRow('flight')" style="cursor: pointer; display: none"></i>
                        	</span> -->
							<hr>
							<br />
							<div id="flightParentDiv">
								<div class="row flightChildDiv">
									<div id="flight1">
										<h5>Flight 1</h5>
										<br />
										<div id="flightStaticHtml">
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1sourceCountry">Source Country*</label>
												</div>
												<div class="col-md-2" id="1sourceCountry"></div>
												<div class="col-md-2">
													<label for="1sourceCity">Source City*</label>
												</div>
												<div class="col-md-2" id="1sourceCity"></div>
												<div class="col-md-2">
													<label for="1sourceAirport">Source Airport</label>
												</div>
												<div class="col-md-2" id="1sourceAirport"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1destinationCountry">Destination
														Country*</label>
												</div>
												<div class="col-md-2" id="1destinationCountry"></div>
												<div class="col-md-2">
													<label for="1destinationCity">Destination City*</label>
												</div>
												<div class="col-md-2" id="1destinationCity"></div>
												<div class="col-md-2">
													<label for="1destinationAirport">Destination
														Airport</label>
												</div>
												<div class="col-md-2" id="1destinationAirport"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1departureDate">Departure
														Date*</label>
												</div>
												<div class="col-md-2" id="1departureDate"></div>
												<div class="col-md-2">
													<label class="form-control-label"
														for="1departureTimePreference">Departure Time
														Preference*</label>
												</div>
												<div class="col-md-2" id="1departureTimePreference"></div>
												<div class="col-md-2">
													<label class="form-control-label"
														for="1requiredArrivalTime">Required Arrival Time*</label>
												</div>
												<div class="col-md-2" id="1requiredArrivalTime"></div>
											</div>
											<div class="form-group row">


												<div class="col-md-1">
													<label class="form-control-label" for="1portOfEntry">Port
														of Entry*</label>
												</div>
												<div class="col-md-1" id="1portOfEntry"></div>
												<div class="col-md-2">
													<label class="form-control-label"
														for="1frequentFlierAirline">Frequent Flier
														Airlines</label>
												</div>
												<div class="col-md-2" id="1frequentFlierAirline"></div>



												<div class="col-md-2">
													<label class="form-control-label" for="1frequentFlierNo">Frequent
														Flier Number</label>
												</div>
												<div class="col-md-2" id="1frequentFlierNo"></div>


												<div class="col-md-1">
													<label class="form-control-label"
														for="1frequentFlierFlightNo">Frequent Flier Flight
														Number</label>
												</div>
												<div class="col-md-1" id="1frequentFlierFlightNo"></div>


											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1pppSelectionF">Project/Process/Prospect*</label>
												</div>
												<div class="col-md-2"></div>
												<div id="1projectDivF">
													<div class="col-md-2">
														<label for="1projectIdF">Project</label>
													</div>
													<div class="col-md-2" id="1projectIdF"></div>
												</div>
												<div id="1processDivF">
													<div class="col-md-2">
														<label for="1processIdF">Process</label>
													</div>
													<div class="col-md-2" id="1processIdF"></div>
												</div>
												<div id="1prospectDivF">
													<div class="col-md-2">
														<label for="1prospectNameF">Prospect</label>
													</div>
													<div class="col-md-2" id="1prospectNameF"></div>
												</div>
												<div id="1clientNameDivF">
													<div class="col-md-2">
														<label class="form-control-label" for="1clientNameF">Client
															Name</label>
													</div>
													<div class="col-md-2" id="1clientNameF"></div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1travelPurposeF">Purpose of Travel</label>
												</div>
												<div class="col-md-2" id="1travelPurposeF"></div>
												<div class="col-md-2">
													<label class="form-control-label">Mode of Trip</label>
												</div>
												<div class="col-md-2"></div>
												<div class="col-md-2"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1notesF">Notes</label>
												</div>
												<div class="col-md-10" id="1notesF"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
			<div class="Cm bookedHotel">
				<div class="row" id="hotelDetailsDiv">
					<div class="col-lg-12">
						<div class="margin-bottom-50">
							<hr>
							<h4>Hotel Details</h4>
							 <span class="pull-right">
	                        	<i class="fa fa-plus" id="hotelAddIcon" aria-hidden="false" onclick="addRow('hotel')" style="cursor: pointer;"></i>&nbsp;&nbsp;&nbsp;
	                        	<i class="fa fa-minus" id="hotelRemoveIcon" aria-hidden="false" onclick="removeRow('hotel')" style="cursor: pointer; display: none"></i>
                        	</span>
							<hr>
							<br />
							<div id="hotelParentDiv">
								<div class="row hotelChildDiv">
									<div id="hotel1">
										<h5>Hotel 1</h5>
										<br />
										<div id="hotelStaticHtml">
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1hotelCountry">Country*</label>
												</div>
												<div class="col-md-2" id="1hotelCountry"></div>
												<div class="col-md-2">
													<label for="1hotelCity">City*</label>
												</div>
												<div class="col-md-2" id="1hotelCity"></div>
												<div class="col-md-2">
													<label class="form-control-label notImp"
														for="1preferredHotel">Preferred Hotel</label>
												</div>
												<div class="col-md-2" id="1preferredHotel"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1checkInDate">Check-in
														Date*</label>
												</div>
												<div class="col-md-2" id="1checkInDate"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1checkOutDate">Check-out
														Date*</label>
												</div>
												<div class="col-md-2" id="1checkOutDate"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1bookNearLocation">Book
														Near Location</label>
												</div>
												<div class="col-md-2" id="1bookNearLocation"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1rewardMemberNo">Reward
														Member Number</label>
												</div>
												<div class="col-md-2" id="1rewardMemberNo"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1rewardHotelName">Reward
														Hotel Name</label>
												</div>
												<div class="col-md-2" id="1rewardHotelName"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1pppSelectionH">Project/Process/Prospect*</label>
												</div>
												<div class="col-md-2"></div>
												<div id="1projectDivH">
													<div class="col-md-2">
														<label for="1projectIdH">Project*</label>
													</div>
													<div class="col-md-2" id="1projectIdH"></div>
												</div>
												<div id="1processDivH">
													<div class="col-md-2">
														<label for="1processIdH">Process*</label>
													</div>
													<div class="col-md-2" id="1processIdH"></div>
												</div>
												<div id="1prospectDivH">
													<div class="col-md-2">
														<label for="1prospectNameH">Prospect</label>
													</div>
													<div class="col-md-2" id="1prospectNameH"></div>
												</div>
												<div id="1clientNameDivH">
													<div class="col-md-2">
														<label class="form-control-label" for="1clientNameH">Client
															Name</label>
													</div>
													<div class="col-md-2 notImp" id="1clientNameH"></div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1clientAddressH">Client Address*</label>
												</div>
														<div class="col-md-2" id="1clientAddressH"></div>
														<div class="col-md-2">
													<label class="form-control-label" for="1clientContactNoH">Client
														Contact No</label>
												</div>
												<div class="col-md-2" id="1clientContactNoH"></div>
												<div class="col-md-2">
													<label for="1travelPurposeH">Purpose of Travel</label>
												</div>
												<div class="col-md-2" id="1travelPurposeH"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1notesH">Notes</label>
												</div>
												<div class="col-md-10" id="1notesH"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Cm bookedCar">
				<div class="row" id="carDetailsDiv">
					<div class="col-lg-12">
						<div class="margin-bottom-50">
							<hr>
							<h4>Car Details</h4>
							<hr>
							<br />
							<div id="carParentDiv">
								<div class="row carChildDiv">
									<div id="car1">
										<h5>Car 1</h5>
												<span class="pull-right"> <i class="fa fa-plus"
													id="carAddIcon" aria-hidden="false" onclick="addRow('car')"
													style="cursor: pointer;"></i>&nbsp;&nbsp;&nbsp; <i
													class="fa fa-minus" id="carRemoveIcon" aria-hidden="false"
													onclick="removeRow('car')"
													style="cursor: pointer; display: none"></i>
												</span> <br />
										<div id="carStaticHtml">
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1carCountry">Country*</label>
												</div>
												<div class="col-md-2" id="1carCountry"></div>
												<div class="col-md-2">
													<label for="1carCity">City*</label>
												</div>
												<div class="col-md-2" id="1carCity"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1pickupAddress">Pick-up
														Location Address*</label>
												</div>
												<div class="col-md-2" id="1pickupAddress"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1pickupDateAndTime">Pick-up
														Date and Time*</label>
												</div>
												<div class="col-md-2" id="1pickupDateAndTime"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1dropOffAddress">Drop-off
														Location Address*</label>
												</div>
												<div class="col-md-2" id="1dropOffAddress"></div>
												<div class="col-md-2">
													<label class="form-check-label checkbox-inline">
														Need Return Pick-up </label>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1carType">Car
														Type*</label>
												</div>
												<div class="col-md-2" id="1carType"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1carMembershipNo">Membership
														Number</label>
												</div>
												<div class="col-md-2" id="1carMembershipNo"></div>
												<div class="col-md-2">
													<label class="form-control-label"
														for="1carMembershipCarCompany">Membership Car
														Company</label>
												</div>
												<div class="col-md-2" id="1carMembershipCarCompany"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1pppSelectionC">Project/Process/Prospect*</label>
												</div>
												<div class="col-md-2"></div>
												<div id="1projectDivC">
													<div class="col-md-2">
														<label for="1projectIdC">Project*</label>
													</div>
													<div class="col-md-2" id="1projectIdC"></div>
												</div>
												<div id="1processDivC">
													<div class="col-md-2">
														<label for="1processIdC">Process*</label>
													</div>
													<div class="col-md-2" id="1processIdC"></div>
												</div>
												<div id="1prospectDivC">
													<div class="col-md-2">
														<label for="1prospectNameC">Prospect*</label>
													</div>
													<div class="col-md-2" id="1prospectNameC"></div>
												</div>
												<div id="1clientNameDivC">
													<div class="col-md-2">
														<label class="form-control-label" for="1clientNameC">Client
															Name</label>
													</div>
													<div class="col-md-2" id="1clientNameC"></div>
												</div>
											</div>
											<div class="form-group row">

												<!-- <div class="col-md-2">
													<label for="1clientAddress">Client Address*</label>
												</div>
												<div class="col-md-2"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1clientContactNo">Client
														Contact No</label>
												</div>
												<div class="col-md-2"></div> -->
												
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1travelPurposeC">Purpose of Travel</label>
												</div>
												<div class="col-md-2" id="1travelPurposeC"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1notesC">Notes</label>
												</div>
												<div class="col-md-10" id="1notesC"></div>
											</div>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="Cm bookedCompanion">
				<div class="row" id="companionDetailsDiv">
					<div class="col-lg-12">
						<div class="margin-bottom-50">
							<hr>
							<h4>Companion Details</h4>
							 <span class="pull-right">
	                        	<i class="fa fa-plus" id="companionAddIcon" aria-hidden="false" onclick="addRow('companion')" style="cursor: pointer;"></i>&nbsp;&nbsp;&nbsp;
	                        	<i class="fa fa-minus" id="companionRemoveIcon" aria-hidden="false" onclick="removeRow('companion')" style="cursor: pointer; display: none"></i>
                        	</span>
							<hr>
							<br />
							<div id="companionParentDiv">
								<div class="row companionChildDiv">
									<div id="companion1">
										<h5>Companion 1</h5>
										<br />
										<div id="companionStaticHtml">
											<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1companionName">Companion
														Name*</label>
												</div>
												<div class="col-md-2" id="1companionName"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1relation">Relation*</label>
												</div>
												<div class="col-md-2" id="1relation"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1cdateOfBirth">Date
														of Birth*</label>
												</div>
												<div class="col-md-2" id="1cdateOfBirth"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1cpassportNo">Passport
														No.*</label>
												</div>
												<div class="col-md-2" id="1cpassportNo"></div>
												<div class="col-md-2">
													<label for="1caddressOnPassport">Address on
														Passport*</label>
												</div>
												<div class="col-md-2" id="1caddressOnPassport"></div>
												<div class="col-md-2">
													<label class="form-control-label"
														for="1cpassportIssuePlace">Passport Issue Place*</label>
												</div>
												<div class="col-md-2" id="1cpassportIssuePlace"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1passportIssueDate">Passport
														Issue Date*</label>
												</div>
												<div class="col-md-2" id="1passportIssueDate"></div>
												<div class="col-md-2">
													<label class="form-control-label"
														for="1cpassportExpiryDate">Passport Expiry Date*</label>
												</div>
												<div class="col-md-2" id="1cpassportExpiryDate"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1cECNRCheck">ECNR
														Check*</label>
												</div>
												<div class="col-md-2" id="1cECNRCheck"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1cvisaType">Visa
														Type*</label>
												</div>
												<div class="col-md-2" id="1cvisaType"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1cvisaNo">Visa
														Number*</label>
												</div>
												<div class="col-md-2" id="1cvisaNo"></div>

												<div class="col-md-2">
													<label class="form-control-label" for="1cvisaIssueDate">Visa
														Issue Date*</label>
												</div>
												<div class="col-md-2" id="1cvisaIssueDate"></div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1cvisaExpiryDate">Visa
														Expiry Date*</label>
												</div>
												<div class="col-md-2" id="1cvisaExpiryDate"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1cvalidity">Validity*</label>
												</div>
												<div class="col-md-2" id="1cvalidity"></div>
												<div class="col-md-2">
													<label class="form-control-label" for="1cdateOfTravel">Date
														of Travel*</label>
												</div>
												<div class="col-md-2" id="1cdateOfTravel"></div>
											</div>

										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		</section>
		<form id="redirectForm" action="redirect" method="post">
			<input type="hidden" name="travelReqId" id="travelReqId" value="${travelReqId}" />
			<input type="hidden" name="targetPage" id="targetPage" value="travelRequestForm" />
		</form>
	</div>
	</section>
	<!-- <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#bookedFlight">View Flight Details</button>
 <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#bookedHotel">View Hotel Details</button>
 <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#bookedCar">View Car Details</button>
 <button type="button" class="btn btn-info" data-toggle="collapse" data-target="#bookedCompanion">View Companion Details</button>
  <div id="bookedFlight" class="collapse">
   
	            <div class="row" id="flightDetailsDiv" >
	                <div class="col-lg-12">
	                    <div class="margin-bottom-50">
	                        <h4>Flight Details</h4>
	                        <br />
	                        <div id="flightParentDiv">
	                        	<div class="row flightChildDiv">
	                        		<div id="flight1">
			                        	<h5>Flight 1</h5>
			                        	<br />
			                        	<div id="flightStaticHtml">
				                            <div class="form-group row">
												<div class="col-md-2">
													<label for="1sourceCountry">Source Country*</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div class="col-md-2">
													<label for="1sourceCity">Source City*</label>
												</div>
												<div class="col-md-2">
															
												</div>
												<div class="col-md-2">
													<label for="1sourceAirport">Source Airport</label>
												</div>
												<div class="col-md-2">
														
												</div>	
				                            </div>
				                            <div class="form-group row">
				                                <div class="col-md-2">
													<label for="1destinationCountry">Destination Country*</label>
												</div>
												<div class="col-md-2">
														
												</div>
												<div class="col-md-2">
													<label for="1destinationCity">Destination City*</label>
												</div>
												<div class="col-md-2">
															
												</div>
												<div class="col-md-2">
													<label for="1destinationAirport">Destination Airport</label>
												</div>
												<div class="col-md-2">
															
												</div>
				                            </div>
				                            <div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1departureDate">Departure Date*</label>
												</div>
												<div class="col-md-2">
													 
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1departureTimePreference">Departure Time Preference*</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1requiredArrivalTime">Required Arrival Time*</label>
												</div>
												<div class="col-md-2">
													
												</div>
											</div>											
											<div class="form-group row">
												
												-added start
												<div class="col-md-1">
													<label class="form-control-label" for="1portOfEntry">Port of Entry*</label>
												</div>
												<div class="col-md-1">
													
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1frequentFlierAirline">Frequent Flier Airlines</label>
												</div>
												<div class="col-md-2">
													
												</div>
												
												added end
												
												<div class="col-md-2">
													<label class="form-control-label" for="1preferredAirline">Preferred Airlines</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control notImp" placeholder="Preferred Airlines" name="1preferredAirlines" data-msg="Enter preferred airlines for flight1" id="1preferredAirlines" />
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1frequentFlierNo">Frequent Flier Number</label>
												</div>
												<div class="col-md-2">
													
												</div>
												added start
												
												<div class="col-md-1">
													<label class="form-control-label" for="1frequentFlierFlightNo">Frequent Flier Flight Number</label>
												</div>
												<div class="col-md-1">
											
												</div>
												
												-added end
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1pppSelectionF">Project/Process/Prospect*</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div id="1projectDivF" style="display: none;">
													<div class="col-md-2">
														<label for="1projectIdF">Project</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
												<div id="1processDivF" style="display: none;">
													<div class="col-md-2">
														<label for="1processIdF">Process</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
												<div id="1prospectDivF" style="display: none;">
													<div class="col-md-2">
														<label for="1prospectNameF">Prospect</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
												<div id="1clientNameDivF" style="display: none;">
													<div class="col-md-2">
														<label class="form-control-label" for="1clientNameF">Client Name</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1travelPurposeF">Purpose of Travel</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div class="col-md-2">
													<label class="form-control-label">Mode of Trip</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div class="col-md-2">
													
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1notesF">Notes</label>
												</div>
												<div class="col-md-10">
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
	                    </div>
	                </div>
	            </div>
	           
  </div>
  <div id="bookedHotel" class="collapse">
    <div class="row" id="hotelDetailsDiv">
	                <div class="col-lg-12">
	                    <div class="margin-bottom-50">
	                        <h4>Hotel Details</h4>
	                        <hr>
	                        <br />
	                        <div id="hotelParentDiv">
	                        	<div class="row hotelChildDiv">
	                        		<div id="hotel1">
			                        	<h5>Hotel 1</h5>
			                        	<br />
			                        	<div id="hotelStaticHtml">
			                        		<div class="form-group row">
												<div class="col-md-2">
													<label for="1hotelCountry">Country*</label>
												</div>
												<div class="col-md-2">
															
												</div>
												<div class="col-md-2">
													<label for="1hotelCity">City*</label>
												</div>
												<div class="col-md-2">
															
												</div>
												<div class="col-md-2">
													<label class="form-control-label notImp" for="1preferredHotel">Preferred Hotel</label>
												</div>
												<div class="col-md-2">
													
												</div>	
				                            </div>
			                        		<div class="form-group row">
			                        			<div class="col-md-2">
													<label class="form-control-label" for="1checkInDate">Check-in Date*</label>
												</div>
												<div class="col-md-2">
													
												</div>
			                        			<div class="col-md-2">
													<label class="form-control-label" for="1checkOutDate">Check-out Date*</label>
												</div>
												<div class="col-md-2">

												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="bookNearLocation">Book Near Location</label>
												</div>
												<div class="col-md-2">
													
												</div>												
			                        		</div>			                        		
			                        		<div class="form-group row">
			                        			<div class="col-md-2">
													<label class="form-control-label" for="1rewardMemberNo">Reward Member Number</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1rewardHotelName">Reward Hotel Name</label>
												</div>
												<div class="col-md-2">
													
												</div>
			                        		</div>
			                        		<div class="form-group row">
												<div class="col-md-2">
													<label for="1pppSelectionH">Project/Process/Prospect*</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div id="1projectDivH" >
													<div class="col-md-2">
														<label for="1projectIdH">Project*</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
												<div id="1processDivH">
													<div class="col-md-2">
														<label for="1processIdH">Process*</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
												<div id="1prospectDivH">
													<div class="col-md-2">
														<label for="1prospectNameH">Prospect</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
												<div id="1clientNameDivH" >
													<div class="col-md-2">
														<label class="form-control-label" for="1clientNameH">Client Name</label>
													</div>
													<div class="col-md-2 notImp">
														<input type="text" class="form-control" placeholder="Client Name" name="1clientNameH" data-msg="Enter client name for hotel1" id="1clientNameH" readonly/>
													</div>
												</div>
											</div>
											<div class="form-group row">
											<div class="col-md-2">
													<label for="1clientAddressH">Client Address*</label>
												</div>
												<div class="col-md-2">
													
												</div>
											<div class="col-md-2">
													<label class="form-control-label" for="1clientContactNoH">Client Contact No</label>
												</div>
												<div class="col-md-2">
													 
												</div>
												<div class="col-md-2">
													<label for="1travelPurposeH">Purpose of Travel</label>
												</div>
												<div class="col-md-2">
													
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1notesH">Notes</label>
												</div>
												<div class="col-md-10">
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
	                    </div>
	                </div>
	            </div>
  </div>
  <div id="bookedCar" class="collapse">
   <div class="row" id="carDetailsDiv">
	                <div class="col-lg-12">
	                    <div class="margin-bottom-50">
	                        <h4>Car Details</h4>
	                       <hr>
	                        <br />
	                        <div id="carParentDiv">
	                        	<div class="row carChildDiv">
	                        		<div id="car1">
			                        	<h5>Car 1</h5>
			                        	<br />
			                        	<div id="carStaticHtml">
			                        		<div class="form-group row">
			                        			<div class="col-md-2">
													<label for="1carCountry">Country*</label>
												</div>
												<div class="col-md-2">
															
												</div>
												<div class="col-md-2">
													<label for="1carCity">City*</label>
												</div>
												<div class="col-md-2">
															
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1pickupAddress">Pick-up Location Address*</label>
												</div>
												<div class="col-md-2">
													
												</div>
			                        		</div>
			                        		<div class="form-group row">
			                        			<div class="col-md-2">
													<label class="form-control-label" for="1pickupDateAndTime">Pick-up Date and Time*</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1dropOffAddress">Drop-off Location Address*</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div class="col-md-2">
													<label class="form-check-label checkbox-inline">
														<input type="checkbox" name="1needReturnPickUp" id="1needReturnPickUp" value="1" onclick="toggleReturnPickupFields(this.id,this.value)"/>
														Need Return Pick-up
													</label>
												</div>
			                        		</div>
			                        		<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1carType">Car Type*</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1carMembershipNo">Membership Number</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1carMembershipCarCompany">Membership Car Company</label>
												</div>
												<div class="col-md-2">
													
												</div>
			                        		</div>
			                        		<div class="form-group row">
												<div class="col-md-2">
													<label for="1pppSelectionC">Project/Process/Prospect*</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div id="1projectDivC">
													<div class="col-md-2">
														<label for="1projectIdC">Project*</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
												<div id="1processDivC" >
													<div class="col-md-2">
														<label for="1processIdC">Process*</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
												<div id="1prospectDivC" >
													<div class="col-md-2">
														<label for="1prospectNameC">Prospect*</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
												<div id="1clientNameDivC" >
													<div class="col-md-2">
														<label class="form-control-label" for="1clientNameC">Client Name</label>
													</div>
													<div class="col-md-2">
														
													</div>
												</div>
											</div>
											<div class="form-group row">
											
											<div class="col-md-2">
													<label for="1clientAddress">Client Address*</label>
												</div>
												<div class="col-md-2">
													
												</div>
											<div class="col-md-2">
													<label class="form-control-label" for="1clientContactNo">Client Contact No</label>
												</div>
												<div class="col-md-2">
													
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1sharedCarType">Shared car</label>
												</div>
												<div class="col-md-2">
													
												</div>
												</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1travelPurposeC">Purpose of Travel</label>
												</div>
												<div class="col-md-2">
													
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1notesC">Notes</label>
												</div>
												<div class="col-md-10">
													
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
	                    </div>
	                </div>
	            </div>
  </div>
  <div id="bookedCompanion" class="collapse">
   <div class="row" id="companionDetailsDiv">
	                <div class="col-lg-12">
	                    <div class="margin-bottom-50">
	                         <h4>Companion Details</h4>
	                        <hr>
	                        <br />
	                        <div id="companionParentDiv">
	                        	<div class="row companionChildDiv">
	                        		<div id="companion1">
			                        	<h5>Companion 1</h5>
			                        	<br />
			                        	<div id="companionStaticHtml">
                            <div class="form-group row">
                                <div class="col-md-2">
									<label class="form-control-label" for="1companionName">Companion Name*</label>
								</div>
								<div class="col-md-2">
									
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1relation">Relation*</label>
								</div>
								<div class="col-md-2">
									
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cdateOfBirth">Date of Birth*</label>
								</div>
								<div class="col-md-2">
									</div>
								</div>
								<div class="form-group row">
                                <div class="col-md-2">
									<label class="form-control-label" for="1cpassportNo">Passport No.*</label>
								</div>
								<div class="col-md-2">
									
								</div>
								<div class="col-md-2">
								<label for="1caddressOnPassport">Address on Passport*</label>
								</div>
								<div class="col-md-2">
								
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cpassportIssuePlace">Passport Issue Place*</label>
								</div>
								<div class="col-md-2">										
									
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-2">
									<label class="form-control-label" for="passportIssueDate">Passport Issue Date*</label>
								</div>
								<div class="col-md-2">
									
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cpassportExpiryDate">Passport Expiry Date*</label>
								</div>
								<div class="col-md-2">										
									
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cECNRCheck">ECNR Check*</label>
								</div>
								<div class="col-md-2">
									
								</div>
								</div>
							<div class="form-group row">
								<div class="col-md-2">
									<label class="form-control-label" for="1cvisaType">Visa Type*</label>
								</div>
								<div class="col-md-2">
									
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cvisaNo">Visa Number*</label>
								</div>
								<div class="col-md-2">
									</div>
						
								<div class="col-md-2">
									<label class="form-control-label" for="1cvisaIssueDate">Visa Issue Date*</label>
								</div>
								<div class="col-md-2">
									
								</div>
								</div>
							<div class="form-group row">
								<div class="col-md-2">
									<label class="form-control-label" for="1cvisaExpiryDate">Visa Expiry Date*</label>
								</div>
								<div class="col-md-2">
					
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cvalidity">Validity*</label>
								</div>
								<div class="col-md-2">
									
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cdateOfTravel">Date of Travel*</label>
								</div>
								<div class="col-md-2">
									
								</div>
								</div>
								
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
  </div>      -->
</body>
</html>