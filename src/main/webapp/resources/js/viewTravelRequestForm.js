
var flightStaticHtml;
var hotelStaticHtml;
var carStaticHtml;
var companionStaticHtml;

$(function(){
	$('button').on('click',function(){
		$('.book').hide();
		var getid=$(this).attr('data-id');
		$('.'+getid).show();

	});
	$(".readonly").attr('readonly','readonly');
	
	setComponentStaticHtml();
	fetchTravelRequest();
	fetchFlightDetails();
	fetchHotelDetails();
	fetchCarDetails();
	fetchCompanionDetails();
	fetchEmployeeDetails();
	
	
});
//generate static html start

function setComponentStaticHtml() {
	flightStaticHtml = $('#flightStaticHtml').html();
	hotelStaticHtml = $('#hotelStaticHtml').html();
	carStaticHtml = $('#carStaticHtml').html();
	companionStaticHtml = $('#companionStaticHtml').html();
}



function indexElements(componentStaticHtml,index) {
	componentStaticHtml = componentStaticHtml.replace(/class="form-control"/g, 'class="form-control required "' );
	componentStaticHtml = componentStaticHtml.replace(/class="form-control datepicker-only-init"/g, 'class="form-control datepicker-only-init required "' );
	componentStaticHtml = componentStaticHtml.replace(/class="form-control timepicker-init"/g, 'class="form-control timepicker-init required "' );
	componentStaticHtml = componentStaticHtml.replace(/for="1/g, 'for="' + index);
	componentStaticHtml = componentStaticHtml.replace(/name="1/g, 'name="' + index);
	componentStaticHtml = componentStaticHtml.replace(/id="1/g, 'id="' + index);
	return componentStaticHtml;
}

function getComponentStaticHtml(component) {
	if(component == 'flight') {
		return flightStaticHtml;
	} else if(component == 'hotel') {
		return hotelStaticHtml;
	} else if(component == 'car') {
		return carStaticHtml;
	}else
		return companionStaticHtml;
}

function removeRow(component) {
	
	var componentCount = $('.' + component + 'ChildDiv').length;
	$('#' + component + componentCount).parent().remove();
	$('#' + component + 'AddIcon').show();
	if(--componentCount == 1) {
		subFormValidation(divId,type);
		$('#' + component + 'RemoveIcon').hide();
	}
	
}



//generate static html end

function fetchTravelRequest() {
	//alert(parseInt($('#travelReqId').val()))
	$.ajax({
		async: false,
		cache: false,
		method: "POST",
		url: "fetchEmployeeRequest",
		dataType: "json",
		data: {
			travelReqId: $('#travelReqId').val()
		},
		success: function(travelRequest) {
			//alert("inside success")
			populateTravelRequest(travelRequest);
			
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			alert(errorThrown);
			$('#errorMsg').val(errorThrown);
			$('#redirectForm').submit();
		}
	});
}

function populateTravelRequest(travelRequest) {
	
	if(travelRequest != undefined || travelRequest != null || travelRequest != '') {
		
		
		$('#employeeId').text(travelRequest.empId);
		//$('#employeeName').text(travelRequest.empName);
		//$('#dateOfBirth').text(travelRequest.dob);
		//('#designation').text(travelRequest.designation);
		$('#contactNo').text(travelRequest.contactNo);
		$('#travelType').text(travelRequest.travelType);
		$('#passportNo').text(travelRequest.passportNo);
		$('#nameOnPassport').text(travelRequest.nameOnPassport);
		$('#addressOnPassport').text(travelRequest.addrOnPassport);
		$('#passportIssuePlace').text(travelRequest.passportIssuePlace);
		$('#passportIssueDate').text(travelRequest.passportIssueDate);
		$('#ECNRCheck').text(travelRequest.encrCheck);
		$('#visaType').text(travelRequest.visaType);
		$('#visaNo').text(travelRequest.visaNo);
		$('#visaIssueDate').text(travelRequest.visaIssueDate);
		$('#visaExpiryDate').text(travelRequest.visaExpiryDate);
		$('#pitition').text(travelRequest.pitition);
		$('#validity').text(travelRequest.validity);
		$('#nomineeName').text(travelRequest.nomineeName);
		$('#emergencyContactPersonName').text(travelRequest.emrContactPersonName);
		$('#emergencyContactPersonAddress').text(travelRequest.emrContactPersonAddr);
		$('#emergencyContactPersonNumber').text(travelRequest.emrContactPersonNo);
		
		
	}
	
}	

	function fetchEmployeeDetails() {
		
		
		$.ajax({
			async: false,
			cache: false,
			method: "POST",
			url: "fetchEmployeeDetails",
			dataType: "json",
			data: {
				empId: $('#employeeId').text()
			},
			success: function(data) {
				//alert("inside success")
				populateEmployeeDetails(data);
			},
			error: function(jqXHR, textStatus, errorThrown)	{
				//alert(errorThrown);
				$('#errorMsg').val(errorThrown);
				$('#redirectForm').submit();
			}
		});
	}

	function populateEmployeeDetails(data){
		//alert(JSON.stringify(data));
		
		if(data != undefined || data != null || data != '') {
			
			$('#employeeName').text(data.fullName);
			$('#dateOfBirth').text(data.dateOfBirth);
			$('#designation').text(data.designation);
			$('#gender').text(data.gender);
			
		}
	}
	

	//fetch flight details
	function fetchFlightDetails() {
		//alert("inside ajax")
		$.ajax({
			async: false,
			cache: false,
			method: "POST",
			url: "fetchFlightDetails",
			dataType: "json",
			data: {
				travelReqId: parseInt($('#travelReqId').val())
			},
			success: function(travelRequest) {
				//alert("inside success")
				populateFlightDetails(travelRequest);
			},
			error: function(jqXHR, textStatus, errorThrown)	{
				//alert(errorThrown);
				$('#errorMsg').val(errorThrown);
				$('#redirectForm').submit();
			}
		});
	}

	function populateFlightDetails(travelRequest) {
		//alert("inside populate data")
		var component = 'flight';
		
		if(travelRequest != undefined || travelRequest != null || travelRequest != '') {
			
			for(i=0;i<travelRequest.length;i++){
				
				if(i>0){

					function addRow(component) {

						var componentCount = $('.' + component + 'ChildDiv').length;
						var componentStaticHtml = indexElements(getComponentStaticHtml(component),componentCount + 1);
						var newComponentRowHtml = '<div class="row ' + component + 'ChildDiv">'
							+ '<div"  id="' + component + ++componentCount + '">'
								+ '<h5>' + component + ' ' + componentCount + '</h5>'
								+ '<br>'
								+ componentStaticHtml
							+ '</div>'
						+ '</div>';
						$('#' + component + 'ParentDiv').append(newComponentRowHtml);
						$('#' + component + 'RemoveIcon').show();
						if(componentCount == 4) {
							$('#' + component + 'AddIcon').hide();
						}
						$("#"+component+componentCount+ " .datepicker-only-init").each(function(){
							$(this).datetimepicker({
						        widgetPositioning: {
						            horizontal: 'left'
						        },
						        icons: {
						            time: "fa fa-clock-o",
						            date: "fa fa-calendar",
						            up: "fa fa-arrow-up",
						            down: "fa fa-arrow-down"
						        },
						        format: 'MM/DD/YYYY'
						    });
							$(this).val('');
						});
						$("#"+component+componentCount+ " .timepicker-init").each(function(){
							$(this).datetimepicker({
						        widgetPositioning: {
						            horizontal: 'left'
						        },
						        icons: {
						            time: "fa fa-clock-o",
						            date: "fa fa-calendar",
						            up: "fa fa-arrow-up",
						            down: "fa fa-arrow-down"
						        },
						        format: 'HH:mm'
						    });
							$(this).val('');
						});
						
						
					}
				}
				
				/*$('#1sourceCountry').text(travelRequest[i].srCountryId);
				$('#1sourceCity').text(travelRequest[i].srCityId);
				$('#1sourceAirport').text(travelRequest[i].srAirportId);
				$('#1destinationCountry').text(travelRequest[i].dsCountryId);
				$('#1destinationCity').text(travelRequest[i].dsCityId);
				$('#1destinationAirport').text(travelRequest[i].dsAirportId);
				$('#1departureDate').text(travelRequest[i].deptDate);
				$('#1departureTimePreference').text(travelRequest[i].deptTime);
				$('#1requiredArrivalTime').text(travelRequest[i].arrivalTime);
				$('#1portOfEntry').text(travelRequest[i].portOfEntry);
				$('#1frequentFlierNo').text(travelRequest[i].frquentFlierNo);
				$('#1frequentFlierAirline').text(travelRequest[i].frequentFlierAirline);
				$('#1frequentFlierFlightNo').text(travelRequest[i].frequentFlierFlightNo);
				$('#1projectIdF').text(travelRequest[i].projectName);
				$('#1processIdF').text(travelRequest[i].projectName);
				$('#1prospectNameF').text(travelRequest[i].prospectName);
				$('#1clientNameF').text(travelRequest[i].clientName);
				$('#1travelPurposeF').text(travelRequest[i].travelPurpose);
				$('#1notesF').text(travelRequest[i].notes);
				*/
				
				
				
				$('#'+parseInt(i+1)+'sourceCountry').text(travelRequest[i].srCountryId);
				$('#'+parseInt(i+1)+'sourceCity').text(travelRequest[i].srCityId);
				$('#'+parseInt(i+1)+'sourceAirport').text(travelRequest[i].srAirportId);
				$('#'+parseInt(i+1)+'destinationCountry').text(travelRequest[i].dsCountryId);
				$('#'+parseInt(i+1)+'destinationCity').text(travelRequest[i].dsCityId);
				$('#'+parseInt(i+1)+'destinationAirport').text(travelRequest[i].dsAirportId);
				$('#'+parseInt(i+1)+'departureDate').text(travelRequest[i].deptDate);
				$('#'+parseInt(i+1)+'departureTimePreference').text(travelRequest[i].deptTime);
				$('#'+parseInt(i+1)+'requiredArrivalTime').text(travelRequest[i].arrivalTime);
				$('#'+parseInt(i+1)+'portOfEntry').text(travelRequest[i].portOfEntry);
				$('#'+parseInt(i+1)+'frequentFlierNo').text(travelRequest[i].frquentFlierNo);
				$('#'+parseInt(i+1)+'frequentFlierAirline').text(travelRequest[i].frequentFlierAirline);
				$('#'+parseInt(i+1)+'frequentFlierFlightNo').text(travelRequest[i].frequentFlierFlightNo);
				$('#'+parseInt(i+1)+'projectIdF').text(travelRequest[i].projectName);
				$('#'+parseInt(i+1)+'processIdF').text(travelRequest[i].projectName);
				$('#'+parseInt(i+1)+'prospectNameF').text(travelRequest[i].prospectName);
				$('#'+parseInt(i+1)+'clientNameF').text(travelRequest[i].clientName);
				$('#'+parseInt(i+1)+'travelPurposeF').text(travelRequest[i].travelPurpose);
				//$('#'+i+'').text(travelRequest[i].tripMode);
				$('#'+i+'1notesF').text(travelRequest[i].notes);
			}
			

		}
	}

	//fetch hotel details
	function fetchHotelDetails() {
		//alert("inside ajax")
		$.ajax({
			async: false,
			cache: false,
			method: "POST",
			url: "fetchHotelDetails",
			dataType: "json",
			data: {
				travelReqId: parseInt($('#travelReqId').val())
			},
			success: function(travelRequest) {
				//alert("inside success")
				populateHotelDetails(travelRequest);
			},
			error: function(jqXHR, textStatus, errorThrown)	{
				//alert(errorThrown);
				$('#errorMsg').val(errorThrown);
				$('#redirectForm').submit();
			}
		});
	}

	function populateHotelDetails(travelRequest) {
		alert("inside populate data")
		alert(travelRequest);
		alert(travelRequest.length);
		if(travelRequest != undefined || travelRequest != null || travelRequest != ''|| travelRequest.length != 0) {
			//$('#employeeId').text(travelRequest.empId);
			
			for(i=0;i<travelRequest.length;i++){
				
				$('#'+parseInt(i+1)+'hotelCountry').text(travelRequest[i].countryId);
				$('#'+parseInt(i+1)+'hotelCity').text(travelRequest[i].cityId);
				$('#'+parseInt(i+1)+'checkInDate').text(travelRequest[i].checkInDate);
				$('#'+parseInt(i+1)+'checkOutDate').text(travelRequest[i].checkOutDate);
				$('#'+parseInt(i+1)+'rewardMemberNo').text(travelRequest[i].rewardMemberNo);
				$('#'+parseInt(i+1)+'clientNameH').text(travelRequest[i].clientName);
				$('#'+parseInt(i+1)+'bookNearLocation').text(travelRequest[i].travellingLocationAddr);
				$('#'+parseInt(i+1)+'preferredHotel').text(travelRequest[i].prefHotel);
				$('#'+parseInt(i+1)+'projectIdH').text(travelRequest[i].projectName);
				$('#'+parseInt(i+1)+'processIdH').text(travelRequest[i].projectName);
				$('#'+parseInt(i+1)+'prospectNameH').text(travelRequest[i].prospectName);
				$('#'+parseInt(i+1)+'travelPurposeH').text(travelRequest[i].travelPurpose);
				$('#'+parseInt(i+1)+'notesH').text(travelRequest[i].notes);
				
				
			}

		}
	}

	//fetch car details
	
	function fetchCarDetails() {
		//alert("inside ajax")
		$.ajax({
			async: false,
			cache: false,
			method: "POST",
			url: "fetchCarDetails",
			dataType: "json",
			data: {
				travelReqId: parseInt($('#travelReqId').val())
			},
			success: function(travelRequest) {
				//alert("inside success")
				populateCarDetails(travelRequest);
			},
			error: function(jqXHR, textStatus, errorThrown)	{
				//alert(errorThrown);
				$('#errorMsg').val(errorThrown);
				$('#redirectForm').submit();
			}
		});
	}

	function populateCarDetails(travelRequest) {
		//alert("inside populate data")
		//alert(travelRequest);
		if(travelRequest != undefined || travelRequest != null || travelRequest != '') {

			for(i=0;i<travelRequest.length;i++){

				$('#'+parseInt(i+1)+'carCountry').text(travelRequest[i].countryId);
				$('#'+parseInt(i+1)+'carCity').text(travelRequest[i].cityId);
				$('#'+parseInt(i+1)+'pickupAddress').text(travelRequest[i].pickUpLocation);
				$('#'+parseInt(i+1)+'pickupDateAndTime').text(travelRequest[i].pickUpTime);
				$('#'+parseInt(i+1)+'dropOffAddress').text(travelRequest[i].dropLoaction);
				$('#'+parseInt(i+1)+'carType').text(travelRequest[i].carType);
				$('#'+parseInt(i+1)+'carMembershipNo').text(travelRequest[i].mebershipNo);
				$('#'+parseInt(i+1)+'projectIdC').text(travelRequest[i].projectName);
				$('#'+parseInt(i+1)+'processIdC').text(travelRequest[i].projectName);
				$('#'+parseInt(i+1)+'prospectNameC').text(travelRequest[i].prospectName);
				$('#'+parseInt(i+1)+'clientNameC').text(travelRequest[i].clientName);
				$('#'+parseInt(i+1)+'travelPurposeC').text(travelRequest[i].travelPurpose);
				$('#'+parseInt(i+1)+'notesC').text(travelRequest[i].notes);


			}

		}
	}

//fetch companion details
	
	function fetchCompanionDetails() {
		//alert("inside ajax")
		$.ajax({
			async: false,
			cache: false,
			method: "POST",
			url: "fetchCompanionDetails",
			dataType: "json",
			data: {
				travelReqId: parseInt($('#travelReqId').val())
			},
			success: function(travelRequest) {
				//alert("inside success")
				populateCompanionDetails(travelRequest);
			},
			error: function(jqXHR, textStatus, errorThrown)	{
				alert(errorThrown);
				$('#errorMsg').val(errorThrown);
				$('#redirectForm').submit();
			}
		});
	}

	function populateCompanionDetails(travelRequest) {
		//alert("inside populate data")
		alert(travelRequest);
		if(travelRequest != undefined || travelRequest != null || travelRequest != '') {
			for(i=0;i<travelRequest.length;i++){

				$('#'+parseInt(i+1)+'companionName').text(travelRequest[i].companionName);
				$('#'+parseInt(i+1)+'relation').text(travelRequest[i].relation);
				$('#'+parseInt(i+1)+'1cdateOfBirth').text(travelRequest[i].cdateOfBirth);
				$('#'+parseInt(i+1)+'1cpassportNo').text(travelRequest[i].cpassportNo);
				$('#'+parseInt(i+1)+'1caddressOnPassport').text(travelRequest[i].caddressOnPassport);
				$('#'+parseInt(i+1)+'1cpassportIssuePlace').text(travelRequest[i].cpassportIssuePlace);
				$('#'+parseInt(i+1)+'1passportIssueDate').text(travelRequest[i].cpassportIssueDate);
				$('#'+parseInt(i+1)+'1cpassportExpiryDate').text(travelRequest[i].cpassportExpiryDate);
				$('#'+parseInt(i+1)+'1cECNRCheck').text(travelRequest[i].cECNRCheck);
				$('#'+parseInt(i+1)+'1cvisaType').text(travelRequest[i].cvisaType);
				$('#'+parseInt(i+1)+'1cvisaNo').text(travelRequest[i].cvisaNo);
				$('#'+parseInt(i+1)+'1cvisaIssueDate').text(travelRequest[i].cvisaIssueDate);
				$('#'+parseInt(i+1)+'1cvisaExpiryDate').text(travelRequest[i].cvisaExpiryDate);
				$('#'+parseInt(i+1)+'1cvalidity').text(travelRequest[i].cvalidity);
				$('#'+parseInt(i+1)+'1cdateOfTravel').text(travelRequest[i].cdateOfTravel);
				
			}

		}
	}
	
	

