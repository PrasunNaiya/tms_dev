$(function() {
	alert(JSON.stringify(getL0Model()));
	$.ajax({
        type: "POST",
        url: "testController",
        dataType: "json",
        data: {
        	l0Model: JSON.stringify(getL0Model())
        },
        success: function(data)
        {
            if (data !== null)
            {
            	alert("success");
            	alert(data);
                //alert(JSON.stringify(data));
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
        	alert("error");
        	alert(JSON.stringify(jqXHR));
        	alert(textStatus);
        	alert(errorThrown);
        }
    });
});

function L3Model() {
	this.v1 = null;
	this.v2 = null;
}

function getL3Model(i) {
	var l3Model = new L3Model();
	l3Model.v1 = "v1 value - " + i;
	l3Model.v2 = "v2 value - " + i;
	return l3Model;
}

function getL3List() {
	var l3List = [];
	l3List.push(getL3Model(1));
	l3List.push(getL3Model(2));
	l3List.push(getL3Model(3));
	return l3List;
}

function L2Model() {
	this.u1 = null;
	this.u2 = null;
}

function getL2Model() {
	var l2Model = new L2Model();
	l2Model.u1 = "u1 value";
	l2Model.u2 = "u2 value";
	return l2Model;
}

function L1Model() {
	this.t1 = null;
	this.t2 = null;
}

function getL1Model() {
	var l1Model = new L1Model();
	l1Model.t1 = "t1 value";
	l1Model.t2 = "t2 value";
	return l1Model;
}

function L0Model() {
	this.s1 = null;
	this.s2 = null;
	this.l1 = null;
	this.l2 = null;
	this.l3List = null;
}

function getL0Model() {
	var l0Model = new L0Model();
	l0Model.s1 = "s1 value";
	l0Model.s2 = "s2 value";
	l0Model.l1 = getL1Model();
	l0Model.l2 = getL2Model();
	l0Model.l3List = getL3List();
	return l0Model;
}

