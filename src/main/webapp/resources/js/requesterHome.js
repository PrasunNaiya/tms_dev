$(function() {
	fetchTravelRequestList();
});

function fetchTravelRequestList() {
	$.ajax({
		async: false,
		method: "POST",
		url: "fetchRequestInformation",
		dataType: "json",
		data: {
			empId: $('#userId').val()
		},
		success: function(travelRequestList) {
			populateTravelRequestList(travelRequestList);
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			//alert(errorThrown);
			$('#errorMsg').val(errorThrown);
			$('#redirectForm').submit();
		}
	});
}

function populateTravelRequestList(travelRequestList) {
	var tableRow = '';
	$.each(travelRequestList, function(index, travelRequest) {
		tableRow += '<tr><td style="cursor: pointer;" onclick="showTravelRequestData(' + travelRequest.travelReqId + ')">' + travelRequest.travelReqId + '</td>' + 
			'<td>' + travelRequest.creationTimeStamp + '</td></tr>';
	});
	$('#travelRequestList tbody').append(tableRow);
}

function showTravelRequestData(travelReqId) {
	$('#travelReqId').val(travelReqId);
	$('#targetPage').val("viewTravelRequestForm");
	$('#redirectForm').submit();	
}