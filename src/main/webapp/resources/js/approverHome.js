$(function() {
	fetchTravelRequestList();
});

function fetchTravelRequestList() {
	
	$.ajax({
		async: false,
		method: "POST",
		url: "approvePendingHotelRequest",
		dataType: "json",
		data: {
			empId: $('#userId').val()
		},
		success: function(travelRequestList) {
			
			populatePendingApproveRequestList(travelRequestList);
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			//alert(errorThrown);
			$('#errorMsg').val(errorThrown);
			$('#redirectForm').submit();
		}
	});
}

function populatePendingApproveRequestList(travelRequestList) {
	
	var tableRow = '';
	$.each(travelRequestList, function(index, travelRequest) {
		
		tableRow += '<tr><td style="cursor: pointer;" onclick="showTravelRequestData(' + travelRequest.rrqNo + ')">' + travelRequest.rrqNo + '</td></tr>'
	});
	$('#travelRequestList tbody').append(tableRow);
}

function showTravelRequestData(travelReqId) {
	$('#travelReqId').val(travelReqId);
	$('#targetPage').val("viewTravelRequestForm");
	$('#redirectForm').submit();	
}