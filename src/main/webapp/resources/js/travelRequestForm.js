var flightStaticHtml;
var hotelStaticHtml;
var carStaticHtml;
var companionStaticHtml;

$(function(){
	
	$(".internationalField").hide();
	
	$('.datepicker-only-init').datetimepicker({
        widgetPositioning: {
            horizontal: 'left'
        },
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        format: 'MM/DD/YYYY'
    });
	
	$('.timepicker-init').datetimepicker({
        widgetPositioning: {
            horizontal: 'left'
        },
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        format: 'HH:mm'
    });
	
	$('.datepicker-init').datetimepicker({
        widgetPositioning: {
            horizontal: 'left'
        },
        icons: {
            time: "fa fa-clock-o",
            date: "fa fa-calendar",
            up: "fa fa-arrow-up",
            down: "fa fa-arrow-down"
        },
        format: 'MM/DD/YYYY HH:mm'
    });
	
	fetchEmployeeDetails($('#employeeId').val());
	fetchCountries();
	fetchProjects();
	fetchProcesses();
	setComponentStaticHtml();
	formOnLoad();
});

function fetchEmployeeDetails(empId) {
	$.ajax({
		async: false,
		method: "POST",
		url: "fetchEmployeeDetails",
		dataType: "json",
		data: {
			empId: empId
		},
		success: function(userDetails) {
			populateEmployeeDetails(userDetails);
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			//alert(errorThrown);
			$('#errorMsg').val(errorThrown);
			$('#redirectForm').submit();
		}
	});
}

function populateEmployeeDetails(userDetails) {
	if(userDetails != undefined || userDetails != null || userDetails != '') {
		$('#employeeName').val(userDetails.fullName);
		$('#dateOfBirth').val(userDetails.dateOfBirth);
		$('#gender').val(userDetails.gender);
		$('#designation').val(userDetails.designation);
	}
}

function fetchCountries() {
	$.ajax({
		async: false,
		method: "POST",
		url: "fetchCountries",
		dataType: "json",
		success: function(countryList) {
			populateCountryList(countryList);
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			//alert(errorThrown);
			$('#errorMsg').val(errorThrown);
			$('#redirectForm').submit();
		}
	});
}

function populateCountryList(countryList) {
	if(countryList != undefined || countryList != null || countryList != '' || countryList.lenght != 0) {
		var optionHtml = '<option value="-1">Select</option>';
		$.each(countryList, function(i,country) {
			optionHtml += '<option value="' + country.countryId + '">' + country.countryName + '</option>';
		});
		$('#1sourceCountry').children().remove();
		$('#1sourceCountry').html(optionHtml);
		$('#1destinationCountry').children().remove();
		$('#1destinationCountry').html(optionHtml);
		$('#1hotelCountry').children().remove();
		$('#1hotelCountry').html(optionHtml);
		$('#1carCountry').children().remove();
		$('#1carCountry').html(optionHtml);
	}
}

function fetchProjects() {
	$.ajax({
		async: false,
		method: "POST",
		url: "fetchProjects",
		dataType: "json",
		success: function(projectList) {
			populateProjects(projectList);
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			//alert(errorThrown);
			$('#errorMsg').val(errorThrown);
			$('#redirectForm').submit();
		}
	});
}

function fetchProcesses() {
	$.ajax({
		async: false,
		method: "POST",
		url: "fetchProcesses",
		dataType: "json",
		success: function(processList) {
			populateProcesses(processList);
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			//alert(errorThrown);
			$('#errorMsg').val(errorThrown);
			$('#redirectForm').submit();
		}
	});
}

function populateProjects(projectList) {
	if(projectList != undefined || projectList != null || projectList != '' || projectList.lenght != 0) {
		var optionHtml = '<option value="-1">Select</option>';
		$.each(projectList, function(i,project) {
			optionHtml += '<option value="' + project.projectId + '">' + project.projectName + '</option>';
		});
		$('#1projectIdF').children().remove();
		$('#1projectIdF').html(optionHtml);
		$('#1projectIdH').children().remove();
		$('#1projectIdH').html(optionHtml);
		$('#1projectIdC').children().remove();
		$('#1projectIdC').html(optionHtml);
	}
}

function populateProcesses(processList) {
	if(processList != undefined || processList != null || processList != '' || processList.lenght != 0) {
		var optionHtml = '<option value="-1">Select</option>';
		$.each(processList, function(i,process) {
			optionHtml += '<option value="' + process.projectId + '">' + process.projectName + '</option>';
		});
		$('#1processIdF').children().remove();
		$('#1processIdF').html(optionHtml);
		$('#1processIdH').children().remove();
		$('#1processIdH').html(optionHtml);
		$('#1processIdC').children().remove();
		$('#1processIdC').html(optionHtml);
	}
}

function setComponentStaticHtml() {
	flightStaticHtml = $('#flightStaticHtml').html();
	hotelStaticHtml = $('#hotelStaticHtml').html();
	carStaticHtml = $('#carStaticHtml').html();
	companionStaticHtml = $('#companionStaticHtml').html();
}

function toggleTravelType(travelType) {
	if(travelType == "International") {
		$(".internationalField").show();
	} else {
		$(".internationalField").hide();
		$(".internationalField input").val("");
	}
}

function toggleSubRequests(component) {
	if($('#' + component + 'Requirement').prop("checked")) {
		$('#' + component + 'DetailsDiv').show();
	} else {
		$('#' + component + 'DetailsDiv').hide();
	}
}

function populateCityList(point,elementId,countryId) {
	if(countryId == -1) {
		$('#' + elementId.charAt(0) + point + 'City').val("-1");
		$('#' + elementId.charAt(0) + point + 'City').attr('disabled','disabled');
		if(point == "flight") {			
			$('#' + elementId.charAt(0) + point + 'Airport').val("-1");
			$('#' + elementId.charAt(0) + point + 'Airport').attr('disabled','disabled');
		} else if(point == "hotel") {
			$('#' + elementId.charAt(0) + 'preferredHotel').val("");
			$('#' + elementId.charAt(0) + 'preferredHotel').attr('disabled','disabled');
			$('#' + elementId.charAt(0) + 'bookNearLocation').val("");
			$('#' + elementId.charAt(0) + 'bookNearLocation').attr('disabled','disabled');
		} else if(point == "car") {
			$('#' + elementId.charAt(0) + 'pickupAddress').val("");
			$('#' + elementId.charAt(0) + 'pickupAddress').attr('disabled','disabled');
			$('#' + elementId.charAt(0) + 'dropOffAddress').val("");
			$('#' + elementId.charAt(0) + 'dropOffAddress').attr('disabled','disabled');
		}
	} else {
		var cityList = fetchCities(countryId);
		if(cityList != undefined || cityList != null || cityList != '' || cityList.lenght != 0) {
			var optionHtml = '<option value="-1">Select</option>';
			$.each(cityList, function(i,city) {
				optionHtml += '<option value="' + city.cityId + '">' + city.cityName + '</option>';
			});
			$('#' + elementId.charAt(0) + point + 'City').children().remove();
			$('#' + elementId.charAt(0) + point + 'City').html(optionHtml);
		}
			$('#' + elementId.charAt(0) + point + 'City').removeAttr('disabled');
		if(point == "flight") {
			$('#' + elementId.charAt(0) + point + 'Airport').val("-1");
			$('#' + elementId.charAt(0) + point + 'Airport').attr('disabled','disabled');
		} else if(point == "hotel") {
			$('#' + elementId.charAt(0) + 'preferredHotel').val("");
			$('#' + elementId.charAt(0) + 'preferredHotel').attr('disabled','disabled');
			$('#' + elementId.charAt(0) + 'bookNearLocation').val("");
			$('#' + elementId.charAt(0) + 'bookNearLocation').attr('disabled','disabled');
		} else if(point == "car") {
			$('#' + elementId.charAt(0) + 'pickupAddress').val("");
			$('#' + elementId.charAt(0) + 'pickupAddress').attr('disabled','disabled');
			$('#' + elementId.charAt(0) + 'dropOffAddress').val("");
			$('#' + elementId.charAt(0) + 'dropOffAddress').attr('disabled','disabled');
		}
	}
}

function fetchCities(countryId) {
	var cities;
	$.ajax({
		async: false,
		method: "POST",
		url: "fetchCities",
		dataType: "json",
		data: {
			countryId: parseInt(countryId)
		},
		success: function(cityList) {
			cities = cityList;
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			//alert(errorThrown);
			$('#errorMsg').val(errorThrown);
			$('#redirectForm').submit();
		}
	});
	return cities;
}

function populateAirportList(point,elementId,cityId) {
	if(cityId == -1) {
		$('#' + elementId.charAt(0) + point + 'Airport').val("-1");
		$('#' + elementId.charAt(0) + point + 'Airport').attr('disabled','disabled');
	} else {
		var airportList = fetchAirports(cityId);
		if(airportList != undefined || airportList != null || airportList != '' || airportList.lenght != 0) {
			var optionHtml = '<option value="-1">Select</option>';
			$.each(airportList, function(i,airport) {
				optionHtml += '<option value="' + airport.airportId + '">' + airport.airportName + '</option>';
			});
			$('#' + elementId.charAt(0) + point + 'Airport').children().remove();
			$('#' + elementId.charAt(0) + point + 'Airport').html(optionHtml);
		}
		$('#' + elementId.charAt(0) + point + 'Airport').removeAttr('disabled');
	}
}

function fetchAirports(cityId) {
	var airports;
	$.ajax({
		async: false,
		method: "POST",
		url: "fetchAirports",
		dataType: "json",
		data: {
			cityId: parseInt(cityId)
		},
		success: function(airportList) {
			airports = airportList;
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			//alert(errorThrown);
			$('#errorMsg').val(errorThrown);
			$('#redirectForm').submit();
		}
	});
	return airports;
}

function showProjectProcessProspectSpecificFields(component,elementId,pppSelection) {
	
	if(pppSelection == "") {
		$('#' + elementId.charAt(0) + 'projectDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'projectDiv' + component.toUpperCase().charAt(0)).find('select').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'processDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'processDiv' + component.toUpperCase().charAt(0)).find('select').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'prospectDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'prospectDiv' + component.toUpperCase().charAt(0)).find('input').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'clientNameDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'clientNameDiv' + component.toUpperCase().charAt(0)).find('input').removeClass('required');
		
	} else if(pppSelection == "Project") {
		$('#' + elementId.charAt(0) + 'processDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'processDiv' + component.toUpperCase().charAt(0)).find('select').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'prospectDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'prospectDiv' + component.toUpperCase().charAt(0)).find('input').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'clientNameDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'clientNameDiv' + component.toUpperCase().charAt(0)).find('input').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'projectId' + component.toUpperCase().charAt(0)).val('-1');
		
		$('#' + elementId.charAt(0) + 'projectDiv' + component.toUpperCase().charAt(0)).show();
		$('#' + elementId.charAt(0) + 'projectDiv' + component.toUpperCase().charAt(0)).find('select').addClass('required');
		
	}  else if(pppSelection == "Process") {
		$('#' + elementId.charAt(0) + 'projectDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'projectDiv' + component.toUpperCase().charAt(0)).find('select').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'prospectDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'prospectDiv' + component.toUpperCase().charAt(0)).find('input').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'clientNameDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'clientNameDiv' + component.toUpperCase().charAt(0)).find('input').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'processId' + component.toUpperCase().charAt(0)).val('-1');
		
		$('#' + elementId.charAt(0) + 'processDiv' + component.toUpperCase().charAt(0)).show();
		$('#' + elementId.charAt(0) + 'processDiv' + component.toUpperCase().charAt(0)).find('select').addClass('required');
		
	}  else if(pppSelection == "Prospect") {
		$('#' + elementId.charAt(0) + 'projectDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'projectDiv' + component.toUpperCase().charAt(0)).find('select').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'processDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'processDiv' + component.toUpperCase().charAt(0)).find('select').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'clientName' + component.toUpperCase().charAt(0)).val('');
		
		$('#' + elementId.charAt(0) + 'clientNameDiv' + component.toUpperCase().charAt(0)).hide();
		$('#' + elementId.charAt(0) + 'clientNameDiv' + component.toUpperCase().charAt(0)).find('input').removeClass('required');
		
		$('#' + elementId.charAt(0) + 'prospectName' + component.toUpperCase().charAt(0)).val('');
		
		$('#' + elementId.charAt(0) + 'prospectDiv' + component.toUpperCase().charAt(0)).show();
		$('#' + elementId.charAt(0) + 'prospectDiv' + component.toUpperCase().charAt(0)).find('input').addClass('required');
	}
}

function showClientName(component,elementId,projectId) {
	if(projectId == "-1") {
		$('#' + elementId.charAt(0) + 'clientNameDiv' + component.toUpperCase().charAt(0)).hide();
	} else {
		$('#' + elementId.charAt(0) + 'clientName' + component.toUpperCase().charAt(0)).val(fetchClientName(projectId));
		$('#' + elementId.charAt(0) + 'clientNameDiv' + component.toUpperCase().charAt(0)).show();
	}	
}

function fetchClientName(projectId) {
	var clientName = '';
	$.ajax({
		async: false,
		method: "POST",
		url: "fetchClient",
		dataType: "json",
		data: {
			projectId: parseInt(projectId)
		},
		success: function(client) {
			if(client != undefined || client != null || client != '') {
				clientName = client.customerName;
			}			
		},
		error: function(jqXHR, textStatus, errorThrown)	{
			//alert(errorThrown);
			$('#errorMsg').val(errorThrown);
			$('#redirectForm').submit();
		}
	});	
	return clientName;
}

function suggestProspectName(component,elementId,projectName) {
	//future scope
}

function toggleRoundTripFields(tripMode) {
	
}

function toggleCityBasedHotelInfo(elementId,cityId) {
	if(cityId == -1) {
		$('#' + elementId.charAt(0) + 'preferredHotel').val("");
		$('#' + elementId.charAt(0) + 'preferredHotel').attr('disabled','disabled');
		$('#' + elementId.charAt(0) + 'bookNearLocation').val("");
		$('#' + elementId.charAt(0) + 'bookNearLocation').attr('disabled','disabled');
	} else {
		$('#' + elementId.charAt(0) + 'preferredHotel').val("");
		$('#' + elementId.charAt(0) + 'preferredHotel').removeAttr('disabled','disabled');
		$('#' + elementId.charAt(0) + 'bookNearLocation').val("");
		$('#' + elementId.charAt(0) + 'bookNearLocation').removeAttr('disabled','disabled');
	}
}

function toggleCityBasedCarInfo(elementId,cityId) {
	if(cityId == -1) {
		$('#' + elementId.charAt(0) + 'pickupAddress').val("");
		$('#' + elementId.charAt(0) + 'pickupAddress').attr('disabled','disabled');
		$('#' + elementId.charAt(0) + 'dropOffAddress').val("");
		$('#' + elementId.charAt(0) + 'dropOffAddress').attr('disabled','disabled');
	} else {
		$('#' + elementId.charAt(0) + 'pickupAddress').val("");
		$('#' + elementId.charAt(0) + 'pickupAddress').removeAttr('disabled','disabled');
		$('#' + elementId.charAt(0) + 'dropOffAddress').val("");
		$('#' + elementId.charAt(0) + 'dropOffAddress').removeAttr('disabled','disabled');
	}
}

function toggleReturnPickupFields(elementId,value) {
	
}

function addRow(component) {

	var componentCount = $('.' + component + 'ChildDiv').length;
	var componentStaticHtml = indexElements(getComponentStaticHtml(component),componentCount + 1);
	var newComponentRowHtml = '<div class="row ' + component + 'ChildDiv">'
		+ '<div"  id="' + component + ++componentCount + '">'
			+ '<h5>' + component + ' ' + componentCount + '</h5>'
			+ '<br>'
			+ componentStaticHtml
		+ '</div>'
	+ '</div>';
	$('#' + component + 'ParentDiv').append(newComponentRowHtml);
	$('#' + component + 'RemoveIcon').show();
	if(componentCount == 4) {
		$('#' + component + 'AddIcon').hide();
	}
	$("#"+component+componentCount+ " .datepicker-only-init").each(function(){
		$(this).datetimepicker({
	        widgetPositioning: {
	            horizontal: 'left'
	        },
	        icons: {
	            time: "fa fa-clock-o",
	            date: "fa fa-calendar",
	            up: "fa fa-arrow-up",
	            down: "fa fa-arrow-down"
	        },
	        format: 'MM/DD/YYYY'
	    });
		$(this).val('');
	});
	$("#"+component+componentCount+ " .timepicker-init").each(function(){
		$(this).datetimepicker({
	        widgetPositioning: {
	            horizontal: 'left'
	        },
	        icons: {
	            time: "fa fa-clock-o",
	            date: "fa fa-calendar",
	            up: "fa fa-arrow-up",
	            down: "fa fa-arrow-down"
	        },
	        format: 'HH:mm'
	    });
		$(this).val('');
	});
	
	
}

function indexElements(componentStaticHtml,index) {
	componentStaticHtml = componentStaticHtml.replace(/class="form-control"/g, 'class="form-control required "' );
	componentStaticHtml = componentStaticHtml.replace(/class="form-control datepicker-only-init"/g, 'class="form-control datepicker-only-init required "' );
	componentStaticHtml = componentStaticHtml.replace(/class="form-control timepicker-init"/g, 'class="form-control timepicker-init required "' );
	componentStaticHtml = componentStaticHtml.replace(/for="1/g, 'for="' + index);
	componentStaticHtml = componentStaticHtml.replace(/name="1/g, 'name="' + index);
	componentStaticHtml = componentStaticHtml.replace(/id="1/g, 'id="' + index);
	return componentStaticHtml;
}

function getComponentStaticHtml(component) {
	if(component == 'flight') {
		return flightStaticHtml;
	} else if(component == 'hotel') {
		return hotelStaticHtml;
	} else if(component == 'car') {
		return carStaticHtml;
	}else
		return companionStaticHtml;
}

function removeRow(component) {
	
	var componentCount = $('.' + component + 'ChildDiv').length;
	$('#' + component + componentCount).parent().remove();
	$('#' + component + 'AddIcon').show();
	if(--componentCount == 1) {
		subFormValidation(divId,type);
		$('#' + component + 'RemoveIcon').hide();
	}
	
}


function removeRedBorder(){
	$('.form-group input').on('click',function(){
		$(this).removeAttr('style');
	});
	$('.form-group select').on('click',function(){
		$(this).removeAttr('style');
	});
	
}

function formOnLoad(){
	$('#travelType').on('change',function(){
		if($(this).val()=='International'){
			$('.internationalField input').addClass('required');
		}else{
			$('.internationalField input').removeClass('required');
		}
		
	});
	$(document).on('click','.redError',function(){
		$(this).removeClass('redError');
	});
	
	$('#flightRequirement').on('click',function(){
		if($(this).is(':checked')){
			//$('#flightStaticHtml input').addClass('required');
			 subFormValidation('flightStaticHtml','add');
			 
			
		}else{
			subFormValidation('flightStaticHtml','remove');
			
		}
		
	});
	$('#hotelRequirement').on('click',function(){
		if($(this).is(':checked')){
			//$('#flightStaticHtml input').addClass('required');
			 subFormValidation('hotelStaticHtml','add');
			 
			
		}else{
			subFormValidation('hotelStaticHtml','remove');
			
		}
		
	});
	$('#carRequirement').on('click',function(){
		if($(this).is(':checked')){
			//$('#flightStaticHtml input').addClass('required');
			 subFormValidation('carStaticHtml','add');
			 
			
		}else{
			subFormValidation('carStaticHtml','remove');
			
		}
		
	});
	$('#companionRequirement').on('click',function(){
		if($(this).is(':checked')){
			//$('#flightStaticHtml input').addClass('required');
			 subFormValidation('companionStaticHtml','add');
			 
			
		}else{
			subFormValidation('companionStaticHtml','remove');
			
		}
		
	});
	

	/*if($('#contactNo').val().length<10 || $('#contactNo').val().length>10)
	{
		alert("Contact No should be of 10 characters");
	}*/
}
function subFormValidation(divId,type){
	
	
		if(type=='add'){
			$('#'+divId+' input').addClass('required');
			$('#'+divId+' textarea').addClass('required');
			$('#'+divId+' select').addClass('required');
			
			$('#'+divId+' .notImp').each(function(){
				$(this).removeClass('required');
			});
			$('#'+divId+' .redError').each(function(){
				$(this).removeClass('redError');
			});
		}else{
			
			$('#'+divId+' .required').each(function(){
				$(this).removeClass('required');
			});
			$('#'+divId+' .redError').each(function(){
				$(this).removeClass('redError');
			});
			
		}
		
	
	
}
function formSubmit(){
	
	removeRedBorder();

	var fieldIdArr = [];
	var errMsgArr = [];
	var errMsgConcat = "";
	$('#travelType').on('change',function(){
		if($(this).val()=='International'){
			$('.internationalField input').addClass('required');
		}else{
			$('.internationalField input').removeClass('required');
		}
		
	});
	
	
	$('.required').each(function(){
		var getVal = $(this).val();
		var errorMsg = $(this).attr('data-msg');
		
		if(getVal == ""||getVal==null||getVal==-1){
			errMsgArr.push(errorMsg);
			$(this).addClass('redError');
		}
	});
	
		
	/*$('#1pppSelectionF').on('change', function() {
		  alert("change");
		  if($('##1pppSelectionF').val()=='project'){
			  $('.form-control project').addClass('required'); 
		  }
		  else{
				$('.form-control project').removeClass('required');
			}
		});*/
	
	
	
	/*if($('#contactNo').val().length<10 || $('#contactNo').val().length>10)
	{
	fieldIdArr.push('#contactNo');
	errMsgArr.push("Contact No should be of 10 characters");
	}
	
	if($('#contactNo').val()== null || $('#contactNo').val()==''){
		fieldIdArr.push('#contactNo');
		errMsgArr.push("enter contact No");
	}

	if($('#travelType').val()== null || $('#travelType').val()==''){
		fieldIdArr.push('#travelType');
		errMsgArr.push("select travel type");

	}
	if($('#travelType').val() == "International") {	
		if($('#passportNo').val()==null || $('#passportNo').val()==''){
			fieldIdArr.push('#passportNo');
			errMsgArr.push("Provide passport number");
		}
		if($('#passportExpiryDate').val()==null || $('#passportExpiryDate').val()==''){
			fieldIdArr.push('#passportExpiryDate');
			errMsgArr.push("Provide passport expiry date");
		}
		if($('#visaType').val()==null || $('#visaType').val()==''){
			fieldIdArr.push('#visaType');
			errMsgArr.push("provide visa type");
		}
		if($('#visaExpiryDate').val()==null || $('#visaExpiryDate').val()==''){
			fieldIdArr.push('#visaExpiryDate');
			errMsgArr.push("Provide visa Expiry Date");
		}
		if($('#visaNo').val()==null || $('#visaNo').val()==''){
			fieldIdArr.push('#visaNo');
			errMsgArr.push("provide visa No");
		}

	}
	//flight
	if($('#flightRequirement').val() == "flight") {

		if($('#1sourceCountry').val()==null || $('#1sourceCountry').val()==''){
			fieldIdArr.push('#1sourceCountry');
			errMsgArr.push("please select source country");
		}
		if($('#1destinationCountry').val()==null || $('#1destinationCountry').val()==''){
			fieldIdArr.push('#1destinationCountry');
			errMsgArr.push("Please select destination country");
		}
		if($('#1sourceCity').val()==null || $('#1sourceCity').val()==''){
			fieldIdArr.push('#1sourceCity');
			errMsgArr.push("please select source city");
		}
		if($('#1destinationCity').val()==null || $('#1destinationCity').val()==''){
			fieldIdArr.push('#1destinationCity');
			errMsgArr.push("please select destination city");
		}
		if($('#1sourceAirport').val()==null || $('#1sourceAirport').val()==''){
			fieldIdArr.push('#1sourceAirport');
			errMsgArr.push("Please select source Airport");
		}
		if($('#1destinationAirport').val()==null || $('#1destinationAirport').val()==''){
			fieldIdArr.push('#1destinationAirport');
			errMsgArr.push("Please select destination airport");
		}
		if($('#1departureDate').val()==null || $('#1departureDate').val()==''){
			fieldIdArr.push('#1departureDate');
			errMsgArr.push("Provide departure date");
		}
		if($('#1departureTimePreference').val()==null || $('#1departureTimePreference').val()==''){
			fieldIdArr.push('#1departureTimePreference');
			errMsgArr.push("provide departure Time Preference");
		}
		if($('#1pppSelectionF').val()==null || $('#1pppSelectionF').val()==''){
			fieldIdArr.push('#1pppSelectionF');
			errMsgArr.push("select project/process/prospect for flight");
		}
		if($('#1pppSelectionF').val()=="Project" ){
			
			if($('#1projectIdF').val()==null || $('#1projectIdF').val()==''){
				fieldIdArr.push('#1projectIdF');
				errMsgArr.push("select project");
			}
			
			if($('#1clientNameF').val()==null || $('#1clientNameF').val()==''){
				fieldIdArr.push('#1clientNameF');
				errMsgArr.push("provide client name");
			}
		}
          if($('#1pppSelectionF').val()=="Process" ){
			
			if($('#1processIdF').val()==null || $('#1processIdF').val()==''){
				fieldIdArr.push('#1processIdF');
				errMsgArr.push("select process");
			}
			
			if($('#1clientNameF').val()==null || $('#1clientNameF').val()==''){
				fieldIdArr.push('#1clientNameF');
				errMsgArr.push("provide client name");
			}
		}
          if($('#1pppSelectionF').val()=="Prospect" ){
  			
  			if($('#1prospectNameF').val()==null || $('#1prospectNameF').val()==''){
  				fieldIdArr.push('#1prospectNameF');
  				errMsgArr.push("provide prospect");
  			}
  			
  			if($('#1clientNameF').val()==null || $('#1clientNameF').val()==''){
  				fieldIdArr.push('#1clientNameF');
  				errMsgArr.push("provide client name");
  			}
  		}
		
		if($('#1travelPurposeF').val()==null || $('#1travelPurposeF').val()==''){
			fieldIdArr.push('#1travelPurposeF');
			errMsgArr.push("select travel Purpose for flight");
		}
		if($('#1notesF').val()==null || $('#1notesF').val()==''){
			fieldIdArr.push('#1notesF');
			errMsgArr.push("Provide notes for flight");
		}

	}

	//hotel

	if($('#hotelRequirement').val() == "hotel"){


		if($('#1hotelCountry').val()==null || $('#1hotelCountry').val()==''){
			fieldIdArr.push('#1hotelCountry');
			errMsgArr.push("please select hotel country");
		}
		if($('#1hotelCity').val()==null || $('#1hotelCity').val()==''){
			fieldIdArr.push('#1hotelCity');
			errMsgArr.push("Please select hotel city");
		}
		if($('#1checkInDate').val()==null || $('#1checkInDate').val()==''){
			fieldIdArr.push('#1checkInDate');
			errMsgArr.push("please select check-in Date");
		}
		if($('#1checkOutDate').val()==null || $('#1checkOutDate').val()==''){
			fieldIdArr.push('#1checkOutDate');
			errMsgArr.push("please select check-out date");
		}
		if($('#1bookNearLocation').val()==null || $('#1bookNearLocation').val()==''){
			fieldIdArr.push('#1bookNearLocation');
			errMsgArr.push("Provide near location");
		}
		if($('#1pppSelectionH').val()==null || $('#1pppSelectionH').val()==''){
			fieldIdArr.push('#1pppSelectionH');
			errMsgArr.push("Please select project/process/prospect for hotel");
		}
		if($('#1travelPurposeH').val()==null || $('#1travelPurposeH').val()==''){
			fieldIdArr.push('#1travelPurposeH');
			errMsgArr.push("Please select travel purpose");
		}
		if($('#1notesH').val()==null || $('#1notesH').val()==''){
			fieldIdArr.push('#1notesH');
			errMsgArr.push("provide notes for hotel");
		}


	}

	//car
	if($('#carRequirement').val() == "car"){


		if($('#1carCountry').val()==null || $('#1carCountry').val()==''){
			fieldIdArr.push('#1carCountry');
			errMsgArr.push("please select car country");
		}
		if($('#1carCity').val()==null || $('#1carCity').val()==''){
			fieldIdArr.push('#1carCity');
			errMsgArr.push("Please select car city");
		}
		if($('#1pickupAddress').val()==null || $('#1pickupAddress').val()==''){
			fieldIdArr.push('#1pickupAddress');
			errMsgArr.push("provide pick up address");
		}
		if($('#1pickupDateAndTime').val()==null || $('#1pickupDateAndTime').val()==''){
			fieldIdArr.push('#1pickupDateAndTime');
			errMsgArr.push("provide pick up date and time");
		}
		if($('#1dropOffAddress').val()==null || $('#1dropOffAddress').val()==''){
			fieldIdArr.push('#1dropOffAddress');
			errMsgArr.push("Provide drop off address");
		}
		if($('#1carType').val()==null || $('#1carType').val()==''){
			fieldIdArr.push('#1carType');
			errMsgArr.push("Please select car type");
		}

		if($('#1pppSelectionC').val()==null || $('#1pppSelectionC').val()==''){
			fieldIdArr.push('#1pppSelectionC');
			errMsgArr.push("Please select project/process/prospect for car");
		}


		if($('#1travelPurposeC').val()==null || $('#1travelPurposeC').val()==''){
			fieldIdArr.push('#1travelPurposeH');
			errMsgArr.push("Please select travel purpose for car");
		}
		if($('#1notesC').val()==null || $('#1notesC').val()==''){
			fieldIdArr.push('#1notesC');
			errMsgArr.push("provide notes for car");
		}
	}

*/

	if(errMsgArr.length>0){
		//alert(errMsgArr.length)
		errMsgArr.forEach(function(obj){
			//errMsgConcat = obj;
			errMsgConcat += obj+"\n";

		});
		alert(errMsgConcat);

		fieldIdArr.forEach(function(obj2){
			//alert(obj2);
			$(obj2).css('border-color', 'red');
		});

	}else{
//		alert(JSON.stringify(getTravelRequestTModel()));
		travelSubmitAjax();
		//alert(JSON.stringify(getEmployeeDetails()));
	//	alert("form submitted succcessfully");
		
	}

}

//json submit


function travelSubmitAjax(){
	alert(JSON.stringify(getTravelRequestTModel()));
	$.ajax({
        type: "POST",
        url: "travelRequestSubmitController",
        dataType: "json",
        data: {
        	travelRequestDetails: JSON.stringify(getTravelRequestTModel())
        },
        success: function(data)
        {
            if (data !== null)
            {
            	alert("success");
            	alert(data);
                //alert(JSON.stringify(data));
            }
        },
        error: function(jqXHR, textStatus, errorThrown)
        {
        	alert("error");
        	alert(JSON.stringify(jqXHR));
        	alert(textStatus);
        	alert(errorThrown);
        }
    });
	}



function FlightRequestTModel() {
	this.flightRequestId = null;
	this.srCountryId = null;
	this.srCountryId = null;
	this.srCityId = null;
	this.srAirportId = null;
	this.dsCountryId = null;
	this.dsCityId = null;
	this.dsAirportId = null;
//	this.cost = null;
//	this.isBudgeted = null;
//	this.isBillable = null;
//	this.isApproved = null;
//	this.isRejected = null;
//	this.isCostApproved = null;
//	this.isCostRejected = null;
//	this.isActive = null;
//	this.rrqNo = null;
	this.deptDate = null;
	this.deptTime = null;
	this.arrivalTime = null;
//	this.seatPreference = null;
	this.prefAirlines = null;
	this.frquentFlierNo = null;
	this.clientName = null;
	this.travellingLocationAddr = null;
	this.travelPurpose = null;
	this.projectId = null;
	this.processId = null;
//	this.projectName = null;
	this.prospectName = null;
//	this.tripMode = null;
	this.notes = null;
//	this.currency = null;
//	this.createdBy = null;
//	this.modifiedBy = null;
//	this.creationTimeStamp = null;
//	this.modificationTimeStamp = null;

}



function getFlightRequestTModel(i) {
	var flightRequestTModel = new FlightRequestTModel();
		
	//flightRequestTModel.flightRequestId = null;
	flightRequestTModel.srCountryId = $('#'+i+'sourceCountry').val();;
	flightRequestTModel.srCityId = $('#'+i+'sourceCity').val();;
	flightRequestTModel.srAirportId = $('#'+i+'sourceAirport').val();;
	flightRequestTModel.dsCountryId = $('#'+i+'destinationCountry').val();;
	flightRequestTModel.dsCityId = $('#'+i+'destinationCity').val();;
	flightRequestTModel.dsAirportId = $('#'+i+'destinationAirport').val();
//	flightRequestTModel.cost = null;
//	flightRequestTModel.isBudgeted = null;
//	flightRequestTModel.isBillable = null;
//	flightRequestTModel.isApproved = null;
//	flightRequestTModel.isRejected = null;
//	flightRequestTModel.isCostApproved = null;
//	flightRequestTModel.isCostRejected = null;
//	flightRequestTModel.isActive = null;
//	flightRequestTModel.rrqNo = null;
	flightRequestTModel.deptDate = $('#'+i+'departureDate').val();
	flightRequestTModel.deptTime = $('#'+i+'departureTimePreference').val();
	flightRequestTModel.arrivalTime = $('#'+i+'requiredArrivalTime').val();
//	flightRequestTModel.seatPreference = $('#'+i+'departureDate').val();;
	flightRequestTModel.prefAirlines = $('#'+i+'preferredAirlines').val();;
	flightRequestTModel.frquentFlierNo = $('#'+i+'frequentFlierNo').val();;
	flightRequestTModel.clientName = $('#'+i+'clientNameF').val();;
//	flightRequestTModel.travellingLocationAddr = $('#'+i+'departureDate').val();;
	flightRequestTModel.travelPurpose = $('#'+i+'travelPurposeF').val();;
	flightRequestTModel.projectId = $('#'+i+'projectIdF').val();;
	flightRequestTModel.processId = $('#'+i+'processIdF').val();;
	flightRequestTModel.prospectName = $('#'+i+'prospectNameF').val();;
//	flightRequestTModel.tripMode = $('#'+i+'').val();;
	flightRequestTModel.notes = $('#'+i+'notesF').val();;
//	flightRequestTModel.currency = $('#').val();;
//	flightRequestTModel.createdBy = null;
//	flightRequestTModel.modifiedBy = null;
//	flightRequestTModel.creationTimeStamp = null;
//	flightRequestTModel.modificationTimeStamp = null;


	
	
	return flightRequestTModel;
}

function getFlightRequestTModelList() {
	var flightClassSize = $('.flightChildDiv').length;
	/*$.each(flightClassSize, function(i,country){
		
	});*/
	var flightRequestTModelList = [];
	//flightRequestTModelList.push(getFlightRequestTModel(1));
	for(i=1;i<= flightClassSize;i++){
		flightRequestTModelList.push(getFlightRequestTModel(i));
	}
	
	return flightRequestTModelList;
}



function HotelRequestTModel(){
	
//	this.hotelRequestId = null;
	this.countryId = null;
	this.cityId = null;
	this.projectId = null;
//	this.cost = null;
//	this.rrqNo = null;
	this.prefHotel = null;
	this.checkInDate = null;
	this.checkOutDate = null;
	this.rewardMemberNo = null;
	this.clientName = null;
	this.travellingLocationAddr = null;
	this.travelPurpose = null;
	this.projectId = null;
	this.processId = null;
	this.prospectName = null;
//	this.tripMode = null;
	this.notes = null;
//	this.currency = null;
//	this.isBudgeted = null;
//	this.isBillable = null;
//	this.isApproved = null;
//	this.isRejected = null;
//	this.isCostApproved = null;
//	this.isCostRejected = null;
//	this.isActive = null;
//	this.createdBy = null;
//	this.modifiedBy = null;
//	this.creationTimeStamp = null;
//	this.modificationTimeStamp = null;
	
	
}


function getHotelRequestTModel(i){
	
	var hotelRequestTModel = new HotelRequestTModel();
	

	
//	hotelRequestTModel.hotelRequestId = $('#'+i+'').val();
	hotelRequestTModel.countryId = $('#'+i+'hotelCountry').val();
	hotelRequestTModel.cityId = $('#'+i+'hotelCity').val();
	hotelRequestTModel.projectId = $('#'+i+'projectIdH').val();
	hotelRequestTModel.processId = $('#'+i+'processIdH').val();
//	hotelRequestTModel.cost = $('#'+i+'').val();
//	hotelRequestTModel.rrqNo = $('#'+i+'').val();
	hotelRequestTModel.prefHotel = $('#'+i+'preferredHotel').val();
	hotelRequestTModel.checkInDate = $('#'+i+'checkInDate').val();
	hotelRequestTModel.checkOutDate = $('#'+i+'checkOutDate').val();
	hotelRequestTModel.rewardMemberNo = $('#'+i+'rewardMemberNo').val();
	hotelRequestTModel.clientName = $('#'+i+'clientNameH').val();
	hotelRequestTModel.travellingLocationAddr = $('#'+i+'bookNearLocation').val();
	hotelRequestTModel.travelPurpose = $('#'+i+'travelPurposeH').val();
//	hotelRequestTModel.projectName = $('#'+i+'').val();
	hotelRequestTModel.prospectName = $('#'+i+'prospectNameH').val();
//	hotelRequestTModel.tripMode = $('#'+i+'').val();
	hotelRequestTModel.notes = $('#'+i+'notesH').val();
//	hotelRequestTModel.currency = $('#'+i+'').val();
//	hotelRequestTModel.isBudgeted = $('#'+i+'').val();
//	hotelRequestTModel.isBillable = $('#'+i+'').val();
//	hotelRequestTModel.isApproved = $('#'+i+'').val();
//	hotelRequestTModel.isRejected = $('#'+i+'').val();
//	hotelRequestTModel.isCostApproved = $('#'+i+'').val();
//	hotelRequestTModel.isCostRejected = $('#'+i+'').val();
//	hotelRequestTModel.isActive = $('#'+i+'').val();
//	hotelRequestTModel.createdBy = $('#'+i+'').val();
//	hotelRequestTModel.modifiedBy = $('#'+i+'').val();
//	hotelRequestTModel.creationTimeStamp = $('#'+i+'').val();
//	hotelRequestTModel.modificationTimeStamp = $('#'+i+'').val();
	
	

	
	
	return hotelRequestTModel;
	
	
}

function getHotelRequestTModelList() {
	var hotelClassSize = $('.hotelChildDiv').length;
	
	var hotelRequestTModelList = [];
		for(i=1;i<= hotelClassSize;i++){
			hotelRequestTModelList.push(getHotelRequestTModel(i));
	}
	
	return hotelRequestTModelList;
}

function CarRequestTModel(){
//	  this.carRequestId = null;
	  this.countryId = null;
	  this.cityId = null;
	  this.projectId = null;
	  this.processId = null;
//	  this.cost = null;
//	  this.rrqNo = null;
	  this.pickUpLocation = null;
	  this.pickUpTime = null;
	  this.dropLoaction = null;
	  this.needPickUp = null;
	  this.carType = null;
	  this.prefCarCompany = null;
	  this.mebershipNo = null;
//	  this.sharedCar = null;
	  this.clientName = null;
//	  this.travellingLocationAddr = null;
	  this.travelPurpose = null;
//	  this.projectName = null;
	  this.prospectName = null;
//	  this.tripMode = null;
	  this.notes = null;
//	  this.currency = null;
//	  this.isBudgeted = null;
//	  this.isBillable = null;
//	  this.isApproved = null;
//	  this.isRejected = null;
//	  this.isCostApproved = null;
//	  this.isCostRejected = null;
//	  this.isActive = null;
//	  this.createdBy = null;
//	  this.modifiedBy = null;
//	  this.creationTimeStamp = null;
//	  this.modificationTimeStamp = null;

	
}

function getCarRequestTModel(i){
	
	var carRequestTModel = new CarRequestTModel();
	

//	carRequestTModel.carRequestId = $('#'+i+'').val();
	carRequestTModel.countryId = $('#'+i+'carCountry').val();
	carRequestTModel.cityId = $('#'+i+'carCity').val();
	carRequestTModel.projectId = $('#'+i+'projectIdC').val();
	carRequestTModel.processId = $('#'+i+'processIdC').val();
//	carRequestTModel.cost = null;
//	carRequestTModel.rrqNo = null;
	carRequestTModel.pickUpLocation = $('#'+i+'pickupAddress').val();
	carRequestTModel.pickUpTime = $('#'+i+'pickupDateAndTime').val();
	carRequestTModel.dropLoaction = $('#'+i+'dropOffAddress').val();
//	carRequestTModel.needPickUp = $('#'+i+'').val();
	carRequestTModel.carType = $('#'+i+'carType').val();
	carRequestTModel.prefCarCompany = $('#'+i+'preferredCarCo').val();
	carRequestTModel.mebershipNo = $('#'+i+'carMembershipNo').val();
//	carRequestTModel.sharedCar = null;
	carRequestTModel.clientName = $('#'+i+'clientNameC').val();
//	carRequestTModel.travellingLocationAddr = $('#'+i+'').val();
	carRequestTModel.travelPurpose = $('#'+i+'travelPurposeC').val();
//	carRequestTModel.projectName = null;
	carRequestTModel.prospectName = $('#'+i+'prospectNameC').val();
//	carRequestTModel.tripMode = null;
	carRequestTModel.notes = $('#'+i+'notesC').val();
//	carRequestTModel.currency = null;
//	carRequestTModel.isBudgeted = null;
//	carRequestTModel.isBillable = null;
//	carRequestTModel.isApproved = null;
//	carRequestTModel.isRejected = null;
//	carRequestTModel.isCostApproved = null;
//	carRequestTModel.isCostRejected = null;
//	carRequestTModel.isActive = null;
//	carRequestTModel.createdBy = null;
//	carRequestTModel.modifiedBy = null;
//	carRequestTModel.creationTimeStamp = null;
//	carRequestTModel.modificationTimeStamp = null;

	return carRequestTModel;
	
}

function getCarRequestTModelList() {
	var carClassSize = $('.carChildDiv').length;
	
	var carRequestTModelList = [];
		for(i=1;i<= carClassSize;i++){
			carRequestTModelList.push(getCarRequestTModel(i));
	}
	
	return carRequestTModelList;
}



function EmployeeDetails(){
	
	this.contactNo = null;
    this.empId = null;
    this.dob = null;
    this.travelType = null;
    this.mealPreference = null;
    this.passportNo = null;
    this.passportExpiryDate = null;
    this.visaType = null;
    this.visaNo = null;
    this.visaExpiryDate = null;
    this.mealPreference = null;
    this.designation = null;
    this.gender = null;
    this.empName = null;
}
function getEmployeeDetails(){
	
	var empDtlsModel = new EmployeeDetails();
	//travelRequestTModel.travelRequestId = ;
	empDtlsModel.contactNo = $('#contactNo').val();
	empDtlsModel.empId = $('#employeeId').val();
	empDtlsModel.dob = $('#dateOfBirth').val();
	empDtlsModel.travelType = $('#travelType').val();
	empDtlsModel.mealPreference = $('#employeeId').val();
	empDtlsModel.passportNo = $('#passportNo').val();
	empDtlsModel.passportExpiryDate = $('#passportExpiryDate').val();;
	empDtlsModel.visaType = $('#visaType').val();
	empDtlsModel.visaNo = $('#visaNo').val();
	empDtlsModel.visaExpiryDate = $('#visaExpiryDate').val();
	empDtlsModel.mealPreference = $('#mealPreference').val();;
	empDtlsModel.designation = $('#designation').val();;
	empDtlsModel.gender = $('#gender').val();;
	empDtlsModel.empName = $('#employeeName').val();;
	
//	empDtlsModel.flightList = getFlightRequestTModelList();
//	empDtlsModel.hotelList = getHotelRequestTModelList();
//	empDtlsModel.carList = getCarRequestTModelList();
	return empDtlsModel;
	
}


function TravelRequestTModel(){
	
	this.empDtls =  null;
	this.flightDtlsList =  null;
	this.hotelDtlsList =  null;
	this.carDtlsList =  null;
		
}


function getTravelRequestTModel(){
	var travelRequestDetails = new TravelRequestTModel();
	
	travelRequestDetails.empDtls = getEmployeeDetails(); 
	travelRequestDetails.flightDtlsList = getFlightRequestTModelList(); 
	travelRequestDetails.hotelDtlsList = getHotelRequestTModelList(); 
	travelRequestDetails.carDtlsList = getCarRequestTModelList(); 
	
	return travelRequestDetails;

}



