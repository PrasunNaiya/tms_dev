<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head lang="en">

    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>TMS: Travel Request Form</title>
    
	<link href="resources/assets/common/img/favicon.png" rel="icon" type="image/png">
    <link href="resources/icon/travel_logo_2.ico" rel="shortcut icon">

    <!-- Vendors Styles -->
    <!-- v1.0.0 -->
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/jscrollpane/style/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/fullcalendar/dist/fullcalendar.min.css">

    <!-- Clean UI Styles -->
    <link rel="stylesheet" type="text/css" href="resources/assets/common/css/source/main.css">

    <!-- Vendors Scripts -->
    <!-- v1.0.0 -->
    <script src="resources/assets/vendors/jquery/jquery.min.js"></script>
    <script src="resources/assets/vendors/tether/dist/js/tether.min.js"></script>
    <script src="resources/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="resources/assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="resources/assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="resources/assets/vendors/spin.js/spin.js"></script>
    <script src="resources/assets/vendors/ladda/dist/ladda.min.js"></script>
    <script src="resources/assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="resources/assets/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="resources/assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="resources/assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="resources/assets/vendors/autosize/dist/autosize.min.js"></script>
    <script src="resources/assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="resources/assets/vendors/moment/min/moment.min.js"></script>
    <script src="resources/assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="resources/assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    
    <script src="resources/assets/vendors/nestable/jquery.nestable.js"></script>
    <script src="resources/assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="resources/assets/vendors/datatables/media/js/dataTables.bootstrap4.min.js"></script>
    <script src="resources/assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="resources/assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="resources/assets/vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="resources/assets/vendors/d3/d3.min.js"></script>
    <script src="resources/assets/vendors/c3/c3.min.js"></script>
    
    <!-- v1.0.1 -->

    <!-- Clean UI Scripts -->
    <script src="resources/assets/common/js/common.js"></script>
    <script src="resources/assets/common/js/demo.temp.js"></script>
    
    <!-- Custom JS -->
    <script src="resources/js/travelRequestForm.js"></script>
    
</head>
<body class="theme-default">
<nav class="left-menu" left-menu>
    <div class="logo-container">
        <a href="http://www.rssoftware.com" class="logo">
            <img src="resources/assets/common/img/logo.png" alt="Clean UI Admin Template" />
            
        </a>
    </div>    
</nav>

<nav class="top-menu">
    <div class="menu-icon-container hidden-md-up">
        <div class="animate-menu-button left-menu-toggle">
            <div><!-- --></div>
        </div>
    </div>
    <div class="menu">
		 
        <a href="index.html" class="logo hidden-md-up">
            <img src="resources/assets/common/img/logo.png" alt=""  style=" width:45%"/>
            
        </a>
			 <h3 style="color: #1fb2fe;margin-top: 6px;">Travel Management System</h3>
        <div class="menu-info-block"></div>
    </div>
</nav>


<section class="page-content">
	<div class="page-content-inner">
	
	    <!-- Basic Form Elements -->
	    <section class="panel">
	        <div class="panel-heading">
	            <h3>Travel Request</h3>
	        </div>
	        <div class="panel-body">
	            <div class="row" id="employeeDetailsDiv">
	                <div class="col-lg-12">
	                    <div class="margin-bottom-50">
	                        <h4>Employee Details</h4>
	                        <br />
                            <div class="form-group row">
                                <div class="col-md-2">
									<label class="form-control-label" for="employeeId">Employee ID</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" name="employeeId" id="employeeId" value="${userId}" readonly>
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="employeeName">Name</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" name="employeeName" id="employeeName" readonly>
								</div>
                            </div>
                            <div class="form-group row">
                                <div class="col-md-2">
									<label class="form-control-label" for="dateOfBirth">Date of Birth</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" name="dateOfBirth" id="dateOfBirth" readonly>
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="gender">Gender</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" name="gender" id="gender" readonly>
								</div>
                            </div>
                            <div class="form-group row">
								<div class="col-md-2">
									<label class="form-control-label" for="designation">Designation</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" name="designation" id="designation" readonly>
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="contactNo">Contact Number</label>
								</div>
								<div class="col-md-4">
									<input type="number" class="form-control required" data-msg="Enter contact No" name="contactNo" id="contactNo">
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-2">
									<label for="travelType">Travel Type</label>
								</div>
								<div class="col-md-4">
									<select class="form-control required" name="travelType" data-msg="Enter flight type"  id="travelType" onclick="toggleTravelType(this.value)">
										<option value="">Select</option>
										<option value="Domestic">Domestic</option>
										<option value="International">International</option>
									</select>
								</div>
							<!-- 	<div class="col-md-2">
									<label for="mealPreference">Meal Preference</label>
								</div>
								<div class="col-md-4">
									<select class="form-control required" data-msg="Enter Meal type" name="mealPreference" id="mealPreference">
										<option value="">Select</option>
										<option value="No meal">No Meal</option>
										<option value="Vegan">Vegan</option>
										<option value="Veg">Veg</option>
										<option value="Non-Veg">Non-Veg</option>
									</select>
								</div> -->
							</div>
							<div class="form-group row internationalField">
								<div class="col-md-2">
									<label class="form-control-label" for="passportNo">Passport Number</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Passport Number" name="passportNo" data-msg="Enter passport No" id="passportNo">
								</div>
							<!-- // added -->	
								
								<div class="col-md-2">
									<label class="form-control-label" for="nameOnPassport">Name on Passport</label>
								</div>
								<div class="col-md-4">										
									<input type="text" class="form-control" placeholder="Name on Passport" name="nameOnPassport" data-msg="Enter the name on Passport" id="nameOnPassport"/>
								</div>
							</div>
							<div class="form-group row internationalField">
								<div class="col-md-2">
									<label class="form-control-label" for="addressOnPassport">Address on Passport</label>
								</div>
								<div class="col-md-4">
									<textarea class="form-control" name="addressOnPassport" id="addressOnPassport" data-msg="Enter address on passport"></textarea>
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="passportIssuePlace">Passport Issue Place</label>
								</div>
								<div class="col-md-4">										
									<input type="text" class="form-control" placeholder="Passport Issue Place" name="passportIssuePlace" data-msg="Enter Passport Issue Place" id="passportIssuePlace"/>
								</div>
							</div>
							<div class="form-group row internationalField">
								<div class="col-md-2">
									<label class="form-control-label" for="passportIssueDate">Passport Issue Date</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control datepicker-only-init" placeholder="Click to Select" name="passportIssueDate" data-msg="Enter passport Issue date" id="passportIssueDate">
								</div>
								
							<!-- //added -->	
								
								<div class="col-md-2">
									<label class="form-control-label" for="passportExpiryDate">Passport Expiry Date</label>
								</div>
								<div class="col-md-4">										
									<input type="text" class="form-control datepicker-only-init" placeholder="Click to Select" name="passportExpiryDate" data-msg="Enter passport expiry date" id="passportExpiryDate"/>
								</div>
							</div>
							
							<!-- added -->
							
							<div class="form-group row internationalField">
								<div class="col-md-2">
									<label class="form-control-label" for="ECNRCheck">ECNR Check</label>
								</div>
								<div class="col-md-4">
									<select class="form-control" name="ECNRCheck" data-msg="Enter ECNR Check"  id="ECNRCheck" >
									<option value="Select">Select</option>
									<option value="Required">Required</option>
									<option value=" NotRequired">Not Required</option>
								</select>
								</div>
								</div>
							
							<!-- --added -->
							
							<div class="form-group row internationalField">
								<div class="col-md-2">
									<label class="form-control-label" for="visaType">Visa Type</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Visa Type" name="visaType" data-msg="Enter visa type" id="visaType">
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="visaNo">Visa Number</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Visa Number" name="visaNo" data-msg="Enter visa No" id="visaNo">
								</div>
							</div>
							<div class="form-group row internationalField">
							<!-- added -->
							
							<div class="col-md-2">
									<label class="form-control-label" for="visaIssueDate">Visa Issue Date</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control datepicker-only-init" placeholder="Click to Select" name="visaIssueDate" data-msg="Enter visa issue date" id="visaIssueDate"/>
								</div>
							
								<div class="col-md-2">
									<label class="form-control-label" for="visaExpiryDate">Visa Expiry Date</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control datepicker-only-init" placeholder="Click to Select" name="visaExpiryDate" data-msg="Enter visa epiry date" id="visaExpiryDate"/>
								</div>
							</div>
							
							<div class="form-group row internationalField">
								<div class="col-md-2">
									<label class="form-control-label" for="pitition">Pitition #(if H1)</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control notImp" placeholder="Enter Pitition" name="pitition" data-msg="Enter Pitition" id="pitition">
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="validity">Validity</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Validity" name="validity" data-msg="Enter validity" id="validity">
								</div>
							</div>
							<div class="form-group row internationalField">
								<div class="col-md-2">
									<label class="form-control-label" for="nomineeName">Nominee Name</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Enter Nominee Name " name="nomineeName" data-msg="Enter Nominee Name" id="nomineeName">
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="emergencyContactPersonName">Emergency Contact Person Name</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Emergency Contact Person Name" name="emergencyContactPersonName" data-msg="Enter Emergency Contact Person Name" id="emergencyContactPersonName">
								</div>
							</div>
							
							<div class="form-group row internationalField">
								<div class="col-md-2">
									<label class="form-control-label" for="emergencyContactPersonAddress">Emergency Contact Person Address</label>
								</div>
								<div class="col-md-4">
									<textarea class="form-control" name="emergencyContactPersonAddress" id="emergencyContactPersonAddress" data-msg="Enter Emergency Contact Person Name"></textarea>
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="emergencyContactPersonNumber">Emergency Contact Person Contact Number</label>
								</div>
								<div class="col-md-4">
									<input type="text" class="form-control" placeholder="Emergency Contact Person Number" name="emergencyContactPersonNumber" data-msg="Enter Emergency Contact Person Number" id="emergencyContactPersonNumber">
								</div>
							</div>
							<!-- added -->
							
							<div class="form-group row">
								<div class="col-md-2">
									<label class="form-control-label">Requirements</label>
								</div>
								<div class="col-md-2">
									<label class="form-check-label checkbox-inline">
										<input type="checkbox" name="flightRequirement" id="flightRequirement" value="flight" onclick="toggleSubRequests('flight')"/>
										Flight
									</label>
								</div>
								<div class="col-md-2">
									<label class="form-check-label checkbox-inline">
										<input type="checkbox" name="hotelRequirement" id="hotelRequirement" value="hotel" onclick="toggleSubRequests('hotel')"/>
										Hotel
									</label>
								</div>
								<div class="col-md-2">
									<label class="form-checkl-label checkbox-inline">
										<input type="checkbox" name="carRequirement" id="carRequirement" value="car" onclick="toggleSubRequests('car')"/>
										Car
									</label>
								</div>
								<div class="col-md-2">
									<label class="form-checkl-label checkbox-inline">
										<input type="checkbox" name="companionRequirement" id="companionRequirement" value="companion" onclick="toggleSubRequests('companion')"/>
										Companion
									</label>
								</div>
							</div>
							
							<!-- --comp -->
							<!-- <div class="form-group row">
								<div class="col-md-2">
									<label class="form-control-label">Optional</label>
								</div>
							<div class="col-md-2">
									<label class="form-checkl-label checkbox-inline">
										<input type="checkbox" name="companionRequirement" id="companionRequirement" value="companion" onclick="toggleSubRequests('companion')"/>
										Companion
									</label>
								</div>
							</div> -->
						<!-- -comp -->	
							
							
	                    </div>
	                </div>
	            </div>
	            <div class="row" id="flightDetailsDiv" style="display: none">
	                <div class="col-lg-12">
	                    <div class="margin-bottom-50">
	                        <h4>Flight Details</h4>
	                        <span class="pull-right">
	                        	<i class="fa fa-plus" id="flightAddIcon" aria-hidden="false" onclick="addRow('flight')" style="cursor: pointer;"></i>&nbsp;&nbsp;&nbsp;
	                        	<i class="fa fa-minus" id="flightRemoveIcon" aria-hidden="false" onclick="removeRow('flight')" style="cursor: pointer; display: none"></i>
                        	</span>
	                        <br />
	                        <div id="flightParentDiv">
	                        	<div class="row flightChildDiv">
	                        		<div id="flight1">
			                        	<h5>Flight 1</h5>
			                        	<br />
			                        	<div id="flightStaticHtml">
				                            <div class="form-group row">
												<div class="col-md-2">
													<label for="1sourceCountry">Source Country</label>
												</div>
												<div class="col-md-2">
													<select id="1sourceCountry" data-msg="Select source country for flight1" class="form-control" onchange="populateCityList('source',this.id,this.value)">
														<option value="-1">Select</option>		
													</select>		
												</div>
												<div class="col-md-2">
													<label for="1sourceCity">Source City</label>
												</div>
												<div class="col-md-2">
													<select id="1sourceCity" data-msg="Select source city for flight1" class="form-control"  onchange="populateAirportList('source',this.id,this.value)" disabled>
														<option value="-1">Select</option>
													</select>		
												</div>
												<div class="col-md-2">
													<label for="1sourceAirport">Source Airport</label>
												</div>
												<div class="col-md-2">
													<select id="1sourceAirport" data-msg="Select source airport for flight1" class="form-control" disabled>
														<option value="-1">Select</option>
													</select>		
												</div>	
				                            </div>
				                            <div class="form-group row">
				                                <div class="col-md-2">
													<label for="1destinationCountry">Destination Country</label>
												</div>
												<div class="col-md-2">
													<select id="1destinationCountry" data-msg="Select destination country for flight1" class="form-control" onchange="populateCityList('destination',this.id,this.value)">
														<option value="-1">Select</option>		
													</select>		
												</div>
												<div class="col-md-2">
													<label for="1destinationCity">Destination City</label>
												</div>
												<div class="col-md-2">
													<select id="1destinationCity" data-msg="Select destination city for flight1" class="form-control" onchange="populateAirportList('destination',this.id,this.value)" disabled>
														<option value="-1">Select</option>
													</select>		
												</div>
												<div class="col-md-2">
													<label for="1destinationAirport">Destination Airport</label>
												</div>
												<div class="col-md-2">
													<select id="1destinationAirport" data-msg="Select destination airport for flight1" class="form-control" disabled>
														<option value="-1">Select</option>
													</select>		
												</div>
				                            </div>
				                            <div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1departureDate">Departure Date</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control datepicker-only-init" placeholder="Click to Select" name="1departureDate" data-msg="Enter departure date for flight1" id="1departureDate" />
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1departureTimePreference">Departure Time Preference</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control timepicker-init" placeholder="Click to Select" name="1departureTimePreference" data-msg="Enter departure time for flight1" id="1departureTimePreference" />
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1requiredArrivalTime">Required Arrival Time</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control timepicker-init" placeholder="Click to Select" name="1requiredArrivalTime" data-msg="Select arrival time for flight1" id="1requiredArrivalTime" />
												</div>
											</div>											
											<div class="form-group row">
												
												<!-- -added start-->
												<div class="col-md-1">
													<label class="form-control-label" for="1portOfEntry">Port of Entry</label>
												</div>
												<div class="col-md-1">
													<select class="form-control" name="1portOfEntry" id="1portOfEntry" data-msg="Select Port of Entry">
														<option value="-1">Select</option>
														<option value="1">International</option>
														</select>
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1frequentFlierAirline">Frequent Flier Airlines</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control notImp" placeholder="Frequent Flier Airlines" name="1frequentFlierAirline" data-msg="Enter Frequent Flier airlines for flight1" id="1frequentFlierAirline" />
												</div>
												
												<!-- added end-->
												
												<!-- <div class="col-md-2">
													<label class="form-control-label" for="1preferredAirline">Preferred Airlines</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control notImp" placeholder="Preferred Airlines" name="1preferredAirlines" data-msg="Enter preferred airlines for flight1" id="1preferredAirlines" />
												</div> -->
												<div class="col-md-2">
													<label class="form-control-label" for="1frequentFlierNo">Frequent Flier Number</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control notImp" placeholder="Frequent Flier Number" name="1frequentFlierNo" data-msg="Enter frequent flier number" id="1frequentFlierNo">
												</div>
												<!-- added start -->
												
												<div class="col-md-1">
													<label class="form-control-label" for="1frequentFlierFlightNo">Frequent Flier Flight Number</label>
												</div>
												<div class="col-md-1">
													<input type="text" class="form-control notImp" placeholder="Frequent Flier Flight Number" name="1frequentFlierFlightNo" data-msg="Enter frequent flier flight number" id="1frequentFlierFlightNo">
												</div>
												
												<!-- -added end -->
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1pppSelectionF">Project/Process/Prospect</label>
												</div>
												<div class="col-md-2">
													<select class="form-control" name="1pppSelectionF" id="1pppSelectionF" data-msg="Select destination project/process/prospect for flight1" onchange="showProjectProcessProspectSpecificFields('flight',this.id,this.value)">
														<option value="">Select</option>
														<option value="Project">Project</option>
														<option value="Process">Process</option>
														<option value="Prospect">Prospect</option>
													</select>
												</div>
												<div id="1projectDivF" style="display: none;">
													<div class="col-md-2">
														<label for="1projectIdF">Project</label>
													</div>
													<div class="col-md-2">
														<select class="form-control" name="1projectIdF" id="1projectIdF" data-msg="Select project for flight1" onchange="showClientName('flight',this.id,this.value);">
															<option value="-1">Select</option>
														</select>
													</div>
												</div>
												<div id="1processDivF" style="display: none;">
													<div class="col-md-2">
														<label for="1processIdF">Process</label>
													</div>
													<div class="col-md-2">
														<select class="form-control" name="1processIdF" id="1processIdF" data-msg="Select process for flight1" onchange="showClientName('flight',this.id,this.value);">
															<option value="-1">Select</option>
														</select>
													</div>
												</div>
												<div id="1prospectDivF" style="display: none;">
													<div class="col-md-2">
														<label for="1prospectNameF">Prospect</label>
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control" placeholder="Prospect Name" name="1prospectNameF" id="1prospectNameF" data-msg="Enter prospect for flight1" onkeyup="suggestProspectName('flight',this.id,this.value);"/>
													</div>
												</div>
												<div id="1clientNameDivF" style="display: none;">
													<div class="col-md-2">
														<label class="form-control-label" for="1clientNameF">Client Name</label>
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control notImp" placeholder="Client Name" name="1clientNameF" id="1clientNameF" data-msg="Enter client name for flight1" readonly/>
													</div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1travelPurposeF">Purpose of Travel</label>
												</div>
												<div class="col-md-2">
													<select class="form-control notImp" name="1travelPurposeF" id="1travelPurposeF" data-msg="Select puprose of travel for flight1">
														<option value="">Select</option>
														<option value="Meeting">Meeting</option>
														<option value="Project">Project</option>
													</select>
												</div>
												<!-- <div class="col-md-2">
													<label class="form-control-label">Mode of Trip</label>
												</div>
												<div class="col-md-2">
													<label class="form-check-inline">
														<input type="radio" name="1tripMode" id="1singleTripMode" value="Single Trip" onclick="toggleRoundTripFields(this.value)"/>
															Single Trip
													</label>
												</div>
												<div class="col-md-2">
													<label class="form-check-inline">
														<input type="radio" name="1tripMode" id="1roundTripMode" value="Round Trip" onclick="toggleRoundTripFields(this.value)"/>
															Round Trip
													</label>
												</div> -->
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1notesF">Notes</label>
												</div>
												<div class="col-md-10">
													<textarea class="form-control notImp" rows="3" name="1notesF" id="1notesF" data-msg="Enter notes for flight1"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
	                    </div>
	                </div>
	            </div>
	            <div class="row" id="hotelDetailsDiv" style="display: none">
	                <div class="col-lg-12">
	                    <div class="margin-bottom-50">
	                        <h4>Hotel Details</h4>
	                        <span class="pull-right">
	                        	<i class="fa fa-plus" id="hotelAddIcon" aria-hidden="false" onclick="addRow('hotel')" style="cursor: pointer;"></i>&nbsp;&nbsp;&nbsp;
	                        	<i class="fa fa-minus" id="hotelRemoveIcon" aria-hidden="false" onclick="removeRow('hotel')" style="cursor: pointer; display: none"></i>
                        	</span>
	                        <br />
	                        <div id="hotelParentDiv">
	                        	<div class="row hotelChildDiv">
	                        		<div id="hotel1">
			                        	<h5>Hotel 1</h5>
			                        	<br />
			                        	<div id="hotelStaticHtml">
			                        		<div class="form-group row">
												<div class="col-md-2">
													<label for="1hotelCountry">Country</label>
												</div>
												<div class="col-md-2">
													<select id="1hotelCountry" class="form-control" data-msg="Select country for hotel1" onchange="populateCityList('hotel',this.id,this.value)">
														<option value="-1">Select</option>		
													</select>		
												</div>
												<div class="col-md-2">
													<label for="1hotelCity">City</label>
												</div>
												<div class="col-md-2">
													<select id="1hotelCity" class="form-control" data-msg="Select city for hotel1" onchange="toggleCityBasedHotelInfo(this.id,this.value)" disabled>
														<option value="-1">Select</option>
													</select>		
												</div>
												<div class="col-md-2">
													<label class="form-control-label notImp" for="1preferredHotel">Preferred Hotel</label>
												</div>
												<div class="col-md-2">
													<input class="form-control notImp" name="1preferredHotel" id="1preferredHotel" placeholder="Preferred Hotel" data-msg="Enter preferred hotel for hotel1" disabled />
												</div>	
				                            </div>
			                        		<div class="form-group row">
			                        			<div class="col-md-2">
													<label class="form-control-label" for="1checkInDate">Check-in Date</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control datepicker-init" placeholder="Click to Select" name="1checkInDate" data-msg="Enter check in date for hotel1" id="1checkInDate" />
												</div>
			                        			<div class="col-md-2">
													<label class="form-control-label" for="1checkOutDate">Check-out</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control datepicker-init" placeholder="Click to Select" name="1checkOutDate" data-msg="Enter check out date for hotel1" id="1checkOutDate" />
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="bookNearLocation">Book Near Location</label>
												</div>
												<div class="col-md-2">
													<input class="form-control notImp" name="1bookNearLocation" id="1bookNearLocation" data-msg="Enter near location for hotel1" placeholder="Book Near Location" disabled />
												</div>												
			                        		</div>			                        		
			                        		<div class="form-group row">
			                        			<div class="col-md-2">
													<label class="form-control-label" for="1rewardMemberNo">Reward Member Number</label>
												</div>
												<div class="col-md-2">
													<input class="form-control notImp" name="1rewardMemberNo" id="1rewardMemberNo" placeholder="Reward Member Number" data-msg="Enter Reward Member Number for flight1" />
												</div>
			                        		</div>
			                        		<div class="form-group row">
												<div class="col-md-2">
													<label for="1pppSelectionH">Project/Process/Prospect</label>
												</div>
												<div class="col-md-2">
													<select class="form-control" name="1pppSelectionH" id="1pppSelectionH" data-msg="Select project/process/prospect for hotel1" onchange="showProjectProcessProspectSpecificFields('hotel',this.id,this.value)">
														<option value="">Select</option>
														<option value="Project">Project</option>
														<option value="Process">Process</option>
														<option value="Prospect">Prospect</option>
													</select>
												</div>
												<div id="1projectDivH" style="display: none;">
													<div class="col-md-2">
														<label for="1projectIdH">Project</label>
													</div>
													<div class="col-md-2">
														<select class="form-control" name="1projectIdH" id="1projectIdH" data-msg="Select project for hotel1" onchange="showClientName('hotel',this.id,this.value);">
															<option value="-1">Select</option>
														</select>
													</div>
												</div>
												<div id="1processDivH" style="display: none;">
													<div class="col-md-2">
														<label for="1processIdH">Process</label>
													</div>
													<div class="col-md-2">
														<select class="form-control" name="1processIdH" id="1processIdH" data-msg="Select process for hotel1" onchange="showClientName('hotel',this.id,this.value);">
															<option value="-1">Select</option>
														</select>
													</div>
												</div>
												<div id="1prospectDivH" style="display: none;">
													<div class="col-md-2">
														<label for="1prospectNameH">Prospect</label>
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control" placeholder="Prospect Name" name="1prospectNameH" id="1prospectNameH" data-msg="Enter prospect for hotel1" onkeyup="suggestProspectName('hotel',this.id,this.value);"/>
													</div>
												</div>
												<div id="1clientNameDivH" style="display: none;">
													<div class="col-md-2">
														<label class="form-control-label" for="1clientNameH">Client Name</label>
													</div>
													<div class="col-md-2 notImp">
														<input type="text" class="form-control" placeholder="Client Name" name="1clientNameH" data-msg="Enter client name for hotel1" id="1clientNameH" readonly/>
													</div>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1travelPurposeH">Purpose of Travel</label>
												</div>
												<div class="col-md-2">
													<select class="form-control notImp" name="1travelPurposeH" data-msg="Select travel purpose for hotel1" id="1travelPurposeH">
														<option value="">Select</option>
														<option value="Meeting">Meeting</option>
														<option value="Project">Project</option>
													</select>
												</div>
											</div>
											<div class="form-group row">
												<div class="col-md-2">
													<label for="1notesH">Notes</label>
												</div>
												<div class="col-md-10">
													<textarea class="form-control notImp" rows="3" name="1notesH" data-msg="Select notes for hotel1" id="1notesH"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
	                    </div>
	                </div>
	            </div>
	            <div class="row" id="carDetailsDiv" style="display: none">
	                <div class="col-lg-12">
	                    <div class="margin-bottom-50">
	                        <h4>Car Details</h4>
	                        <span class="pull-right">
	                        	<i class="fa fa-plus" id="carAddIcon" aria-hidden="false" onclick="addRow('car')" style="cursor: pointer;"></i>&nbsp;&nbsp;&nbsp;
	                        	<i class="fa fa-minus" id="carRemoveIcon" aria-hidden="false" onclick="removeRow('car')" style="cursor: pointer; display: none"></i>
                        	</span>
	                        <br />
	                        <div id="carParentDiv">
	                        	<div class="row carChildDiv">
	                        		<div id="car1">
			                        	<h5>Car 1</h5>
			                        	<br />
			                        	<div id="carStaticHtml">
			                        		<div class="form-group row">
			                        			<div class="col-md-2">
													<label for="1carCountry">Country</label>
												</div>
												<div class="col-md-2">
													<select id="1carCountry" class="form-control" data-msg="Select country for car1" onchange="populateCityList('car',this.id,this.value)">
														<option value="-1">Select</option>		
													</select>		
												</div>
												<div class="col-md-2">
													<label for="1carCity">City</label>
												</div>
												<div class="col-md-2">
													<select id="1carCity" class="form-control" data-msg="Select city for car1" onchange="toggleCityBasedCarInfo(this.id,this.value)" disabled>
														<option value="-1">Select</option>
													</select>		
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1pickupAddress">Pick-up Location Address</label>
												</div>
												<div class="col-md-2">
													<textarea class="form-control" rows="3" name="1pickupAddress" data-msg="Enter pick up location for car1" id="1pickupAddress" disabled></textarea>
												</div>
			                        		</div>
			                        		<div class="form-group row">
			                        			<div class="col-md-2">
													<label class="form-control-label" for="1pickupDateAndTime">Pick-up Date and Time</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control datepicker-init" placeholder="Click to Select" name="1pickupDateAndTime" data-msg="Enter pick up date/time for car1" id="1pickupDateAndTime" />
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1dropOffAddress">Drop-off Location Address</label>
												</div>
												<div class="col-md-2">
													<textarea class="form-control" rows="3" name="1dropOffAddress" data-msg="Select drop address for car1" id="1dropOffAddress" disabled></textarea>
												</div>
												<!-- <div class="col-md-2">
													<label class="form-check-label checkbox-inline">
														<input type="checkbox" name="1needReturnPickUp" id="1needReturnPickUp" value="1" onclick="toggleReturnPickupFields(this.id,this.value)"/>
														Need Return Pick-up
													</label>
												</div> -->
			                        		</div>
			                        		<div class="form-group row">
												<div class="col-md-2">
													<label class="form-control-label" for="1carType">Car Type</label>
												</div>
												<div class="col-md-2">
													<select id="1carType" data-msg="Select car type for car1" class="form-control">
														<option value="">Select</option>
														<option value="Ordinary">Ordinary</option>
														<option value="Limo">Limo</option>
													</select>
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1preferredCarCo">Preferred Car Company</label>
												</div>
												<div class="col-md-2">
													<div class="form-group">
														<input type="text" class="form-control notImp" placeholder="Preferred Car Company" name="1preferredCarCo" id="1preferredCarCo" class="form-control models" data-msg="Enter preferrred car company for car1" />
													</div>
												</div>
												<div class="col-md-2">
													<label class="form-control-label" for="1carMembershipNo">Membership Number</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control notImp" placeholder="Membership Number" name="1carMembershipNo" id="1carMembershipNo" data-msg="Enter car membership number for car1" />
												</div>
			                        		</div>
			                        		<div class="form-group row">
												<div class="col-md-2">
													<label for="1pppSelectionC">Project/Process/Prospect</label>
												</div>
												<div class="col-md-2">
													<select class="form-control" name="1pppSelectionC" id="1pppSelectionC" data-msg="Select project/process/prospect for car1" onchange="showProjectProcessProspectSpecificFields('car',this.id,this.value)">
														<option value="">Select</option>
														<option value="Project">Project</option>
														<option value="Process">Process</option>
														<option value="Prospect">Prospect</option>
													</select>
												</div>
												<div id="1projectDivC" style="display: none;">
													<div class="col-md-2">
														<label for="1projectIdC">Project</label>
													</div>
													<div class="col-md-2">
														<select class="form-control" name="1projectIdC" id="1projectIdC" data-msg="Select project for car1" onchange="showClientName('car',this.id,this.value);">
															<option value="-1">Select</option>
														</select>
													</div>
												</div>
												<div id="1processDivC" style="display: none;">
													<div class="col-md-2">
														<label for="1processIdC">Process</label>
													</div>
													<div class="col-md-2">
														<select class="form-control" name="1processIdC" id="1processIdC" data-msg="Select process for car1" onchange="showClientName('car',this.id,this.value);">
															<option value="-1">Select</option>
														</select>
													</div>
												</div>
												<div id="1prospectDivC" style="display: none;">
													<div class="col-md-2">
														<label for="1prospectNameC">Prospect</label>
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control" placeholder="Prospect Name" name="1prospectNameC" id="1prospectNameC" data-msg="Enter prospect for car1" onkeyup="suggestProspectName('car',this.id,this.value);"/>
													</div>
												</div>
												<div id="1clientNameDivC" style="display: none;">
													<div class="col-md-2">
														<label class="form-control-label" for="1clientNameC">Client Name</label>
													</div>
													<div class="col-md-2">
														<input type="text" class="form-control notImp" placeholder="Client Name" name="1clientNameC" id="1clientNameC" data-msg="Enter client name for car1" readonly/>
													</div>
												</div>
											</div>
											<div class="form-group row">
											
										<!-- 	<div class="col-md-2">
													<label for="1clientAddress">Client Address</label>
												</div>
												<div class="col-md-2">
													<textarea class="form-control notImp" name="1clientAddress" id="1clientAddress" data-msg="Enter client address for flight1"></textarea>
												</div>
											 -->
											
												<div class="col-md-2">
													<label for="1travelPurposeC">Purpose of Travel</label>
												</div>
												<div class="col-md-2">
													<select class="form-control notImp" name="1travelPurposeC" data-msg="Select travel purpose for car1" id="1travelPurposeC">
														<option value="">Select</option>
														<option value="Meeting">Meeting</option>
														<option value="Project">Project</option>
													</select>
												</div>
												
											<!-- 	<div class="col-md-2">
													<label class="form-control-label" for="1clientContactNo">Client Contact No</label>
												</div>
												<div class="col-md-2">
													<input type="text" class="form-control notImp" placeholder="Client Contact Number" name="1clientContactNo" data-msg="Enter client contact number" id="1clientContactNo">
												</div> -->
												
												
											</div>
											<div class="form-group row"> 
												<div class="col-md-2">
													<label for="1notesC">Notes</label>
												</div>
												<div class="col-md-10">
													<textarea class="form-control notImp" rows="3" name="1notesC" id="1notesC" data-msg="Enter notes for car1"></textarea>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
	                    </div>
	                </div>
	            </div>
	            <!-- companion start-->
	            
	             <div class="row" id="companionDetailsDiv" style="display: none">
	                <div class="col-lg-12">
	                    <div class="margin-bottom-50">
	                         <h4>Companion Details</h4>
	                        <span class="pull-right">
	                        	<i class="fa fa-plus" id="companionAddIcon" aria-hidden="false" onclick="addRow('companion')" style="cursor: pointer;"></i>&nbsp;&nbsp;&nbsp;
	                        	<i class="fa fa-minus" id="companionRemoveIcon" aria-hidden="false" onclick="removeRow('companion')" style="cursor: pointer; display: none"></i>
                        	</span>
	                        <br />
	                        <div id="companionParentDiv">
	                        	<div class="row companionChildDiv">
	                        		<div id="companion1">
			                        	<h5>Companion 1</h5>
			                        	<br />
			                        	<div id="companionStaticHtml">
                            <div class="form-group row">
                                <div class="col-md-2">
									<label class="form-control-label" for="1companionName">Companion Name</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control" name="1companionName" id="1companionName">
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1relation">Relation</label>
								</div>
								<div class="col-md-2">
									<select class="form-control" name="1relation" data-msg="Select relation" id="1relation">
														<option value="">Select</option>
														<option value="Spouse">Spouse</option>
														<option value="Child">Child</option>
													</select>
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cdateOfBirth">Date of Birth</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control datepicker-only-init" name="1cdateOfBirth" id="1cdateOfBirth" placeholder="mm/dd/yyyy">
								</div>
								</div>
								<div class="form-group row">
                                <div class="col-md-2">
									<label class="form-control-label" for="1cpassportNo">Passport No.*</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control" name="1cpassportNo" id="1cpassportNo">
								</div>
								<div class="col-md-2">
								<label for="1caddressOnPassport">Address on Passport</label>
								</div>
								<div class="col-md-2">
								<textarea class="form-control" name="1caddressOnPassport" id="1caddressOnPassport" data-msg="Enter address on passport for companion1"></textarea>
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cpassportIssuePlace">Passport Issue Place</label>
								</div>
								<div class="col-md-2">										
									<input type="text" class="form-control" placeholder="Passport Issue Place" name="1cpassportIssuePlace" data-msg="Enter Passport Issue Place" id="1cpassportIssuePlace"/>
								</div>
							</div>
							<div class="form-group row">
								<div class="col-md-2">
									<label class="form-control-label" for="1cpassportIssueDate">Passport Issue Date</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control datepicker-only-init" placeholder="Click to Select" name="1cpassportIssueDate" data-msg="Enter passport Issue date" id="1cpassportIssueDate">
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cpassportExpiryDate">Passport Expiry Date</label>
								</div>
								<div class="col-md-2">										
									<input type="text" class="form-control datepicker-only-init" placeholder="Click to Select" name="1cpassportExpiryDate" data-msg="Enter passport expiry date" id="1cpassportExpiryDate"/>
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cECNRCheck">ECNR Check</label>
								</div>
								<div class="col-md-2">
									<select class="form-control" name="1cECNRCheck" data-msg="Enter ECNR Check"  id="1cECNRCheck" onclick="toggle1cECNRCheck(this.value)">
									<option value="">Select</option>
									<option value="1cRequired">Required</option>
									<option value="1cNotRequired">Not Required</option>
								</select>
								</div>
								</div>
							<div class="form-group row">
								<div class="col-md-2">
									<label class="form-control-label" for="1cvisaType">Visa Type</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control" placeholder="Visa Type" name="1cvisaType" data-msg="Enter visa type" id="1cvisaType">
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cvisaNo">Visa Number</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control" placeholder="Visa Number" name="1cvisaNo" data-msg="Enter visa No" id="1cvisaNo">
								</div>
						
								<div class="col-md-2">
									<label class="form-control-label" for="1cvisaIssueDate">Visa Issue Date</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control datepicker-only-init" placeholder="Click to Select" name="1cvisaIssueDate" data-msg="Enter visa issue date" id="1cvisaIssueDate"/>
								</div>
								</div>
							<div class="form-group row">
								<div class="col-md-2">
									<label class="form-control-label" for="1cvisaExpiryDate">Visa Expiry Date</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control datepicker-only-init" placeholder="Click to Select" name="1cvisaExpiryDate" data-msg="Enter visa expiry date" id="1cvisaExpiryDate"/>
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cvalidity">Validity</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control" placeholder="Validity" name="1cvalidity" data-msg="Enter validity" id="1cvalidity">
								</div>
								<div class="col-md-2">
									<label class="form-control-label" for="1cdateOfTravel">Date of Travel</label>
								</div>
								<div class="col-md-2">
									<input type="text" class="form-control datepicker-only-init" placeholder="Click to Select" name="1cdateOfTravel" data-msg="Enter date of travel" id="1cdateOfTravel"/>
								</div>
								</div>
								
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
                            </div>
	            
	             <!-- companion end -->
	             
	            <div class="row">
	                <div class="col-lg-12">
	                    <div class="margin-bottom-50">
                            <div class="form-actions">
                                <div class="form-group row">
                                    <div class="col-md-9 col-md-offset-3">
                                        <button type="button" class="btn width-150 btn-primary" onclick="formSubmit()">Submit</button>
                                        <button type="button" class="btn btn-default">Cancel</button>
                                    </div>
                                </div>
                            </div>
	                        <form id="errorForm" action="redirect" method="post">
	                        	<input type="hidden" name="targetPage" id="targetPage" value="error" />
	                        	<input type="hidden" name="errorMsg" id="errorMsg" />
	                        </form>
	                    </div>
	                </div>
	            </div>
	        
	    <!-- End -->
	
	</div>
</section>

<div class="main-backdrop"><!-- --></div>

</body>
</html>