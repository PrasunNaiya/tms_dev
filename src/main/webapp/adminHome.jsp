<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    
    <title>TMS: Administrator Home</title>
    
	<link href="resources/assets/common/img/favicon.png" rel="icon" type="image/png">
    <link href="favicon.ico" rel="shortcut icon">

    <!-- Vendors Styles -->
    <!-- v1.0.0 -->
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/bootstrap/dist/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/jscrollpane/style/jquery.jscrollpane.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/ladda/dist/ladda-themeless.min.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/select2/dist/css/select2.min.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/eonasdan-bootstrap-datetimepicker/build/css/bootstrap-datetimepicker.min.css">
    <link rel="stylesheet" type="text/css" href="resources/assets/vendors/fullcalendar/dist/fullcalendar.min.css">

    <!-- Clean UI Styles -->
    <link rel="stylesheet" type="text/css" href="resources/assets/common/css/source/main.css">

    <!-- Vendors Scripts -->
    <!-- v1.0.0 -->
    <script src="resources/assets/vendors/jquery/jquery.min.js"></script>
    <script src="resources/assets/vendors/tether/dist/js/tether.min.js"></script>
    <script src="resources/assets/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <script src="resources/assets/vendors/jquery-mousewheel/jquery.mousewheel.min.js"></script>
    <script src="resources/assets/vendors/jscrollpane/script/jquery.jscrollpane.min.js"></script>
    <script src="resources/assets/vendors/spin.js/spin.js"></script>
    <script src="resources/assets/vendors/ladda/dist/ladda.min.js"></script>
    <script src="resources/assets/vendors/select2/dist/js/select2.full.min.js"></script>
    <script src="resources/assets/vendors/html5-form-validation/dist/jquery.validation.min.js"></script>
    <script src="resources/assets/vendors/jquery-typeahead/dist/jquery.typeahead.min.js"></script>
    <script src="resources/assets/vendors/jquery-mask-plugin/dist/jquery.mask.min.js"></script>
    <script src="resources/assets/vendors/autosize/dist/autosize.min.js"></script>
    <script src="resources/assets/vendors/bootstrap-show-password/bootstrap-show-password.min.js"></script>
    <script src="resources/assets/vendors/moment/min/moment.min.js"></script>
    <script src="resources/assets/vendors/eonasdan-bootstrap-datetimepicker/build/js/bootstrap-datetimepicker.min.js"></script>
    <script src="resources/assets/vendors/fullcalendar/dist/fullcalendar.min.js"></script>
    
    <script src="resources/assets/vendors/nestable/jquery.nestable.js"></script>
    <script src="resources/assets/vendors/datatables/media/js/jquery.dataTables.min.js"></script>
    <script src="resources/assets/vendors/datatables/media/js/dataTables.bootstrap4.min.js"></script>
    <script src="resources/assets/vendors/datatables-fixedcolumns/js/dataTables.fixedColumns.js"></script>
    <script src="resources/assets/vendors/datatables-responsive/js/dataTables.responsive.js"></script>
    <script src="resources/assets/vendors/editable-table/mindmup-editabletable.js"></script>
    <script src="resources/assets/vendors/d3/d3.min.js"></script>
    <script src="resources/assets/vendors/c3/c3.min.js"></script>
    
    <!-- v1.0.1 -->

    <!-- Clean UI Scripts -->
    <script src="resources/assets/common/js/common.js"></script>
    <script src="resources/assets/common/js/demo.temp.js"></script>
    
</head>
<body>

</body>
</html>